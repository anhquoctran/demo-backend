﻿namespace HappyFarm.Core.Interfaces
{
    public interface IHasUpdateTime
    {
        int? UpdatedAt { get; set; }
    }
}
