﻿namespace HappyFarm.Core.Interfaces
{
    public interface IEntity
    {
        int Id { get; set; }
    }

    public interface IEntity<TPrimaryKey>
    {
        TPrimaryKey Id { get; set; }
    }
}
