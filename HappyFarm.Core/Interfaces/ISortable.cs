﻿using System.Linq;

namespace HappyFarm.Core.Interfaces
{
    public interface ISortable<TEntity>
    {
        IQueryable<TEntity> Sort(IQueryable<TEntity> source, string orderByQuerystring, bool useSnackcase = true);
    }
}
