﻿namespace HappyFarm.Core.Interfaces
{
    public interface IHasCreationTime
    {
        public int? CreatedAt { get; set; }
    }
}
