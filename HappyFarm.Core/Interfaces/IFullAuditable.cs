﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HappyFarm.Core.Interfaces
{
    interface IFullAuditable : IHasCreationTime, IHasUpdateTime
    {
    }
}
