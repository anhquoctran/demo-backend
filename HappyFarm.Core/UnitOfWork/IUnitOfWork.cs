﻿using HappyFarm.Core.Models;
using HappyFarm.Core.Repositories;
using Microsoft.EntityFrameworkCore.Storage;
using System.Threading;
using System.Threading.Tasks;

namespace HappyFarm.Core.UnitOfWork
{
    public interface IUnitOfWork
    {
        IRepository<User> UserRepository { get; }
        IRepository<Farm> FarmRepository { get; }
        IRepository<Gateway> GatewayRepository { get; }
        IRepository<SensorNode> SensorNodeRepository { get; }
        IRepository<SensorValue> SensorValueRepository { get; }

        void ReInitialize();

        IDbContextTransaction BeginTransaction();

        int SaveChanges();

        Task<int> SaveChangeAsync(CancellationToken cancelToken = default);
    }
}
