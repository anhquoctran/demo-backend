﻿using HappyFarm.Core.Models;
using HappyFarm.Core.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System.Threading;
using System.Threading.Tasks;

namespace HappyFarm.Core.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private HappyFarmDbContext _dbContext;
        private readonly DbContextOptions<HappyFarmDbContext> _dbContextOptions;

        public UnitOfWork(
            DbContextOptions<HappyFarmDbContext> dbContextOptions, 
            HappyFarmDbContext happyFarmDbContext)
        {
            _dbContextOptions = dbContextOptions;
            _dbContext = happyFarmDbContext;
            InitializeRepositories();
        }

        public IRepository<User> UserRepository { get; private set; }

        public IRepository<Farm> FarmRepository { get; private set; }

        public IRepository<Gateway> GatewayRepository { get; private set; }

        public IRepository<SensorNode> SensorNodeRepository { get; private set; }

        public IRepository<SensorValue> SensorValueRepository { get; private set; }

        public void ReInitialize()
        {
            _dbContext = new HappyFarmDbContext(_dbContextOptions);
            InitializeRepositories();
        }

        public IDbContextTransaction BeginTransaction()
        {
            return _dbContext.Database.BeginTransaction();
        }

        public int SaveChanges()
        {
            return _dbContext.SaveChanges();
        }

        public Task<int> SaveChangeAsync(CancellationToken cancelToken = default)
        {
            return _dbContext.SaveChangesAsync(cancelToken);
        }

        private void InitializeRepositories()
        {
            UserRepository = new Repository<User>(_dbContext);
            FarmRepository = new Repository<Farm>(_dbContext);
            GatewayRepository = new Repository<Gateway>(_dbContext);
            SensorNodeRepository = new Repository<SensorNode>(_dbContext);
            SensorValueRepository = new Repository<SensorValue>(_dbContext);
        }
    }
}
