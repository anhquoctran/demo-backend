﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HappyFarm.Core.Migrations
{
    public partial class AddManagerUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "user",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "prevent_delete", "updated_at" },
                values: new object[] { 1620571377, true, 1620571377 });

            migrationBuilder.InsertData(
                table: "user",
                columns: new[] { "id", "Avatar", "created_at", "email", "farm_id", "firstname", "lastname", "password", "prevent_delete", "role", "updated_at", "username" },
                values: new object[] { 2, null, 1620571377, "manager1@gmail.com", null, "System", "Manager", "$2a$10$ahBg0LfSIkqMuTNBhs9Vte.4hSodUEP0NZm53LVxX3LAzUK5Aam/O", true, "sa", 1620571377, "manager1" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "user",
                keyColumn: "id",
                keyValue: 2);

            migrationBuilder.UpdateData(
                table: "user",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "prevent_delete", "updated_at" },
                values: new object[] { 1620561206, true, 1620561206 });
        }
    }
}
