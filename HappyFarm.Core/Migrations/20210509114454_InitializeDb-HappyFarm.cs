﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HappyFarm.Core.Migrations
{
    public partial class InitializeDbHappyFarm : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "farm",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(nullable: false),
                    Address = table.Column<string>(nullable: true),
                    region_code = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    updated_at = table.Column<int>(nullable: true),
                    created_at = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_farm", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "gateway",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(nullable: false),
                    code = table.Column<string>(nullable: true),
                    password = table.Column<string>(nullable: true),
                    farm_id = table.Column<int>(nullable: false),
                    config = table.Column<string>(nullable: true),
                    created_at = table.Column<int>(nullable: true),
                    updated_at = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_gateway", x => x.id);
                    table.ForeignKey(
                        name: "FK_gateway_farm_farm_id",
                        column: x => x.farm_id,
                        principalTable: "farm",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "user",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    username = table.Column<string>(nullable: false),
                    password = table.Column<string>(nullable: false),
                    email = table.Column<string>(nullable: false),
                    lastname = table.Column<string>(nullable: false),
                    firstname = table.Column<string>(nullable: false),
                    Avatar = table.Column<string>(nullable: true),
                    role = table.Column<string>(nullable: false),
                    farm_id = table.Column<int>(nullable: true),
                    prevent_delete = table.Column<bool>(nullable: false, defaultValue: false),
                    deactivated = table.Column<bool>(nullable: false, defaultValue: false),
                    created_at = table.Column<int>(nullable: true),
                    updated_at = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_user", x => x.id);
                    table.ForeignKey(
                        name: "FK_user_farm_farm_id",
                        column: x => x.farm_id,
                        principalTable: "farm",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "sensor_node",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(nullable: false),
                    code = table.Column<string>(nullable: false),
                    gateway_id = table.Column<int>(nullable: false),
                    updated_at = table.Column<int>(nullable: true),
                    created_at = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sensor_node", x => x.id);
                    table.ForeignKey(
                        name: "FK_sensor_node_gateway_gateway_id",
                        column: x => x.gateway_id,
                        principalTable: "gateway",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "sensor_value",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    temp = table.Column<decimal>(nullable: false),
                    air_humidity = table.Column<decimal>(nullable: false),
                    ph_scale = table.Column<decimal>(nullable: false),
                    light_intensity = table.Column<decimal>(nullable: false),
                    soil_humidity = table.Column<decimal>(nullable: false),
                    wind_speed = table.Column<decimal>(nullable: false),
                    rainfall = table.Column<decimal>(nullable: false),
                    tds = table.Column<decimal>(nullable: false),
                    node_id = table.Column<int>(nullable: false),
                    created_at = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sensor_value", x => x.id);
                    table.ForeignKey(
                        name: "FK_sensor_value_sensor_node_node_id",
                        column: x => x.node_id,
                        principalTable: "sensor_node",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_farm_id",
                table: "farm",
                column: "id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_farm_Phone",
                table: "farm",
                column: "Phone",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_gateway_farm_id",
                table: "gateway",
                column: "farm_id");

            migrationBuilder.CreateIndex(
                name: "IX_gateway_id",
                table: "gateway",
                column: "id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_gateway_name",
                table: "gateway",
                column: "name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_sensor_node_code",
                table: "sensor_node",
                column: "code",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_sensor_node_gateway_id",
                table: "sensor_node",
                column: "gateway_id");

            migrationBuilder.CreateIndex(
                name: "IX_sensor_node_id",
                table: "sensor_node",
                column: "id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_sensor_node_name",
                table: "sensor_node",
                column: "name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_sensor_value_id",
                table: "sensor_value",
                column: "id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_sensor_value_node_id",
                table: "sensor_value",
                column: "node_id");

            migrationBuilder.CreateIndex(
                name: "IX_user_email",
                table: "user",
                column: "email",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_user_farm_id",
                table: "user",
                column: "farm_id");

            migrationBuilder.CreateIndex(
                name: "IX_user_id",
                table: "user",
                column: "id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_user_username",
                table: "user",
                column: "username",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "sensor_value");

            migrationBuilder.DropTable(
                name: "user");

            migrationBuilder.DropTable(
                name: "sensor_node");

            migrationBuilder.DropTable(
                name: "gateway");

            migrationBuilder.DropTable(
                name: "farm");
        }
    }
}
