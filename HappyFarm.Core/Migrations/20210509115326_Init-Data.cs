﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HappyFarm.Core.Migrations
{
    public partial class InitData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "user",
                columns: new[] { "id", "Avatar", "created_at", "email", "farm_id", "firstname", "lastname", "password", "prevent_delete", "role", "updated_at", "username" },
                values: new object[] { 1, null, 1620561206, "sysadmin@gmail.com", null, "System", "Administrator", "$2a$10$ahBg0LfSIkqMuTNBhs9Vte.4hSodUEP0NZm53LVxX3LAzUK5Aam/O", true, "sa", 1620561206, "sysadmin" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "user",
                keyColumn: "id",
                keyValue: 1);
        }
    }
}
