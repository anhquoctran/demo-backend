﻿﻿namespace HappyFarm.Core.Events
{
    public class MessageReceivedEventArgs<TData>
    {
        public MessageReceivedEventArgs(TData data, string fromTopic)
        {
            Data = data;
            ReceivedTopic = fromTopic;
        }

        public TData Data { get; }

        public string ReceivedTopic { get; }
    }
}
