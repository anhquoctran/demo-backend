using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;

namespace HappyFarm.Core.Commons
{
    public static class Constants
    {
        public const double MAXIMUM_FILE_LENGTH_ALLOWED = 10485760;
        public const double MAXIMUM_FILE_EXCEL_LENGTH = 10485760;

        public const int UTC_HCM_OFFSET_SECONDS = 25200;

        public const string VERNREMQ_HOST = "";
        public const ushort VERNEMQ_PORT = 1883;

        public const ushort REDIS_PORT = 6379;
        public const string REDIS_HOST = "localhost";

        public const string STATIC_PATH = "/static";
        public const string STATIC_DIRECTORY_NAME = "wwwroot";

        public const string RESOURCE_DIRECTORY_NAME = "Resources";

        public const string LOCALE_DIRECTORY_NAME = "Locales";

        public const string DEVICE_KEY_NAME_PREFIX = "hpdev";

        public const int REDIS_DEVICE_DB = 2;
        public const int REDIS_JWT_DB = 3;

        public const int MIN_IMAGE_SIZE = 220;

        public const string DEFAULT_SYSTEM_REDIS_ACCOUNT = "sa";
        public const string DEFAULT_SYSTEM_REDIS_PASSWORD = "";

        public const string COMMAND_TOPIC = "/hpdev/+/command";
        public const string STATUS_TOPIC = "/hpdev/+/status";

        public const string SIGNALR_CONNECTIONS_KEY = "SIGNALR_CONNECTIONS";

        public static readonly string[] AllowedOrigins =
        {

        };

        public const string AllowAllOrigins = "*";

        public static readonly IDictionary<string, StringValues> SecurityHeaders = new Dictionary<string, StringValues>
        {
            { "x-xss-protection", "1; mode=block" },
            { "x-frame-options", "deny" },
            { "X-Content-Type-Options", "nosniff" },
            { "Content-Security-Policy", "self';" },
            { "X-Permitted-Cross-Domain-Policies", "master-only" },
            { "Referrer-Policy", "no-referrer" }
        };

        public static readonly IEnumerable<string> UntrustedHeaders = new[] { "Server", "X-Powered-By" };

        public static class Jwt
        {
            public const string WEB_AUTH_SECRET = "80295660-041D-40B7-9CA5-443155E4F5D3";
            public const string WEB_REFRESH_TOKEN_SECRET = "FAFB475D-BA8F-4D7C-8F2A-E02F8E1D9B32";

            public static readonly TimeSpan BlacklistTokenTimout = TimeSpan.FromDays(366);

            public static readonly TimeSpan WebTtl = TimeSpan.FromDays(365); // 10 hours

            public static readonly TimeSpan WebRefreshTtl = TimeSpan.FromDays(365 * 2); // 1 year
        }

        public static class CryptoConfiguration
        {
            public const string SALT_KEY = "429248CB-6E44-44E8-A7BB-15C9C577B211";
        }

        public static class PaginationConfiguration
        {
            public const int DEFAULT_PAGE_SIZE = 10;
            public const int DEFAULT_PAGE_NUMBER = 1;
        }


        public static class DefaultDeviceConfiguration
        {
            public const string SERVER = "";
            public const string PASSWORD = "123456";
            public const int PORT = 1883;
            public const string WIFI_SSID = "";
            public const string WIFI_PASSWORD = "";
            public const int TIMEOUT = 60;
        }

        public enum DeviceCommand
        {
            CommandStatus = 1,
            CommandControlTimer = 2,
            CommandPowerSetting = 3,
            CommandApplyControl = 4,
            CommandGateway = 5,
            CommandTimerModeSetting = 6,
            CommandUpdateSetting = 7,
            CommandSensorValues = 8,
            CommandTimeSync = 9,
            CommandLatestValue = 10,
            CommandInformation = 11,
            CommandResponse = 999,
            CommandGatewaySensor = 12
        }
    }
}
