﻿using System;
using System.Runtime.InteropServices;

namespace HappyFarm.Core.Commons
{
    public static class DateTimeUtils
    {
        private static readonly int CurrentSystemUTCOffset = TimeZoneInfo.Local.BaseUtcOffset.Hours;

        /// <summary>
        /// Total seconds in one hour
        /// </summary>
        public const long TOTAL_SECONDS_ONE_HOUR = 3600L;

        /// <summary>
        /// Total seconds in 7 hours
        /// </summary>
        public const long TOTAL_SECONDS_SEVEN_HOURS = TOTAL_SECONDS_ONE_HOUR * 7L;

        /// <summary>
        /// Convert from datetime to Unix timestamp epoch
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static int ConvertToUnixTime(DateTime dt)
        {
            if (dt == DateTime.MinValue) dt = DateTime.Now;
            var dtOffset = new DateTimeOffset(dt);
            return (int)dtOffset.ToUnixTimeSeconds();
        }

        /// <summary>
        /// Convert to Unix timestamp epoch in millis sec
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static long ConvertToUnixTimeMillis(DateTime dt)
        {
            if (dt == DateTime.MinValue) dt = DateTime.Now;
            var dtOffset = new DateTimeOffset(dt);
            return dtOffset.ToUnixTimeMilliseconds();
        }

        public static DateTime ConvertFromUnixTime(int? unixTime, int offset = 0)
        {
            if (unixTime.HasValue)
            {
                return ConvertDateTimeWithZone(DateTimeOffset.FromUnixTimeSeconds(unixTime.Value).DateTime, offset, GetTimezoneIdByOS());
            }

            var unix = ConvertToUnixTime(DateTime.Now);
            return ConvertDateTimeWithZone(DateTimeOffset.FromUnixTimeSeconds(unix).DateTime, 0, GetTimezoneIdByOS());
        }

        public static int ConvertTimezoneForUnixTime(int unixTimestamp, int offset = 0)
        {
            var dt = DateTimeOffset.FromUnixTimeSeconds(unixTimestamp);
            return (int)dt.AddHours(offset).ToUnixTimeSeconds();
        }

        /// <summary>
        /// Get timezone ID by OS
        /// </summary>
        /// <returns></returns>
        public static string GetTimezoneIdByOS()
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                return "SE Asia Standard Time"; // hande timezone id for Windows
            }

            return "Asia/Ho_Chi_Minh"; // handle timezone id for Linux/Unix
        }

        public static DateTimeOffset NowWithUtcOffset()
        {
            return ConvertDateTimeWithZone(DateTime.Now, CurrentSystemUTCOffset, GetTimezoneIdByOS());
        }

        public static long NowUnix()
        {
            return NowWithUtcOffset().ToUnixTimeSeconds();
        }

        public static DateTime ConvertDateTimeWithZone(DateTime inputTime, int fromOffset, string toZone)
        {
            // Ensure that the given date and time is not a specific kind.
            inputTime = DateTime.SpecifyKind(inputTime, DateTimeKind.Unspecified);

            var fromTimeOffset = new TimeSpan(fromOffset, 0, 0);
            var to = TimeZoneInfo.FindSystemTimeZoneById(toZone);
            var offset = new DateTimeOffset(inputTime, fromTimeOffset);
            var destination = TimeZoneInfo.ConvertTime(offset, to);
            return destination.DateTime;
        }

        public static DateTime SetUtcOffset(this DateTime dt)
        {
            return ConvertDateTimeWithZone(dt, CurrentSystemUTCOffset, GetTimezoneIdByOS());
        }

        public static DateTime SetToEndOfDay(this DateTime dt)
        {
            return dt.AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(59);
        }

        public static DateTimeOffset SetToEndOfDay(this DateTimeOffset dt)
        {
            return new DateTimeOffset(
              dt.Year,
              dt.Month,
              dt.Day,
              23, 59, 59, dt.Offset
            );
        }

        public static DateTimeOffset RemoveTime(this DateTimeOffset dt)
        {
            return new DateTimeOffset(
              dt.Year,
              dt.Month,
              dt.Day,
              0, 0, 0, dt.Offset
            );
        }
    }
}
