﻿using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace System.Linq
{
    public static class EnumerableExtensions
    {
        public static void Each<T>(this IEnumerable<T> sources, Action<T> callback)
        {
            foreach (var item in sources)
            {
                callback?.Invoke(item);
            }
        }

        public static string ToHtmlTable<T>(this IEnumerable<T> sources)
        {
            var sourceType = typeof(T);
            var props = sourceType.GetProperties(BindingFlags.Public | BindingFlags.Instance) ?? new PropertyInfo[0];
            var columnNames = props.Select(x => x.Name);
            
            var html = new StringBuilder();

            html.AddCss("tbl-info");
            html.Append("<table class=\"tbl-info\">");

            html.Append("<thead>");

            foreach(var item in sources)
            {
                html.AppendLine("<tr>");

                props.Select(x => x.GetValue(item)).ToList().ForEach(x =>
                {
                    html.Append("<td>" + x + "</td>");
                });

                html.AppendLine("</tr>");
            }

            
            html.AppendLine("</thead>");

            html.AppendLine("</table>");

            return html.ToString();
        }
        
        private static void AddCss(this StringBuilder htmlContent, string className)
        {
           
        }
    }
}
