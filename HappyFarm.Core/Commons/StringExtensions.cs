﻿using System;
using System.Globalization;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Linq.Expressions;
using System.Text;
using System.Text.RegularExpressions;

namespace HappyFarm.Core.Commons
{
    public static class StringExtensions
    {
        public static string PascalCaseToSnakeCase(this string str)
        {
            return string.Concat(str.Select((x, i) => i > 0 && char.IsUpper(x) ? "_" + x : x.ToString())).ToLowerInvariant();
        }

        public static string ToTitleCase(this string str)
        {
            var textInfor = CultureInfo.InvariantCulture.TextInfo;
            return textInfor.ToTitleCase(str);
        }

        public static string SnakeCaseToPascalCase(this string str)
        {
            if (string.IsNullOrEmpty(str)) return string.Empty;
            if (string.IsNullOrWhiteSpace(str)) return string.Empty;

            var resultBuilder = new StringBuilder();

            foreach (var c in str)
            {
                // Replace anything, but letters and digits, with space
                if (!char.IsLetterOrDigit(c))
                {
                    resultBuilder.Append(" ");
                }
                else
                {
                    resultBuilder.Append(c);
                }
            }

            var result = resultBuilder.ToString();

            // Make result string all lowercase, because ToTitleCase does not change all uppercase correctly
            result = result.ToLower();

            // Creates a TextInfo based on the "en-US" culture.
            var myTi = CultureInfo.CurrentCulture.TextInfo;

            return myTi.ToTitleCase(result).Replace(" ", string.Empty);
        }


        public static string FormatWith(this string format, object source)
        {
            if (string.IsNullOrEmpty(format))
                throw new ArgumentNullException(nameof(format));
            if (source == null)
                throw new ArgumentNullException(nameof(source));

            return Regex.Replace(format, @"{{(?<exp>[^}]+)}}", match =>
            {
                var matchName = match.Value.Replace("{{", "").Replace("}}", "");
                var p = Expression.Parameter(source.GetType(), "source");
                var sP = p.Type.GetProperties().Select(pi => pi.Name).ToList();
                var sF = p.Type.GetFields().Select(fi => fi.Name).ToList();
                sP.AddRange(sF);
                if (!sP.Contains(matchName)) return string.Empty;
                var e = DynamicExpressionParser.ParseLambda(new[] { p }, null, match.Groups["exp"].Value);
                return (e.Compile().DynamicInvoke(source) ?? "").ToString() ?? string.Empty;

            });
        }

        public static string HidePassword(this string password)
        {
            if (StringUtils.CheckIsEmptyOrWhitespaceString(password))
            {
                throw new ArgumentNullException(nameof(password));
            }

            return new string(password.Select(x => '*').ToArray());
        }
    }
}
