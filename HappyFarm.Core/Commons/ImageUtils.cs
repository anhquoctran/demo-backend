﻿using HappyFarm.Core.Commons;
using Microsoft.AspNetCore.Http;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ImageDotNet = System.Drawing.Image;
using ImageSixLabors = SixLabors.ImageSharp.Image;

namespace HappyFarm.Infrastructure.Commons
{
    public static class ImageUtils
    {
        /// <summary>
        /// Check is valid image size
        /// </summary>
        /// <param name="image"></param>
        /// <param name="minWidth"></param>
        /// <param name="minHeight"></param>
        /// <returns></returns>
        public static bool IsValidImageSize(ImageDotNet image, uint minWidth, uint minHeight)
        {
            if (image == null) return false;
            return image.Width >= minWidth || image.Height >= minHeight;
        }

        /// <summary>
        /// Try detech raw bytes buffer is image or not
        /// </summary>
        /// <param name="bytes">Raw bytes</param>
        /// <returns></returns>
        public static bool IsImage(byte[] bytes)
        {
            try
            {
                using var ms = new MemoryStream(bytes);
                ImageDotNet.FromStream(ms);
                return true;
            }
            catch (ArgumentException)
            {
                return false;
            }
        }

        /// <summary>
        /// Resize image from stream image
        /// </summary>
        /// <param name="imageStream">Image stream</param>
        /// <returns></returns>
        private static ImageSixLabors Resize(Stream imageStream)
        {
            try
            {
                var img = ImageSixLabors.Load(imageStream, out var format);
                if (img.Width <= Constants.MIN_IMAGE_SIZE || img.Height <= Constants.MIN_IMAGE_SIZE) return img;

                var result = img.Clone(image =>
                {
                    if (img.Width > img.Height)
                    {
                        image.Resize(new ResizeOptions
                        {
                            Size = new Size(Constants.MIN_IMAGE_SIZE, 0),
                        });
                    }
                    else
                    {
                        image.Resize(0, Constants.MIN_IMAGE_SIZE);
                    }
                });
                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Process and save an image from HTTP Form request
        /// </summary>
        /// <param name="file"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static string ProcessAndSaveImage(IFormFile file, string basePath, string filename = null)
        {
            try
            {
                if (file == null) return null;
                var inStream = file.OpenReadStream();
                var containsExt = !string.IsNullOrEmpty(Path.GetExtension(filename));
                filename ??= $"{StringUtils.GetGuid()}";
                filename = !containsExt ? filename + ".jpg" : filename;
                var fullPath = Path.Combine(FileUtils.GetFullImageResourceDirectoryName(basePath), filename);
                var dbPath = Path.Combine("/", FileUtils.RelativeResourceImageRoute, filename).Replace(@"\", "/");

                var resizedImage = Resize(inStream);
                if (resizedImage != null)
                {
                    resizedImage.Save(fullPath);
                }
                else
                {
                    ImageSixLabors.Load(inStream).Save(fullPath);
                }

                return dbPath;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Save images from HTTP form-data
        /// </summary>
        /// <param name="files">List of file parse from form-data</param>
        /// <returns></returns>
        public static IEnumerable<string> ProcessAndSaveImages(string basePath, params IFormFile[] files)
        {
            if (files != null && files.Length > 0 && files.All(f => f != null))
            {
                try
                {
                    var urls = new List<string>();
                    foreach (IFormFile file in files)
                    {
                        var dbPath = ProcessAndSaveImage(file, basePath);
                        if (!string.IsNullOrEmpty(dbPath))
                        {
                            urls.Add(dbPath);
                        }

                    }
                    return urls;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    return Enumerable.Empty<string>();
                }
            }

            return Enumerable.Empty<string>();

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static bool IsImage(string filePath)
        {
            try
            {
                var fileBinary = File.ReadAllBytes(filePath);
                return FileUtils.ImageHeaders.Any(x => x.SequenceEqual(fileBinary.Take(x.Length)));
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="imageURL"></param>
        /// <param name="checksum"></param>
        /// <returns></returns>
        public static object GetImageInfo(string basePath, string imageURL, string checksum)
        {
            try
            {
                var imageURI = new Uri(imageURL);
                var filename = Path.GetFileName(imageURI.LocalPath);
                var absPath = Path.Combine(FileUtils.GetFullImageResourceDirectoryName(basePath), filename);
                var existImage = File.Exists(absPath);
                if (existImage && IsImage(absPath))
                {
                    var img = ImageDotNet.FromFile(absPath);
                    return new
                    {
                        url = imageURL,
                        width = img.Width,
                        height = img.Height,
                        checksum = StringUtils.CheckIsEmptyOrWhitespaceString(checksum) ? string.Empty : checksum
                    };
                }

                return string.Empty;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return string.Empty;
            }
        }
    }
}
