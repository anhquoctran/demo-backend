﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace HappyFarm.Core.Commons
{
    public static class FileUtils
    {
        #region ReadOnly fields

        public static readonly string[] ImageMimeTypes = {
            "image/jpeg", "image/gif", "image/png"
         };

        public static readonly string[] FileExtensionsToDelete =
        {
            ".xlsx", ".xls", ".tmp", ".zip", ".7z"
        };

        public static readonly string[] ImageExtensions =
        {
            ".jpg", ".jpeg", ".png"
        };

        private static readonly string RelativeResourceDirectoryName = Path.Combine(Constants.STATIC_DIRECTORY_NAME, "images");
        public static readonly string RelativeResourceImageRoute = Path.Combine(Constants.STATIC_PATH, "images");

        public static readonly IEnumerable<byte[]> ImageHeaders = new[]
        {
            new byte[] { 0xff, 0xd8, 0xff, 0xe0 },
            new byte[] { 0xff, 0xd8, 0xff, 0xe1 },
            new byte[] { 0xff, 0xd8, 0xff, 0xe2 },
            new byte[] { 0xff, 0xd8, 0xff, 0xe8 },
            new byte[] { 0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a },
            new byte[] { 0x47, 0x49, 0x46, 0x38, 0x37, 0x61 },
            new byte[] { 0x47, 0x49, 0x46, 0x38, 0x39,0x61 }
        };

        #endregion

        #region Constants

        public const string EXCEL_CONTENT_TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

        #endregion

        #region Public methods

        public static string GetFullImageResourceDirectoryName(string basePath) => Path.Combine(basePath, RelativeResourceDirectoryName);

        public static string GetFullAssetsResourceDirectoryName(string basePath) => Path.Combine(basePath, Constants.STATIC_DIRECTORY_NAME, "assets");


        /// <summary>
        /// Check file is locked (used by another process)
        /// </summary>
        /// <param name="file">File info to check</param>
        /// <returns></returns>
        public static bool IsFileLocked(FileInfo file)
        {
            try
            {
                using var stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
                stream.Close();
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }

            //file is not locked
            return false;
        }

        /// <summary>
        /// Delete image resources by file path
        /// </summary>
        /// <param name="filePath">Files path</param>
        /// <returns></returns>
        public static bool DeleteImageResources(string basePath, params string[] filePath)
        {
            try
            {
                foreach (var path in filePath)
                {
                    var fullPath = Path.GetFullPath(Path.Combine(basePath, path));
                    if (File.Exists(fullPath))
                    {
                        File.Delete(fullPath);
                    }
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        /// <summary>
        /// List file
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="searchPattern"></param>
        /// <returns></returns>
        public static IEnumerable<string> ListingFilesInFolder(string directory, string searchPattern)
        {
            if (string.IsNullOrEmpty(directory))
            {
                return Enumerable.Empty<string>();
            }
            var files = Directory.EnumerateFiles(directory, searchPattern, SearchOption.TopDirectoryOnly);
            return files;
        }

        /// <summary>
        /// Check is image valid dimension
        /// </summary>
        /// <param name="file"></param>
        /// <param name="height"></param>
        /// <param name="width"></param>
        /// <returns></returns>
        public static bool IsImageValidDimension(IFormFile file, double height, double width)
        {
            if (file == null) return false;
            try
            {
                if (!ImageMimeTypes.Contains(file.ContentType)) return true;
                using var image = Image.FromStream(file.OpenReadStream());
                return height >= image.Height || width >= image.Width;

            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Get random file name for excel file
        /// </summary>
        /// <returns></returns>
        public static string GetRandomExcelFilename()
        {
            var now = DateTime.Now.ToString("yyyyMMdd_HHmmss");
            var random = StringUtils.GenerateRandomString(6);
            return $"export_460_{now}_{random}.xlsx";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static bool Deletable(string filename)
        {
            if (StringUtils.CheckIsEmptyOrWhitespaceString(filename)) return false;
            if (!File.Exists(filename)) return false;
            return true;
        }

        public static async Task<bool> SaveFirmware(IFormFile file, string basePath)
        {
            try
            {
                var fileName = "firmware.bin";
                var fullpath = Path.Combine(basePath, Constants.STATIC_DIRECTORY_NAME, "update_firmware");
                if (!Directory.Exists(fullpath)) Directory.CreateDirectory(fullpath);
                var filePath = Path.Combine(fullpath, fileName);
                using var fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                await file.CopyToAsync(fs);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return false;
            }
        }

        public static bool DeleteFirmware(string basePath)
        {
            try
            {
                var fileName = "firmware.bin";
                var fullpath = Path.Combine(basePath, Constants.STATIC_DIRECTORY_NAME, "update_firmware");
                if (!Directory.Exists(fullpath)) Directory.CreateDirectory(fullpath);
                var filePath = Path.Combine(fullpath, fileName);
                if (!File.Exists(filePath)) return true;
                File.Delete(filePath);

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return false;
            }
        }

        public static void InitStaticFolder(string basePath)
        {
            try
            {
                var resourcePath = Path.Combine(basePath, Constants.STATIC_DIRECTORY_NAME);
                var resourceDirectory = Directory.CreateDirectory(resourcePath);
                const string imageFolder = "images";
                const string otherAssets = "assets";
                var imgFolder = Path.Combine(resourceDirectory.FullName, imageFolder);
                var assetsFolder = Path.Combine(resourceDirectory.FullName, otherAssets);
                Directory.CreateDirectory(imgFolder);
                Directory.CreateDirectory(assetsFolder);
            }
            catch (UnauthorizedAccessException uauthEx)
            {
                Console.WriteLine(uauthEx.Message);
            }

        }
    }

    #endregion
}
