﻿/*
 * Copyright [2020] [anhquoctran]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

using PhoneNumbers;
using System;
using System.Collections.Generic;

namespace HappyFarm.Core.Commons
{

    /// <summary>
    /// Handle validation phone number
    /// </summary>
    public static class PhoneNumber
    {
        /// <summary>
        /// Check string is valid phone number, by country code
        /// </summary>
        /// <param name="countryCode">Country code/Region code</param>
        /// <param name="phoneNumberLike">Phone number in string</param>
        /// <returns></returns>
        public static bool IsValidPhonenumber(string countryCode, string phoneNumberLike)
        {
            countryCode = countryCode.Trim().ToUpperInvariant();

            if (phoneNumberLike.StartsWith("00"))
            {
                phoneNumberLike = "+" + phoneNumberLike.Remove(0, 2);
            }

            try
            {
                var phoneProto = PhoneNumberUtil.GetInstance().Parse(phoneNumberLike, countryCode);
                return PhoneNumberUtil.GetInstance().IsValidNumberForRegion(phoneProto, countryCode);
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Parse phone number from string, by country code
        /// </summary>
        /// <param name="countryCode">Country code/Region code</param>
        /// <param name="phone">Phone number in string</param>
        /// <returns></returns>
        public static string ParsePhoneNumber(string countryCode, string phone)
        {
            try
            {
                if (StringUtils.CheckIsEmptyOrWhitespaceString(countryCode) || StringUtils.CheckIsEmptyOrWhitespaceString(phone)) return string.Empty;

                var phoneUtil = PhoneNumberUtil.GetInstance();
                var phoneProto = phoneUtil.Parse(phone, countryCode);
                if (phoneUtil.IsValidNumber(phoneProto))
                {
                    return phoneUtil.Format(phoneProto, PhoneNumberFormat.NATIONAL).Replace(" ", string.Empty);
                }
                return $"{countryCode}{phone}";
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        /// <summary>
        /// Get country code supported
        /// </summary>
        /// <returns></returns>
        public static HashSet<string> GetCountryCodesSupported()
        {
            return PhoneNumberUtil.GetInstance().GetSupportedRegions();
        }

        /// <summary>
        /// Normalize a phone number from raw string
        /// </summary>
        /// <param name="phoneNumber">Phone number raw</param>
        /// <returns></returns>
        public static string NormalizePhoneNumber(this string phoneNumber)
        {
            if (StringUtils.CheckIsEmptyOrWhitespaceString(phoneNumber)) return null;

            var chars = new[] { " ", ".", ",", "-", "_", "+", "~", "(", ")", "=", "|", "\\", "[", "]", "<", ">", "?", "/", ":" };

            foreach (var c in chars)
            {
                phoneNumber = phoneNumber.Replace(c, string.Empty);
            }
            return phoneNumber;
        }
    }
}
