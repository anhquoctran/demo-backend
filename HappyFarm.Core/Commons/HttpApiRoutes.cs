﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HappyFarm.Core.Commons
{
    public static class HttpApiRoutes
    {
        public const string HEALTH_CHECK = "/health";

        public const string API_PREFIX = "/api/v1";

        public const string CLUSTER_STATUS_INFORMATION = API_PREFIX + "/cluster/show";

        public const string SESSION_INFORMATION = API_PREFIX + "/session/show";

        public const string ALL_INSTALLED_LISTENERS = API_PREFIX + "/listener/show";

        public const string ALL_INSTALLED_PLUGINS = API_PREFIX + "/plugin/show";

    }
}
