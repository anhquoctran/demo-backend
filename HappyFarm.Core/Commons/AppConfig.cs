﻿using HappyFarm.Core.Models.AppConfig;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Text;

namespace HappyFarm.Core.Commons
{
    public class AppConfig
    {
        public static SystemAppConfig Config;
        private static FileSystemWatcher Fsw;

        private static string EndpointPath;

        private const string CONFIG_FILE_WATCH = "config.json";

        public static void LoadConfig(string endPointPath)
        {
            EndpointPath = endPointPath;
            if (ReadConfigFile())
            {
                InitWatcher();
            }
        }

        private static void InitWatcher()
        {
            Fsw = new FileSystemWatcher
            {
                Path = EndpointPath,
                Filter = CONFIG_FILE_WATCH,

                NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite
                        | NotifyFilters.FileName | NotifyFilters.DirectoryName | NotifyFilters.CreationTime
            };

            Fsw.Changed += Fsw_Changed;
            Fsw.Deleted += Fsw_Deleted;

            Fsw.EnableRaisingEvents = true;
        }

        private static void Fsw_Deleted(object sender, FileSystemEventArgs e)
        {
            ReadConfigFile();
        }

        private static void Fsw_Changed(object sender, FileSystemEventArgs e)
        {
            ReadConfigFile();
        }

        private static bool ReadConfigFile()
        {
            try
            {
                var filePath = Path.Combine(EndpointPath, CONFIG_FILE_WATCH);
                if (!File.Exists(filePath))
                {
                    var defaultConfig = SystemAppConfig.GetDefault();
                    var jsonConfig = JsonConvert.SerializeObject(defaultConfig);

                    File.WriteAllText(filePath, jsonConfig);
                }

                var text = File.ReadAllText(filePath, Encoding.UTF8);
                
                if (StringUtils.CheckIsEmptyOrWhitespaceString(text) || !StringUtils.IsValidJson(text))
                {
                    throw new ArgumentNullException("Invalid JSON token");
                }
                var config = JsonConvert.DeserializeObject<SystemAppConfig>(text);
                if (config == null) return false;
                Config = config;

                return true ;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return false;
            }
        }
    }
}
