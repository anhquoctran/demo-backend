﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace HappyFarm.Core.Commons
{
    public static class RegexUtils
    {
        private const string PATTERN = @"\d+";

        public static bool TryParseDigitsFromString(string input, out IDictionary<int, int> results)
        {

            if (!StringUtils.CheckIsEmptyOrWhitespaceString(input))
            {
                var regexMatch = Regex.Matches(input, PATTERN);
                if (regexMatch.Count > 0)
                {
                    results = new Dictionary<int, int>();
                    foreach(Match m in regexMatch)
                    {
                        if (m.Success)
                        {
                            results[m.Index] = Convert.ToInt32(m.Value);
                        }
                        
                    }
                    return true;
                }

                results = new Dictionary<int, int>();
                return false;
            }

            results = new Dictionary<int, int>();
            return false;
        }

        public static bool TryParseDigitFromString(string input, out KeyValuePair<int, int> result)
        {
            if (!StringUtils.CheckIsEmptyOrWhitespaceString(input))
            {
                var match = Regex.Match(input, PATTERN);
                if (match.Success)
                {
                    result = new KeyValuePair<int, int>(match.Index, Convert.ToInt32(match.Value));
                    return true;
                }

                result = new KeyValuePair<int, int>();
                return false;
            }

            result = new KeyValuePair<int, int>();
            return false;
        }
    }
}
