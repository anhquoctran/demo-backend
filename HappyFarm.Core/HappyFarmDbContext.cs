﻿using HappyFarm.Core.Commons;
using HappyFarm.Core.Interfaces;
using HappyFarm.Core.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace HappyFarm.Core
{
    public class HappyFarmDbContext : DbContext
    {
        public HappyFarmDbContext(DbContextOptions<HappyFarmDbContext> options) : base(options) { }

        public DbSet<Farm> Farms { get; set; }
        public DbSet<Gateway> Gateways { get; set; }
        public DbSet<SensorNode> SensorNodes { get; set; }
        public DbSet<SensorValue> SensorValues { get; set; }
        public DbSet<User> Users { get; set; }


        private void InternalSaveChanges()
        {
            var entries = ChangeTracker.Entries()
               .Where(e => (e.Entity is IHasCreationTime || e.Entity is IHasUpdateTime) && (e.State == EntityState.Added || e.State == EntityState.Modified));

            foreach (var entityEntry in entries)
            {
                if (entityEntry.Entity is IHasCreationTime creationEntity)
                {
                    if (entityEntry.State == EntityState.Added)
                    {
                        if (!creationEntity.CreatedAt.HasValue)
                        {
                            creationEntity.CreatedAt = DateTimeUtils.ConvertToUnixTime(DateTime.Now);
                        }
                    }
                }

                if (entityEntry.Entity is IHasUpdateTime updateEntity)
                {
                    updateEntity.UpdatedAt = DateTimeUtils.ConvertToUnixTime(DateTime.Now);
                }
            }
        }

        public override int SaveChanges()
        {
            InternalSaveChanges();
            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            InternalSaveChanges();
            return base.SaveChangesAsync(cancellationToken);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("user");

                entity.HasKey(x => x.Id);

                entity.Property(x => x.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("id");

                entity.HasIndex(x => x.Id)
                    .IsUnique();

                entity.Property(x => x.Firstname)
                    .IsRequired()
                    .HasColumnName("firstname")
                    .HasColumnType("varchar(60)")
                    .HasMaxLength(60);

                entity.Property(x => x.Lastname)
                    .IsRequired()
                    .HasColumnName("lastname");

                entity.Property(x => x.Username)
                    .HasColumnName("username")
                    .HasColumnType("varchar(60)")
                    .HasMaxLength(60)
                    .IsRequired();

                entity.Property(x => x.Avatar)
                    .HasColumnName("avatar")
                    .HasMaxLength(255)
                    .HasColumnType("varchar(255)");

                entity.HasIndex(x => x.Username)
                    .IsUnique();

                entity.Property(x => x.Password)
                    .HasColumnName("password")
                    .HasColumnType("varchar(255)")
                    .HasMaxLength(255)
                    .IsRequired();

                entity.Property(x => x.Email)
                    .HasColumnName("email")
                    .HasColumnType("varchar(255)")
                    .HasMaxLength(255)
                    .IsRequired();

                entity.HasIndex(x => x.Email)
                    .IsUnique();

                entity.Property(x => x.Role)
                    .HasColumnName("role")
                    .HasColumnType("varchar(3)")
                    .HasMaxLength(3)
                    .IsRequired();

                entity.Property(x => x.FarmId)
                    .HasColumnName("farm_id");

                entity.Property(x => x.PreventDelete)
                    .HasColumnName("prevent_delete")
                    .HasDefaultValue(false);

                entity.Property(x => x.IsDeactivated)
                    .HasColumnName("deactivated")
                    .HasDefaultValue(false);

                entity.Property(x => x.CreatedAt)
                    .HasColumnName("created_at")
                    .HasColumnType("int")
                    .IsRequired(false);

                entity.Property(x => x.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasColumnType("int")
                    .IsRequired(false);

                entity.HasData(new User {
                    Id = 1,
                    Firstname = "System", 
                    Lastname = "Administrator", 
                    Email = "sysadmin@gmail.com", 
                    Role = "sa",
                    IsDeactivated = false,
                    PreventDelete = true,
                    Username = "sysadmin",
                    Password = "$2a$10$ahBg0LfSIkqMuTNBhs9Vte.4hSodUEP0NZm53LVxX3LAzUK5Aam/O",
                    Avatar = null,
                    Farm = null,
                    FarmId = null,
                    UpdatedAt = DateTimeUtils.ConvertToUnixTime(DateTime.Now),
                    CreatedAt = DateTimeUtils.ConvertToUnixTime(DateTime.Now)
                }, new User {
                    Id = 2,
                    Firstname = "System",
                    Lastname = "Manager",
                    Email = "manager1@gmail.com",
                    Role = "sa",
                    IsDeactivated = false,
                    PreventDelete = true,
                    Username = "manager1",
                    Password = "$2a$10$ahBg0LfSIkqMuTNBhs9Vte.4hSodUEP0NZm53LVxX3LAzUK5Aam/O",
                    Avatar = null,
                    Farm = null,
                    FarmId = null,
                    UpdatedAt = DateTimeUtils.ConvertToUnixTime(DateTime.Now),
                    CreatedAt = DateTimeUtils.ConvertToUnixTime(DateTime.Now)
                });
            });

            modelBuilder.Entity<Farm>(entity =>
            {
                entity.ToTable("farm");

                entity.HasKey(x => x.Id);

                entity.Property(x => x.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.HasIndex(x => x.Id)
                    .IsUnique();

                entity.Property(x => x.Name)
                    .HasColumnName("name")
                    .HasColumnType("varchar(60)")
                    .IsRequired();

                entity.Property(x => x.Phone)
                    .HasColumnName("phone")
                    .HasColumnType("varchar(20)")
                    .HasMaxLength(20)
                    .IsRequired();


                entity.HasIndex(x => x.Phone)
                    .IsUnique();

                entity.Property(x => x.RegionCode)
                    .HasColumnName("region_code")
                    .HasMaxLength(3)
                    .HasColumnType("varchar(3)")
                    .IsRequired();

                entity.Property(x => x.CreatedAt)
                    .HasColumnName("created_at");

                entity.Property(x => x.UpdatedAt)
                    .HasColumnName("updated_at");

                entity.HasMany(x => x.Gateways)
                    .WithOne(x => x.Farm)
                    .HasForeignKey(x => x.FarmId)
                    .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<Gateway>(entity =>
            {
                entity.ToTable("gateway");

                entity.HasKey(x => x.Id);

                entity.Property(x => x.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.HasIndex(x => x.Id)
                    .IsUnique();

                entity.Property(x => x.Name)
                    .HasColumnName("name")
                    .IsRequired();

                entity.HasIndex(x => x.Name)
                    .IsUnique();

                entity.Property(x => x.Code)
                    .HasColumnName("code");

                entity.Property(x => x.Config)
                    .HasColumnName("config");

                entity.Property(x => x.FarmId)
                    .HasColumnName("farm_id");

                entity.Property(x => x.Password)
                    .HasColumnName("password");

                entity.Property(x => x.CreatedAt)
                    .HasColumnName("created_at");

                entity.Property(x => x.UpdatedAt)
                    .HasColumnName("updated_at");

                entity.HasOne(x => x.Farm)
                    .WithMany(x => x.Gateways)
                    .HasForeignKey(x => x.FarmId)
                    .OnDelete(DeleteBehavior.Restrict);

                entity.HasMany(x => x.SensorNodes)
                    .WithOne(x => x.Gateway)
                    .HasForeignKey(x => x.GatewayId);
            });

            modelBuilder.Entity<SensorNode>(entity =>
            {
                entity.ToTable("sensor_node");

                entity.HasKey(x => x.Id);

                entity.Property(x => x.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.HasIndex(x => x.Id)
                    .IsUnique();

                entity.Property(x => x.Name)
                    .HasColumnName("name")
                    .IsRequired();

                entity.HasIndex(x => x.Name)
                    .IsUnique();

                entity.HasIndex(x => x.Code)
                    .IsUnique();

                entity.Property(x => x.Code)
                    .HasColumnName("code")
                    .IsRequired();

                entity.Property(x => x.GatewayId)
                    .HasColumnName("gateway_id");

                entity.Property(x => x.CreatedAt)
                    .HasColumnName("created_at");

                entity.Property(x => x.UpdatedAt)
                    .HasColumnName("updated_at");

                entity.HasMany(x => x.SensorValues)
                    .WithOne(x => x.SensorNode)
                    .HasForeignKey(x => x.NodeId)
                    .OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(x => x.Gateway)
                    .WithMany(x => x.SensorNodes)
                    .HasForeignKey(x => x.GatewayId)
                    .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<SensorValue>(entity =>
            {
                entity.ToTable("sensor_value");

                entity.HasKey(x => x.Id);

                entity.Property(x => x.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.HasIndex(x => x.Id)
                    .IsUnique();

                entity.Property(x => x.Temperature)
                    .HasColumnName("temp");

                entity.Property(x => x.AirHumidity)
                    .HasColumnName("air_humidity");

                entity.Property(x => x.PhScale)
                    .HasColumnName("ph_scale");

                entity.Property(x => x.LightIntensity)
                    .HasColumnName("light_intensity");

                entity.Property(x => x.SoilHumidity)
                    .HasColumnName("soil_humidity");

                entity.Property(x => x.WindSpeed)
                    .HasColumnName("wind_speed");

                entity.Property(x => x.RainFall)
                    .HasColumnName("rainfall");

                entity.Property(x => x.Tds)
                    .HasColumnName("tds");

                entity.Property(x => x.NodeId)
                    .HasColumnName("node_id");

                entity.Property(x => x.CreatedAt)
                    .HasColumnName("created_at");

                entity.HasOne(x => x.SensorNode)
                    .WithMany(x => x.SensorValues)
                    .HasForeignKey(x => x.NodeId)
                    .OnDelete(DeleteBehavior.Restrict);
            });

            
        }
    }
}
