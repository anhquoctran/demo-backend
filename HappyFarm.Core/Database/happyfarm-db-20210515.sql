-- MySQL dump 10.13  Distrib 8.0.24, for Win64 (x86_64)
--
-- Host: localhost    Database: happyfarm
-- ------------------------------------------------------
-- Server version	8.0.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `farm`
--

DROP TABLE IF EXISTS `farm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `farm` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `region_code` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `updated_at` int DEFAULT NULL,
  `created_at` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IX_farm_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `farm`
--

LOCK TABLES `farm` WRITE;
/*!40000 ALTER TABLE `farm` DISABLE KEYS */;
INSERT INTO `farm` VALUES (1,'Farm1','Hue','VN','02344567890',1620566328,1620566328),(2,'tu ha','tu ha','VN','0905337564',1620872044,1620566328),(69,'FarmAVVV','Dia chi','VN','02343544024',1621068448,1621068448),(70,'FarmAVVVAA','Dia chifggdfg','VN','02345556789',1621068510,1621068510),(88,'TestLocal','Test','VN','02340403000',1621068830,1621068830);
/*!40000 ALTER TABLE `farm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gateway`
--

DROP TABLE IF EXISTS `gateway`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gateway` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `password` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `farm_id` int NOT NULL,
  `config` json DEFAULT NULL,
  `created_at` int DEFAULT NULL,
  `updated_at` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IX_gateway_id` (`id`),
  UNIQUE KEY `IX_gateway_name` (`name`),
  KEY `IX_gateway_farm_id` (`farm_id`),
  CONSTRAINT `FK_gateway_farm_farm_id` FOREIGN KEY (`farm_id`) REFERENCES `farm` (`id`) ON DELETE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gateway`
--

LOCK TABLES `gateway` WRITE;
/*!40000 ALTER TABLE `gateway` DISABLE KEYS */;
INSERT INTO `gateway` VALUES (1,'KV1','DV001','yQFWCOsc',1,'{\"port\": 1883, \"server\": \"139.180.130.67\", \"timeout\": 60, \"local_ip\": \"192.168.1.2\", \"wifi_ssid\": \"HappyFarm\", \"pass_system\": \"123456\", \"wifi_version\": null, \"wifi_password\": \"12345679\", \"firmware_version\": \"\"}',1620566350,1620566350),(2,'KV2','DV002','A6X5r0zw',1,'{\"port\": 1883, \"server\": \"139.180.130.67\", \"timeout\": 60, \"local_ip\": \"192.168.1.2\", \"wifi_ssid\": \"HappyFarm\", \"pass_system\": \"123456\", \"wifi_version\": null, \"wifi_password\": \"12345679\", \"firmware_version\": \"\"}',1620566362,1620566362),(3,'tu ha 1','123','OxP8f63x',2,'{\"port\": 1883, \"server\": \"139.180.130.67\", \"timeout\": 60, \"local_ip\": \"192.168.1.2\", \"wifi_ssid\": \"HappyFarm\", \"pass_system\": \"123456\", \"wifi_version\": null, \"wifi_password\": \"12345679\", \"firmware_version\": \"\"}',1620870756,1620870756),(10,'test','Dev02','TUy7e6lQ',70,'{\"port\": 1883, \"server\": \"139.180.130.67\", \"timeout\": 60, \"local_ip\": \"192.168.1.2\", \"wifi_ssid\": \"HappyFarm\", \"pass_system\": \"123456\", \"wifi_version\": null, \"wifi_password\": \"12345679\", \"firmware_version\": \"\"}',1621068862,1621068862),(11,'huong so','DV-HUONGSO','DjKRR7XA',1,'{\"port\": 1883, \"server\": \"139.180.130.67\", \"timeout\": 60, \"local_ip\": \"192.168.1.2\", \"wifi_ssid\": \"HappyFarm\", \"pass_system\": \"123456\", \"wifi_version\": null, \"wifi_password\": \"12345679\", \"firmware_version\": \"\"}',1621068928,1621068928);
/*!40000 ALTER TABLE `gateway` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sensor_node`
--

DROP TABLE IF EXISTS `sensor_node`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sensor_node` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `gateway_id` int NOT NULL,
  `updated_at` int DEFAULT NULL,
  `created_at` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IX_sensor_node_code` (`code`),
  UNIQUE KEY `IX_sensor_node_id` (`id`),
  UNIQUE KEY `IX_sensor_node_name` (`name`),
  KEY `IX_sensor_node_gateway_id` (`gateway_id`),
  CONSTRAINT `FK_sensor_node_gateway_gateway_id` FOREIGN KEY (`gateway_id`) REFERENCES `gateway` (`id`) ON DELETE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sensor_node`
--

LOCK TABLES `sensor_node` WRITE;
/*!40000 ALTER TABLE `sensor_node` DISABLE KEYS */;
INSERT INTO `sensor_node` VALUES (1,'Lo1','L1',1,1620566388,1620566388),(2,'Lo2','L2',2,1620566400,1620566400),(3,'tu ha 123','123',3,1620870801,1620870801);
/*!40000 ALTER TABLE `sensor_node` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sensor_value`
--

DROP TABLE IF EXISTS `sensor_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sensor_value` (
  `id` int NOT NULL AUTO_INCREMENT,
  `temp` decimal(65,30) NOT NULL,
  `air_humidity` decimal(65,30) NOT NULL,
  `ph_scale` decimal(65,30) NOT NULL,
  `light_intensity` decimal(65,30) NOT NULL,
  `soil_humidity` decimal(65,30) NOT NULL,
  `wind_speed` decimal(65,30) NOT NULL,
  `rainfall` decimal(65,30) NOT NULL,
  `tds` decimal(65,30) NOT NULL,
  `node_id` int NOT NULL,
  `created_at` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IX_sensor_value_id` (`id`),
  KEY `IX_sensor_value_node_id` (`node_id`),
  CONSTRAINT `FK_sensor_value_sensor_node_node_id` FOREIGN KEY (`node_id`) REFERENCES `sensor_node` (`id`) ON DELETE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sensor_value`
--

LOCK TABLES `sensor_value` WRITE;
/*!40000 ALTER TABLE `sensor_value` DISABLE KEYS */;
/*!40000 ALTER TABLE `sensor_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `lastname` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `firstname` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `role` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `farm_id` int DEFAULT NULL,
  `prevent_delete` tinyint(1) NOT NULL DEFAULT '0',
  `deactivated` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` int DEFAULT NULL,
  `updated_at` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IX_user_email` (`email`),
  UNIQUE KEY `IX_user_id` (`id`),
  UNIQUE KEY `IX_user_username` (`username`),
  KEY `IX_user_farm_id` (`farm_id`),
  CONSTRAINT `FK_user_farm_farm_id` FOREIGN KEY (`farm_id`) REFERENCES `farm` (`id`) ON DELETE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'sa','$2a$10$NZjU9RwrgLPZb3E58aCvIebRsks5xf9Wh2Av8.bX4guQ1oioFrdCe','sysadmin@gmail.com','Administrator','System',NULL,'sa',NULL,1,0,1620561206,1620561206),(2,'manager1','$2a$10$1NAudyJAdxdL7JFEXWyseeFiEwtCXlxRXPrgdhVunu6Sz3O2gN/2m','manager1@gmail.com','Manager','Test',NULL,'m',1,0,0,1620567642,1620567642),(3,'tuha','$2a$10$qO4Z4uMOUpDKypzJA9X4WeZGwOXiNRtWRpZYALbVqs3SdBUnS.Cny','hongthiet1994@gmail.com','tu ha','farm',NULL,'m',1,0,0,1620618520,1620618520),(4,'nguyenvanquy','$2a$10$4IKLo92LyJ4vgERZbJRSOu6dNE9vyN.PyLIsxNvBcVdY9zjDXqKp2','nguyenvanquy@huaf.edu.vn','Nguyá»…n VÄƒn','Quy',NULL,'m',2,0,0,1620870410,1620870410);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-15 16:02:02
