﻿namespace HappyFarm.Core.Enums
{
    public enum TimerModeState
    {
        Manual = 0,
        Automatic = 1
    }
}
