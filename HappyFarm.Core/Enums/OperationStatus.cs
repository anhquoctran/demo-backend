﻿namespace HappyFarm.Core.Enums
{
    public enum OperationStatus
    {
        Succeed,
        Failed, 
        Exception, 
        NotFound, 
        AccessDenied, 
        NotAuthorized, 
        WrongPassword,
        Incorrect,

    }
}
