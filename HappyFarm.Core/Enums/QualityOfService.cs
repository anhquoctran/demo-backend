﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HappyFarm.Core.Enums
{
    public enum QualityOfService
    {
        AtmostOnce = 0,
        AtleastOnce = 1,
        ExactlyOnce = 2
    }
}
