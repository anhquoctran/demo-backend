﻿using HappyFarm.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace HappyFarm.Core.Models
{
    public class Gateway : IEntity, IHasCreationTime, IHasUpdateTime
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        [Column("password")]
        public string Password { get; set; }

        public int FarmId { get; set; }

        public JsonObject<Config> Config { get; set; }

        public int? CreatedAt { get; set; }

        public int? UpdatedAt { get; set; }

        public virtual Farm Farm { get; set; }

        [InverseProperty("Gateway")]
        public virtual ICollection<SensorNode> SensorNodes { get; set; }
    }
}
