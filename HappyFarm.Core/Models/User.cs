﻿using HappyFarm.Core.Interfaces;

namespace HappyFarm.Core.Models
{
    public class User : IEntity, IHasCreationTime, IHasUpdateTime
    {
        public const string ADMIN_ROLE = "sa";
        public const string MANAGER_ROLE = "m";

        public static readonly string[] Roles = { ADMIN_ROLE, MANAGER_ROLE };

        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Lastname { get; set; }
        public string Firstname { get; set; }
        public string Avatar { get; set; }
        public string Role { get; set; }
        public int? FarmId { get; set; }
        public bool PreventDelete { get; set; }
        public bool IsDeactivated { get; set; }
        public int? CreatedAt { get; set; }
        public int? UpdatedAt { get; set; }

        public virtual Farm Farm { get; set; }
    }
}
