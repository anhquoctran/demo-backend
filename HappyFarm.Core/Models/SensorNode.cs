﻿using HappyFarm.Core.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace HappyFarm.Core.Models
{
    public class SensorNode : IEntity, IHasCreationTime, IHasUpdateTime
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int GatewayId { get; set; }
        public virtual Gateway Gateway { get; set; }

        [InverseProperty("SensorNode")]
        public virtual ICollection<SensorValue> SensorValues { get; set; }
        public int? UpdatedAt { get; set; }
        public int? CreatedAt { get; set; }
        
    }
}
