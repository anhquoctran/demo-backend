﻿using HappyFarm.Core.Interfaces;

namespace HappyFarm.Core.Models
{
    public class PersonalAccessToken : IEntity, IFullAuditable
    {
        public int Id { get; set; }

        public int UserId { get; set; }
        
        public string Name { get; set; }

        public string Token { get; set; }

        public string Fingerprint { get; set; }

        public string Scopes { get; set; }

        public int? LastUsed { get; set; }

        public int? ExpiresOn { get; set; }

        public int? CreatedAt { get; set; }

        public int? UpdatedAt { get; set; }
    }
}
