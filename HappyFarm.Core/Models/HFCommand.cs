﻿using Newtonsoft.Json;

namespace HappyFarm.Core.Models
{
    public class HFCommand<TData, TCommandKey>
    {
        [JsonProperty("cmd")]
        public TCommandKey Command { get; set; }

        [JsonProperty("data", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public TData Data { get; set; }

        [JsonProperty("uuid", DefaultValueHandling = DefaultValueHandling.Ignore, NullValueHandling = NullValueHandling.Ignore)]
        public string ConnectionId { get; set; }
    }

    public class HFCommand<TData>: HFCommand<TData, int> { }
}
