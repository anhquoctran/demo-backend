﻿using HappyFarm.Core.Interfaces;

namespace HappyFarm.Core.Models
{
    public class SensorValue : IEntity, IHasCreationTime
    {
        public int Id { get; set; }

        public decimal Temperature { get; set; }
        public decimal AirHumidity { get; set; }
        public decimal PhScale { get; set; }
        public decimal LightIntensity { get; set; }
        public decimal SoilHumidity { get; set; }
        public decimal WindSpeed { get; set; }
        public decimal RainFall { get; set; }
        public decimal Tds { get; set; }

        public int NodeId { get; set; }
        public int? CreatedAt { get; set; }

        public virtual SensorNode SensorNode { get; set; }
    }
}
