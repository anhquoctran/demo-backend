﻿using HappyFarm.Core.Commons;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations.Schema;

namespace HappyFarm.Core.Models
{
    public class Config
    {
        [JsonProperty("port"), Column("port")]
        public ushort? Port { get; set; }

        [JsonProperty("timeout"), Column("timeout")]
        public uint? Timeout { get; set; }

        [JsonProperty("server"), Column("server")]
        public string Server { get; set; }

        [JsonProperty("local_ip"), Column("local_ip")]
        public string LocalIp { get; set; }

        [JsonProperty("wifi_ssid"), Column("wifi_ssid")]
        public string WiFiSsid { get; set; }

        [JsonProperty("wifi_password"), Column("wifi_password")]
        public string WiFiPassword { get; set; }

        [JsonProperty("wifi_version"), Column("wifi_version")]
        public string WiFiVersion { get; set; }

        [JsonProperty("firmware_version"), Column("firmware_version")]
        public string FirmwareVersion { get; set; }

        [JsonProperty("pass_system"), Column("pass_system")]
        public string PasswordSystem { get; set; }

        public static Config GetDefault()
        {
            return new Config
            {
                Port = Constants.DefaultDeviceConfiguration.PORT,
                FirmwareVersion = string.Empty,
                LocalIp = "192.168.1.2",
                PasswordSystem = Constants.DefaultDeviceConfiguration.PASSWORD,
                Server = Constants.DefaultDeviceConfiguration.SERVER,
                Timeout = Constants.DefaultDeviceConfiguration.TIMEOUT,
                WiFiSsid = Constants.DefaultDeviceConfiguration.WIFI_SSID,
                WiFiPassword = Constants.DefaultDeviceConfiguration.WIFI_PASSWORD
            };
        }
    }
}
