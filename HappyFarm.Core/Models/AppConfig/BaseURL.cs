﻿using Newtonsoft.Json;

namespace HappyFarm.Core.Models.AppConfig
{
    public class BaseURL
    {
        [JsonProperty("BaseImageURL")]
        public string BaseImageUrl { get; set; }

        [JsonProperty("FrontendURL")]
        public string FrontendUrl { get; set; }
    }
}
