﻿using Newtonsoft.Json;

namespace HappyFarm.Core.Models.AppConfig
{
    public class Database
    {
        [JsonProperty("ConnectionString")]
        public string ConnectionString { get; set; }

        [JsonProperty("ServerVersion")]
        public string ServerVersion { get; set; }

        [JsonProperty("Info")]
        public Info Info { get; set; } = new Info();
    }
}
