﻿using Newtonsoft.Json;

namespace HappyFarm.Core.Models.AppConfig
{
    public class Redis
    {
        [JsonProperty("ConnectionString")]
        public string ConnectionString { get; set; }

        [JsonProperty("Host")]
        public string Host { get; set; }

        [JsonProperty("Port")] public ushort? Port { get; set; } = 6379;

        [JsonProperty("Password")]
        public string Password { get; set; }
    }
}
