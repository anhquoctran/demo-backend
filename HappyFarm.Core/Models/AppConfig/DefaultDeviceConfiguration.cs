﻿using Newtonsoft.Json;

namespace HappyFarm.Core.Models.AppConfig
{
    public class DefaultDeviceConfiguration
    {
        [JsonProperty("Password")]
        public string Password { get; set; }

        [JsonProperty("Server")]
        public string Server { get; set; }

        [JsonProperty("Port")]
        public ushort? Port { get; set; }

        [JsonProperty("Timeout")]
        public int? Timeout { get; set; }

        [JsonProperty("WiFiSSID")]
        public string WiFiSsid { get; set; }

        [JsonProperty("WiFiPassword")]
        public string WiFiPassword { get; set; }
    }
}
