﻿using Newtonsoft.Json;

namespace HappyFarm.Core.Models.AppConfig
{
    public class Mqtt
    {
        [JsonProperty("Host")]
        public string Host { get; set; }

        [JsonProperty("Port")]
        public ushort? Port { get; set; }

        [JsonProperty("UseMqttTLS")] public bool? UseMqttTls { get; set; } = false;

        [JsonProperty("DefaultSystemAccount")]
        public DefaultSystemAccount DefaultSystemAccount { get; set; } = new DefaultSystemAccount();

        [JsonProperty("Token")]
        public string Token { get; set; }

        [JsonProperty("VernemqBaseURL")]
        public string VernemqBaseURL { get; set; }

    }
}
