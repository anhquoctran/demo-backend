﻿using Newtonsoft.Json;

namespace HappyFarm.Core.Models.AppConfig
{
    public class Info
    {
        [JsonProperty("Server")]
        public string Server { get; set; }

        [JsonProperty("Username")]
        public string Username { get; set; }

        [JsonProperty("Password")]
        public string Password { get; set; }

        [JsonProperty("Port")]
        public ushort? Port { get; set; }

        [JsonProperty("Database")]
        public string Database { get; set; }
    }
}
