﻿using Newtonsoft.Json;

namespace HappyFarm.Core.Models.AppConfig
{
    public class DefaultSystemAccount
    {
        [JsonProperty("Username")]
        public string Username { get; set; }

        [JsonProperty("Password")]
        public string Password { get; set; }
    }
}
