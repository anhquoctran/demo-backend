﻿using Newtonsoft.Json;

namespace HappyFarm.Core.Models.AppConfig
{
    public class SystemAppConfig
    {
        [JsonProperty("Database")]
        public Database Database { get; set; } = new Database();

        [JsonProperty("Redis")]
        public Redis Redis { get; set; } = new Redis();

        [JsonProperty("Mqtt")]
        public Mqtt Mqtt { get; set; } = new Mqtt();

        [JsonProperty("DefaultDeviceConfiguration")]
        public DefaultDeviceConfiguration DefaultDeviceConfiguration { get; set; } = new DefaultDeviceConfiguration();

        [JsonProperty("BaseURL")]
        public BaseURL BaseUrl { get; set; } = new BaseURL();

        public static SystemAppConfig GetDefault()
        {
            return new SystemAppConfig
            {
                BaseUrl = new BaseURL
                {
                    BaseImageUrl = "https://happyfarm-api.ineo.vn/",
                    FrontendUrl = "https://happyfarm.ineo.vn/"
                },
                Database = new Database
                {
                    ConnectionString = "server=localhost;user id=root;password=Admin@123;port=3306;database=happyfarm;",
                    ServerVersion = "8.0.21-mysql",
                    Info = new Info
                    {
                        Server = "localhost",
                        Port = 3306,
                        Database = "happyfarm",
                        Username = "root",
                        Password = "Admin@123"
                    }
                    
                },
                Redis = new Redis
                {
                    ConnectionString = "localhost:6379,connectRetry=5",
                    Host = "localhost",
                    Port = 6379,
                    Password = null,
                },
                Mqtt = new Mqtt
                {
                    Host = "139.180.130.67",
                    Port = 1883,
                    UseMqttTls = false,
                    DefaultSystemAccount = new DefaultSystemAccount
                    {
                        Username = "sa",
                        Password = "@HappyFarm@2020"
                    }
                },
                DefaultDeviceConfiguration = new DefaultDeviceConfiguration
                {
                    Server = "139.180.130.67",
                    Password = "123456",
                    Port = 1883,
                    Timeout = 60,
                    WiFiPassword = "123456",
                    WiFiSsid = "HappyFarm"
                },
                
            };
        }
    }
}
