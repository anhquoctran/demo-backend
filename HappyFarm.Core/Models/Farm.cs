﻿using HappyFarm.Core.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace HappyFarm.Core.Models
{
    public class Farm : IEntity, IHasCreationTime, IHasUpdateTime
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string RegionCode { get; set; }

        public string Phone { get; set; }

        public int? UpdatedAt { get; set; }

        public int? CreatedAt { get; set; }

        [InverseProperty("Farm")]
        public virtual ICollection<Gateway> Gateways { get; set; }

        [InverseProperty("Farm")]
        public virtual ICollection<User> Users { get; set; }
    }
}
