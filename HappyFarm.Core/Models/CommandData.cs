﻿using HappyFarm.Core.Enums;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;

namespace HappyFarm.Core.Models
{
    public class CommandData
    {
        [JsonProperty("gateway_id",
            NullValueHandling = NullValueHandling.Ignore,
            DefaultValueHandling = DefaultValueHandling.Ignore
            )
        ]
        public int GatewayId { get; set; }
    }

    public class SensorValueCommandData : CommandData
    {
        [JsonProperty("node_count")]
        public int NumberOfNode { get; set; }

        [JsonProperty("nodes_info")]
        public ICollection<NodeInfo> NodesInfo { get; set; }
    }

    public class GatewayControl : CommandData
    {
        [JsonProperty("total_flow_water")]
        public int TotalFlowWater { get; set; }

        [JsonProperty("flow_a")]
        public int FlowA { get; set; }

        [JsonProperty("flow_b")]
        public int FlowB { get; set; }

        [JsonProperty("flow_c")]
        public int FlowC { get; set; }

        [JsonProperty("flow_d")]
        public int FlowD { get; set; }

        [JsonProperty("tds")]
        public int Tds { get; set; }
    }

    public class InformationData : CommandData
    {
        [JsonProperty("total_fw")]
        public int TotalFlowWater { get; set; }

        [JsonProperty("total_A")]
        public int FlowA { get; set; }

        [JsonProperty("total_B")]
        public int FlowB { get; set; }

        [JsonProperty("total_C")]
        public int FlowC { get; set; }

        [JsonProperty("total_D")]
        public int FlowD { get; set; }

        [JsonProperty("num_of_reset")]
        public int NumberOfReset { get; set; }

        [JsonProperty("timer")]
        public int CurrentTimer { get; set; }

        [JsonProperty("version")]
        public string CurrentVersion { get; set; }
    }

    public class NodeInfo
    {
        [JsonProperty("node_code")]
        public string NodeCode { get; set; }

        [JsonProperty("temp")]
        public decimal Temp { get; set; }

        [JsonProperty("air_humidity")]
        public decimal AirHumidity { get; set; }

        [JsonProperty("ph")]
        public decimal PhScale { get; set; }

        [JsonProperty("light_intensity")]
        public decimal LightIntensity { get; set; }

        [JsonProperty("soil_humidi")]
        public decimal SoilHumidity { get; set; }

        [JsonProperty("wind_speed")]
        public decimal WindSpeed { get; set; }

        [JsonProperty("rain")]
        public decimal Rainfall { get; set; }

        [JsonProperty("tds")]
        public decimal Tds { get; set; }

        [JsonProperty("time")]
        public int? CreatedAt { get; set; }
    }

    public class GatewayStatus : CommandData
    {
        [JsonProperty("status")]
        public int Status { get; set; }
    }

    public class PowerControl : CommandData
    {
        [JsonProperty("power")]
        public int Power { get; set; }
    }

    public class ApplyControl : CommandData
    {

        [JsonProperty("channel_A")]
        public int ChannelA { get; set; }

        [JsonProperty("channel_B")]
        public int ChannelB { get; set; }

        [JsonProperty("channel_C")]
        public int ChannelC { get; set; }

        [JsonProperty("channel_D")]
        public int ChannelD { get; set; }

        [JsonProperty("plot")]
        public int Plot { get; set; }
    }

    public class TimerMode : CommandData
    {
        [JsonProperty("mode")]
        public TimerModeState Mode { get; set; }
    }

    public class SettingTimer : CommandData
    {
        [JsonProperty("timer")]
        public IList<Timer> Timers { get; set; }
    }

    public class Timer
    {
        [JsonProperty("index")]
        public int Index { get; set; }

        [JsonProperty("hour")]
        public int Hour { get; set; }

        [JsonProperty("minute")]
        public int Minute { get; set; }

        [JsonProperty("delta_time")]
        public int DeltaTime { get; set; }

        [JsonProperty("plot")]
        public int Plot { get; set; }

        [JsonProperty("flow_A")]
        public int FlowA { get; set; }

        [JsonProperty("flow_B")]
        public int FlowB { get; set; }

        [JsonProperty("flow_C")]
        public int FlowC { get; set; }

        [JsonProperty("flow_D")]
        public int FlowD { get; set; }
    }

    public class LatestValues : CommandData { }

    public class AdministrativeLog
    {
        [JsonProperty("gateway_id")]
        public int GatewayId { get; set; }

        [JsonProperty("command")]
        public int Command { get; set; }

        [JsonProperty("payload")]
        public string PayloadData { get; set; }

        [JsonProperty("created_at")]
        public DateTimeOffset CreatedAt { get; set; }
    }

    public class GatewaySensor
    {
        [JsonProperty("temp")]
        public double Temp { get; set; }

        [JsonProperty("air_humidity")]
        public double AirHumidity { get; set; }

        [JsonProperty("ph")]
        public double PhScale { get; set; }

        [JsonProperty("light")]
        public double LightIntensity { get; set; }

        [JsonProperty("soil_humidity")]
        public double SoilHumidity { get; set; }

        [JsonProperty("wind")]
        public double WindSpeed { get; set; }

        [JsonProperty("rain")]
        public double Rain { get; set; }
    }

    public class TimeSync
    {
        [JsonProperty("systime")]
        public long ServerTime { get; set; }
    }

    public class GatewaySensorBundle: CommandData
    {
        [JsonProperty("values")]
        public GatewaySensorCollection Values { get; set; }

    }

    public class GatewaySensorCollection : ICollection<GatewaySensor>, IList<GatewaySensor>
    {
        private readonly List<GatewaySensor> _sensors;

        public GatewaySensor this[int index] { get => _sensors[index]; set => _sensors[index] = value; }

        public int Count => _sensors.Count;

        public bool IsReadOnly => false;

        public GatewaySensorCollection()
        {
            _sensors = new List<GatewaySensor>();
        }

        public GatewaySensorCollection(int capacity)
        {
            _sensors = new List<GatewaySensor>(capacity);
        }

        public void Add(GatewaySensor item) => _sensors.Add(item);

        public void AddRange(IEnumerable<GatewaySensor> items)
        {
            _sensors.AddRange(items);
        }

        public void Clear()
        {
            _sensors.Clear();
        }

        public bool Contains(GatewaySensor item) => _sensors.Contains(item);

        public void CopyTo(GatewaySensor[] array, int arrayIndex)
        {
            _sensors.CopyTo(array, arrayIndex);
        }

        public IEnumerator<GatewaySensor> GetEnumerator() => _sensors.GetEnumerator();

        public int IndexOf(GatewaySensor item)
        {
            return _sensors.IndexOf(item);
        }

        public void Insert(int index, GatewaySensor item)
        {
            _sensors.Insert(index, item);
        }

        public bool Remove(GatewaySensor item)
        {
            return _sensors.Remove(item);
        }

        public void RemoveAt(int index)
        {
            _sensors.RemoveAt(index);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public static implicit operator List<GatewaySensor>(GatewaySensorCollection collection)
        {
            return collection._sensors;
        }

        public static explicit operator GatewaySensorCollection(List<GatewaySensor> list)
        {
            var collection = new GatewaySensorCollection();
            collection.AddRange(list);
            return collection;
        }

        public static implicit operator GatewaySensor[](GatewaySensorCollection collection)
        {
            return collection._sensors.ToArray();
        }

        public static explicit operator GatewaySensorCollection(GatewaySensor[] list)
        {
            var collection = new GatewaySensorCollection();
            collection.AddRange(list);
            return collection;
        }
    }

}
