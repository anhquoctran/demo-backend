﻿using HappyFarm.Core.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace HappyFarm.Core.Repositories
{
    public class Repository<TEntity> : Sortable<TEntity>, IRepository<TEntity>
        where TEntity : class, IEntity
    {
        private readonly DbSet<TEntity> _dbSet;
        private readonly HappyFarmDbContext _dbContext;

        public Repository(HappyFarmDbContext dbContext)
        {
            _dbContext = dbContext;
            _dbSet = _dbContext.Set<TEntity>();
        }

        public TEntity Add(TEntity entity)
        {
            return _dbSet.Add(entity).Entity;
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            _dbSet.AddRange(entities);
        }

        public int Count()
        {
            return _dbSet.Count();
        }

        public int Count(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbSet.Count(predicate);
        }

        public Task<int> CountAsync(CancellationToken token = default)
        {
            return _dbSet.CountAsync(token);
        }

        public Task<int> CountAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken token = default)
        {
            return _dbSet.CountAsync(predicate, token);
        }

        public void Delete(int id)
        {
            var entity = FindById(id);
            if (entity != null)
            {
                _dbSet.Remove(entity);
            }
        }

        public void Delete(TEntity entityToDelete)
        {
            _dbSet.Remove(entityToDelete);
        }

        public int Delete(IEnumerable<int> listId)
        {
           return _dbSet
                .Where(x => listId.Contains(x.Id))
                .Delete();
        }

        public int Delete(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbSet
                .Where(predicate)
                .Delete();
        }

        public Task<int> DeleteAsync(IEnumerable<int> listId, CancellationToken cts = default)
        {
            return _dbSet
                .Where(x => listId.Contains(x.Id))
                .DeleteAsync(cts);
        }

        public Task<int> DeleteAsync(int id, CancellationToken cts = default)
        {
            return _dbSet
                .Where(x => x.Id == id)
                .DeleteAsync(cts);
        }

        public Task<int> DeleteAsync(TEntity entityToDelete, CancellationToken cts = default)
        {
            return _dbSet
                .Where(x => x.Id == entityToDelete.Id)
                .DeleteAsync(cts);
        }

        public Task<int> DeleteAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken cts = default)
        {
            return _dbSet
                .Where(predicate)
                .DeleteAsync(cts);
        }

        public bool ExistsBy(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbSet.Any(predicate);
        }

        public Task<bool> ExistsByAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken token = default)
        {
            return _dbSet.AnyAsync(predicate, token);
        }

        public bool ExistsById(int id)
        {
            return ExistsBy(x => x.Id == id);
        }

        public Task<bool> ExistsByIdAsync(int id, CancellationToken token = default)
        {
            return ExistsByAsync(x => x.Id == id, token);
        }

        public TEntity Find(Expression<Func<TEntity, bool>> predicate)
        {
            if (predicate != null) return _dbSet.FirstOrDefault(predicate);
            return _dbSet.FirstOrDefault();
        }

        public ICollection<TEntity> FindAll(Expression<Func<TEntity, bool>> predicateFilter = null)
        {
            if (predicateFilter != null)
            {
                return _dbSet.Where(predicateFilter).ToList();
            }
            return _dbSet.ToList();
        }

        public IQueryable<TEntity> AsQuery(Expression<Func<TEntity, bool>> expression = null)
        {
            if (expression != null)
            {
                return _dbSet.Where(expression);
            }

            return _dbSet.AsQueryable();

        }

        public Task<TEntity> FindAsync(Expression<Func<TEntity, bool>> predicateFilter = null, CancellationToken token = default)
        {
            if (predicateFilter != null) return _dbSet.FirstOrDefaultAsync(predicateFilter, token);
            return _dbSet.FirstOrDefaultAsync();
        }

        public TEntity FindById(int id)
        {
            return _dbSet.FirstOrDefault(x => x.Id == id);
        }

        public Task<TEntity> FindByIdAsync(int id, CancellationToken token)
        {
            return _dbSet.FirstOrDefaultAsync(x => x.Id == id, token);
        }

        public void Update(TEntity entity)
        {
            var existing = FindById(entity.Id);
            _dbContext.Entry(existing).CurrentValues.SetValues(entity);
        }
        
        public void UpdateRange(IEnumerable<TEntity> entities)
        {
            var entityType = _dbContext.Model.FindEntityType(typeof(TEntity));
            var primaryKey = entityType.FindPrimaryKey();
            var keyValues = new object[primaryKey.Properties.Count];

            foreach (var entity in entities)
            {
                for (int i = 0; i < keyValues.Length; i++)
                    keyValues[i] = primaryKey.Properties[i].GetGetter().GetClrValue(entity);

                var obj = _dbSet.Find(keyValues);
                _dbContext.Entry(obj).CurrentValues.SetValues(entity);
            }
        }

        public Task<int> MaxByAsync(Expression<Func<TEntity, int>> predicate, CancellationToken cts = default)
        {
            return _dbSet.MaxAsync(predicate, cts);
        }

        public int MaxBy(Expression<Func<TEntity, int>> predicate)
        {
            return _dbSet.Max(predicate);
        }
    }
}
