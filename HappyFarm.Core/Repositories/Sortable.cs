﻿using HappyFarm.Core.Commons;
using HappyFarm.Core.Interfaces;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace HappyFarm.Core.Repositories
{
    public class Sortable<TEntity> : ISortable<TEntity>
    {
        public virtual IQueryable<TEntity> Sort(IQueryable<TEntity> source, string orderByQuerystring, bool useSnackcase = true)
        {
            if (!source.Any()) return source;

            var orderParams = orderByQuerystring.Trim().Split(',').Select(x => x.Trim());
            var propertyInfos = typeof(TEntity).GetProperties();
            var orderQueryBuilder = new StringBuilder();

            foreach (var param in orderParams)
            {
                if (string.IsNullOrEmpty(param)) continue;

                var propertyFromQueryName = param.Split('.')[0];
                if (string.IsNullOrEmpty(propertyFromQueryName)) continue;

                if (!useSnackcase)
                {
                    propertyFromQueryName = propertyFromQueryName.SnakeCaseToPascalCase();
                }

                var objectProperty = propertyInfos.FirstOrDefault(pi => pi.Name.Equals(propertyFromQueryName, StringComparison.InvariantCultureIgnoreCase));

                if (objectProperty == null) continue;

                var sortingOrder = param.EndsWith(".desc") ? "descending" : "ascending";
                orderQueryBuilder.Append($"{propertyFromQueryName} {sortingOrder}, ");
            }

            var orderQuery = orderQueryBuilder.ToString().TrimEnd(',', ' ');

            if (StringUtils.CheckIsEmptyOrWhitespaceString(orderQuery))
            {
                return source;
            }

            source = source.OrderBy(orderQuery);

            return source;
        }
    }
}
