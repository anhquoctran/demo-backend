﻿using HappyFarm.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace HappyFarm.Core.Repositories
{
    public interface IRepository<TEntity> : ISortable<TEntity>
        where TEntity : class, IEntity
    {
        ICollection<TEntity> FindAll(Expression<Func<TEntity, bool>> predicateFilter = null);

        Task<TEntity> FindByIdAsync(int id, CancellationToken token = default);

        TEntity FindById(int id);

        Task<TEntity> FindAsync(Expression<Func<TEntity, bool>> predicateFilter = null, CancellationToken token = default);

        TEntity Find(Expression<Func<TEntity, bool>> predicate);

        IQueryable<TEntity> AsQuery(Expression<Func<TEntity, bool>> expression = null);

        bool ExistsById(int id);

        Task<bool> ExistsByIdAsync(int id, CancellationToken token = default);

        bool ExistsBy(Expression<Func<TEntity, bool>> predicate);

        Task<bool> ExistsByAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken token = default);

        Task<int> CountAsync(CancellationToken token = default);

        int Count();

        Task<int> CountAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken token = default);

        int Count(Expression<Func<TEntity, bool>> predicate);

        TEntity Add(TEntity entity);

        void AddRange(IEnumerable<TEntity> entities);

        void Update(TEntity entity);

        void UpdateRange(IEnumerable<TEntity> entities);

        void Delete(int id);

        int Delete(Expression<Func<TEntity, bool>> predicate);

        void Delete(TEntity entityToDelete);

        int Delete(IEnumerable<int> listId);

        Task<int> DeleteAsync(IEnumerable<int> listId, CancellationToken cts = default);

        Task<int> DeleteAsync(int id, CancellationToken cts = default);

        Task<int> DeleteAsync(TEntity entityToDelete, CancellationToken cts = default);

        Task<int> DeleteAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken cts = default);

        Task<int> MaxByAsync(Expression<Func<TEntity, int>> predicate, CancellationToken cts = default);

        int MaxBy(Expression<Func<TEntity, int>> predicate);
    }
}
