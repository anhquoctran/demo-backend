﻿using FluentValidation;
using HappyFarm.Core.Models;
using HappyFarm.Infrastructure.Services.Interfaces;
using HappyFarm.Infrastructure.Validations;

namespace HappyFarm.Infrastructure.SignalR.Validators
{
    public class PowerControlValidator : AbstractValidator<PowerControl>
    {
        public PowerControlValidator(IGatewayService gatewayService)
        {
            RuleFor(x => x.GatewayId)
                .NotEmpty()
                .DependentRules(() =>
                {
                    RuleFor(x => x.GatewayId)
                        .GatewayExists(gatewayService)
                        .OverridePropertyName("gateway_id");
                })
                .OverridePropertyName("gateway_id");

            RuleFor(x => x.Power)
                .GreaterThanOrEqualTo(0)
                .LessThanOrEqualTo(1)
                .OverridePropertyName("power");
        }
    }
}
