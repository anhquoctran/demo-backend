﻿using FluentValidation;
using HappyFarm.Core.Models;
using HappyFarm.Infrastructure.Services.Interfaces;
using HappyFarm.Infrastructure.Validations;

namespace HappyFarm.Infrastructure.SignalR.Validators
{
    public class SettingTimerValidator : AbstractValidator<SettingTimer>
    {
        public SettingTimerValidator(IGatewayService gatewayService)
        {
            RuleFor(x => x.GatewayId)
                .NotEmpty()
                .DependentRules(() =>
                {
                    RuleFor(x => x.GatewayId)
                        .GatewayExists(gatewayService)
                        .OverridePropertyName("gateway_id");
                })
                .OverridePropertyName("gateway_id");

            RuleFor(x => x.Timers)
                .NotEmpty()
                .DependentRules(() =>
                {
                    RuleForEach(x => x.Timers)
                        .NotEmpty()
                        .SetValidator(new TimerValidator())
                        .OverridePropertyName("timer");
                        
                })
                .OverridePropertyName("timer");
        }
    }

    public class TimerValidator : AbstractValidator<Timer>
    {
        public TimerValidator()
        {
            RuleFor(x => x.Plot)
                .GreaterThanOrEqualTo(0)
                .LessThanOrEqualTo(9999)
                .OverridePropertyName("plot");

            RuleFor(x => x.Index)
                .GreaterThanOrEqualTo(0)
                .LessThanOrEqualTo(9999)
                .OverridePropertyName("index");

            RuleFor(x => x.Hour)
                .GreaterThanOrEqualTo(0)
                .LessThanOrEqualTo(23)
                .OverridePropertyName("hour");

            RuleFor(x => x.Minute)
                .GreaterThanOrEqualTo(0)
                .LessThanOrEqualTo(59)
                .OverridePropertyName("minute");

            RuleFor(x => x.DeltaTime)
                .GreaterThanOrEqualTo(0)
                .LessThanOrEqualTo(9999)
                .OverridePropertyName("delta_time");

            RuleFor(x => x.FlowA)
                .GreaterThanOrEqualTo(0)
                .LessThanOrEqualTo(9999)
                .OverridePropertyName("flow_A");

            RuleFor(x => x.DeltaTime)
                .GreaterThanOrEqualTo(0)
                .LessThanOrEqualTo(9999)
                .OverridePropertyName("flow_B");

            RuleFor(x => x.FlowC)
                .GreaterThanOrEqualTo(0)
                .LessThanOrEqualTo(9999)
                .OverridePropertyName("flow_C");

            RuleFor(x => x.FlowD)
                .GreaterThanOrEqualTo(0)
                .LessThanOrEqualTo(9999)
                .OverridePropertyName("flow_D");
        }
    }
}
