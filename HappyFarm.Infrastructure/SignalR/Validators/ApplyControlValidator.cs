﻿using FluentValidation;
using HappyFarm.Core.Models;
using HappyFarm.Infrastructure.Services.Interfaces;
using HappyFarm.Infrastructure.Validations;

namespace HappyFarm.Infrastructure.SignalR.Validators
{
    public class ApplyControlValidator : AbstractValidator<ApplyControl>
    {
        public ApplyControlValidator(IGatewayService gatewayService)
        {
            RuleFor(x => x.GatewayId)
                .NotEmpty()
                .DependentRules(() =>
                {
                    RuleFor(x => x.GatewayId)
                        .GatewayExists(gatewayService)
                        .OverridePropertyName("gateway_id");
                })
                .OverridePropertyName("gateway_id");

            RuleFor(x => x.Plot)
                .GreaterThanOrEqualTo(0)
                .LessThanOrEqualTo(9999)
                .OverridePropertyName("plot");

            RuleFor(x => x.ChannelA)
                .LessThanOrEqualTo(9999)
                .GreaterThanOrEqualTo(0)
                .OverridePropertyName("channel_A");

            RuleFor(x => x.ChannelB)
                .LessThanOrEqualTo(9999)
                .GreaterThanOrEqualTo(0)
                .OverridePropertyName("channel_B");

            RuleFor(x => x.ChannelC)
                .LessThanOrEqualTo(9999)
                .GreaterThanOrEqualTo(0)
                .OverridePropertyName("channel_C");

            RuleFor(x => x.ChannelD)
                .LessThanOrEqualTo(9999)
                .GreaterThanOrEqualTo(0)
                .OverridePropertyName("channel_D");
        }
    }
}
