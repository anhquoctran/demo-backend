﻿using FluentValidation;
using HappyFarm.Core.Models;

namespace HappyFarm.Infrastructure.SignalR.Validators
{
    public class LatestValuesValidator: AbstractValidator<LatestValues>
    {

        public LatestValuesValidator()
        {
            RuleFor(x => x.GatewayId)
                .NotEmpty();
        }
    }
}
