﻿using FluentValidation;
using HappyFarm.Core.Models;
using HappyFarm.Infrastructure.Services.Interfaces;
using HappyFarm.Infrastructure.Validations;

namespace HappyFarm.Infrastructure.SignalR.Validators
{
    public class TimerModeValidator : AbstractValidator<TimerMode>
    {
        public TimerModeValidator(IGatewayService gatewayService)
        {
            RuleFor(x => x.GatewayId)
                .NotEmpty()
                .DependentRules(() =>
                {
                    RuleFor(x => x.GatewayId)
                        .GatewayExists(gatewayService)
                        .OverridePropertyName("gateway_id");
                })
                .OverridePropertyName("gateway_id");

            RuleFor(x => x.Mode)
                .NotNull()
                .DependentRules(() =>
                {
                    RuleFor(x => x.Mode)
                        .IsInEnum();
                })
                .OverridePropertyName("mode");
        }
    }
}
