﻿using FluentValidation.Results;
using HappyFarm.Core.Commons;
using HappyFarm.Core.Models;
using HappyFarm.Infrastructure.Dto;
using HappyFarm.Infrastructure.Services.Implementations;
using HappyFarm.Infrastructure.Services.Interfaces;
using HappyFarm.Infrastructure.SignalR.Validators;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HappyFarm.Infrastructure.SignalR
{
    public class HappyFarmHub : Hub<IHappyFarmConnector>
    {
        private readonly IMessageQueueService _messageQueueService;
        private readonly IGatewayService _gatewateService;
        private readonly ILogger _logger;

        public HappyFarmHub(
            IMessageQueueService messageQueueService,
            IGatewayService gatewayService,
            ILogger<HappyFarmHub> logger
            )
        {
            _messageQueueService = messageQueueService;
            _gatewateService = gatewayService;
            _logger = logger;
            
        }

        [HubMethodName("sendPowerCmd"), Authorize(Policy = "HFAuthorizationPolicy")]
        public async Task SendPowerCommand(PowerControl powerControl)
        {
            if (powerControl != null)
            {
                var validator = new PowerControlValidator(_gatewateService);
                var validated = await validator.ValidateAsync(powerControl);
                if (validated.IsValid)
                {
                    var topic = $"/hpdev/{powerControl.GatewayId}/command";
                    powerControl.GatewayId = 0;
                    var data = new HFCommand<PowerControl>
                    {
                        ConnectionId = Context.ConnectionId,
                        Command = (int) Constants.DeviceCommand.CommandPowerSetting,
                        Data = powerControl
                    };
                    var packetPayload = new PublishMessageData<HFCommand<PowerControl>>
                    {
                        Data = data,
                        Topic = topic,
                    };
                    await _messageQueueService.Publish(packetPayload);
                    //HandleSuccessResponse(gatewayId);
                }
                else
                {
                    HandleErrorBadRequest(powerControl.GatewayId, validated.Errors);
                }
            }
            else
            {
                SendBadArgumentException();
            }
        }

        [HubMethodName("sendApplyControlCmd"), Authorize(Policy = "HFAuthorizationPolicy")]
        public async Task SendApplyControlCommand(ApplyControl applyControlCmd)
        {
            if (applyControlCmd != null)
            {
                var validator = new ApplyControlValidator(_gatewateService);
                var validated = await validator.ValidateAsync(applyControlCmd);

                if (validated.IsValid)
                {
                    var topic = $"/hpdev/{applyControlCmd.GatewayId}/command";
                    applyControlCmd.GatewayId = 0;
                    var data = new HFCommand<ApplyControl>
                    {
                        ConnectionId = Context.ConnectionId,
                        Command = (int) Constants.DeviceCommand.CommandApplyControl,
                        Data = applyControlCmd
                    };
                    var packetPayload = new PublishMessageData<HFCommand<ApplyControl>>
                    {
                        Data = data,
                        Topic = topic,
                    };
                    await _messageQueueService.Publish(packetPayload);
                    //HandleSuccessResponse(gatewayId);
                }
                else
                {
                    HandleErrorBadRequest(applyControlCmd.GatewayId, validated.Errors);
                }
            }
            else
            {
                SendBadArgumentException();
            }
        }

        [HubMethodName("sendTimerModeCmd"), Authorize(Policy = "HFAuthorizationPolicy")]
        public async Task SendTimerModeCommand(TimerMode timerModeCmd)
        {
            if (timerModeCmd != null)
            {
                var validator = new TimerModeValidator(_gatewateService);
                var validated = await validator.ValidateAsync(timerModeCmd);

                if (validated.IsValid)
                {
                    var topic = $"/hpdev/{timerModeCmd.GatewayId}/command";
                    timerModeCmd.GatewayId = 0;
                    var data = new HFCommand<TimerMode>
                    {
                        ConnectionId = Context.ConnectionId,
                        Command = (int) Constants.DeviceCommand.CommandTimerModeSetting,
                        Data = timerModeCmd
                    };

                    var packetPayload = new PublishMessageData<HFCommand<TimerMode>>
                    {
                        Data = data,
                        Topic = topic,
                    };
                    await _messageQueueService.Publish(packetPayload);
                    //HandleSuccessResponse(gatewayId);
                }
                else
                {
                    HandleErrorBadRequest(timerModeCmd.GatewayId, validated.Errors);
                }
            }
            else
            {
                SendBadArgumentException();
            }
        }

        [HubMethodName("requestLatestValues"), Authorize(Policy = "HFAuthorizationPolicy")]
        public async Task RequestLatestValues(LatestValues latestValues)
        {
            if (latestValues != null)
            {
                var validator = new LatestValuesValidator();
                var validated = await validator.ValidateAsync(latestValues);

                if (validated.IsValid)
                {
                    var topic = $"/hpdev/{latestValues.GatewayId}/command";
                    latestValues.GatewayId = 0;
                    var data = new HFCommand<object>
                    {
                        //ConnectionId = Context.ConnectionId,
                        Command = (int)Constants.DeviceCommand.CommandLatestValue
                    };

                    var packetPayload = new PublishMessageData<HFCommand<object>>
                    {
                        Data = data,
                        Topic = topic,
                    };
                    await _messageQueueService.Publish(packetPayload);
                    //HandleSuccessResponse(gatewayId);
                }
                else
                {
                    HandleErrorBadRequest(latestValues.GatewayId, validated.Errors);
                }
            }
            else
            {
                SendBadArgumentException();
            }
        }

        [HubMethodName("sendSettingTimerCmd"), Authorize(Policy = "HFAuthorizationPolicy")]
        public async Task SendTimerSettingCommand(SettingTimer settingTimerCmd)
        {
            if (settingTimerCmd != null)
            {
                var validator = new SettingTimerValidator(_gatewateService);
                var validated = await validator.ValidateAsync(settingTimerCmd);

                if (validated.IsValid)
                {
                    var topic = $"/hpdev/{settingTimerCmd.GatewayId}/command";
                    settingTimerCmd.GatewayId = 0;
                    var data = new HFCommand<SettingTimer>
                    {
                        ConnectionId = Context.ConnectionId,
                        Command = (int)Constants.DeviceCommand.CommandControlTimer,
                        Data = settingTimerCmd
                    };

                    var packetPayload = new PublishMessageData<HFCommand<SettingTimer>>
                    {
                        Data = data,
                        Topic = topic,
                    };
                    await _messageQueueService.Publish(packetPayload);
                    //HandleSuccessResponse(gatewayId);
                }
                else
                {
                    HandleErrorBadRequest(settingTimerCmd.GatewayId, validated.Errors);
                }
            }
            else
            {
                SendBadArgumentException();
            }
        }

        [HubMethodName("ping"), Authorize(Policy = "HFAuthorizationPolicy")]
        public async Task Ping()
        {
            _logger.LogInformation("Realtime connection requested from client ID: {0}", Context.ConnectionId);
            await Clients.Client(Context.ConnectionId).pong();
        }

        public async override Task OnConnectedAsync()
        {
            _logger.LogInformation("Client {0} joined to Realtime Server", Context.ConnectionId);
            var httpContext = Context.GetHttpContext();
            SignalrConnectionManager.Instance.AddConnection(new SignalrConnection
            {
                ConnectionId = Context.ConnectionId,
                ConnectedTime = DateTimeUtils.ConvertToUnixTime(DateTime.Now),
                RemoteAddress = httpContext.Connection.RemoteIpAddress.MapToIPv4().ToString()
            });
            await base.OnConnectedAsync();
        }

        public async override Task OnDisconnectedAsync(Exception exception)
        {
            _logger.LogInformation("Client {0} leave out", Context.ConnectionId);
            SignalrConnectionManager.Instance.RemoveConnection(Context.ConnectionId);
            await base.OnDisconnectedAsync(exception);
        }

        //private async void HandleSuccessResponse(int gatewayId)
        //{
        //    await Clients.Client(Context.ConnectionId).ReceivedResponseCmdState(new CmdStateResponse
        //    {
        //        GatewayId = gatewayId,
        //        State = true,
        //        Errors = null,
        //        Message = ReasonPhrases.GetReasonPhrase(200)
        //    });
        //}

        private async void SendBadArgumentException()
        {
            IList<string> message = new List<string> { "Không thể gửi lệnh mà không có dữ liệu nào" };
            var error = new Dictionary<string, IList<string>> { { "common", message } };
            await Clients.Client(Context.ConnectionId).ReceivedResponseCmdState(new CmdStateResponse
            {
                Errors = error,
                GatewayId = 0,
                Message = ReasonPhrases.GetReasonPhrase(400),
                State = false
            }) ;
        }

        private async void HandleErrorBadRequest(int gatewayId, IEnumerable<ValidationFailure> failures)
        {
            var dictErrors = new Dictionary<string, IList<string>>();
            foreach (var error in failures)
            {
                if (dictErrors.ContainsKey(error.PropertyName) && dictErrors[error.PropertyName] != null)
                {
                    dictErrors[error.PropertyName].Add(error.ErrorMessage);
                }
                else
                {
                    dictErrors.Add(error.PropertyName, new List<string> { error.ErrorMessage });
                }
            }

            await Clients.Client(Context.ConnectionId).ReceivedResponseCmdState(new CmdStateResponse
            {
                GatewayId = gatewayId,
                Message = ReasonPhrases.GetReasonPhrase(400),
                State = false,
                Errors = dictErrors
            });
        }
    }
}
