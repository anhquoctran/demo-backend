﻿using HappyFarm.Core.Commons;
using HappyFarm.Infrastructure.Commons;
using HappyFarm.Infrastructure.Services.Interfaces;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using System.Threading.Tasks;

namespace HappyFarm.Infrastructure.SignalR.Authorization
{
    public class HFAuthorizationHandler : AuthorizationHandler<HFAuthorizationRequirement>
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IAuthService _authService;

        public HFAuthorizationHandler
            (
                IHttpContextAccessor httpContextAccessor,
                IAuthService authService
            )
        {
            _httpContextAccessor = httpContextAccessor;
            _authService = authService;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, HFAuthorizationRequirement requirement)
        {
            // Implement authorization logic  
            if (_httpContextAccessor.HttpContext.Request.Query.TryGetValue("access_token", out var accessToken))
            {
                if (ValidateRequest(accessToken, out var _))
                {
                    // Authorization passed 
                    
                    context.Succeed(requirement);
                }
                else
                {
                    // Authorization failed  
                    context.Fail();
                }
            }
            else
            {
                // Authorization failed  
                context.Fail();
            }

            return Task.CompletedTask;
        }

        private bool ValidateRequest(string accessToken, out AuthenticationTicket ticket)
        {
            if (!StringUtils.CheckIsEmptyOrWhitespaceString(accessToken))
            {
                var (success, data) = JwtAuthFactory.ValidateJwtToken(accessToken);
                if (!success)
                {
                    ticket = null;
                    return false;
                }

                var payload = data.Payload;
                if (payload.TryGetValue("id", out object idRaw))
                {
                    var idString = (string)idRaw;
                    var idParsedOk = int.TryParse(idString, out int id);

                    if (idParsedOk)
                    {
                        var canAccess = _authService.Authorize(id);
                        if (!canAccess)
                        {
                            ticket = null;
                            return false;
                        }

                        var claims = new[] { new Claim("JwtToken", accessToken) };
                        var identity = new ClaimsIdentity(claims, nameof(HFAuthorizationHandler));
                        ticket = new AuthenticationTicket(new ClaimsPrincipal(identity), JwtBearerDefaults.AuthenticationScheme);


                        return true;
                    }

                    ticket = null;
                    return false;
                }

                ticket = null;
                return false;
            }

            ticket = null;
            return false;
        }
    }
}
