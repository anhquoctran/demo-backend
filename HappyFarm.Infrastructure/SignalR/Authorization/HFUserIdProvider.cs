﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SignalR;

namespace HappyFarm.Infrastructure.SignalR.Authorization
{
    public class HFUserIdProvider : IUserIdProvider
    {
        readonly IHttpContextAccessor _httpContextAccessor;

        public HFUserIdProvider(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public string GetUserId(HubConnectionContext connection)
        {
            return _httpContextAccessor.HttpContext.Request.Query["username"];
        }
    }
}
