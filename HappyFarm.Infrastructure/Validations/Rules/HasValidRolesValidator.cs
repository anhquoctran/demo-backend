﻿using System.Collections;
using System.Linq;
using FluentValidation.Resources;
using FluentValidation.Validators;
using HappyFarm.Core.Commons;
using HappyFarm.Core.Models;

namespace HappyFarm.Infrastructure.Validations.Rules
{
    public class HasValidRolesValidator : PropertyValidator
    {
        public HasValidRolesValidator() : base(new LanguageStringSource(nameof(HasValidRolesValidator))) { }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            if (context.PropertyValue == null) return true;
            var range = ((IEnumerable)context.PropertyValue)
              .Cast<object>()
              .Select(x => x != null ? x.ToString() : string.Empty)
              .Where(x => !StringUtils.CheckIsEmptyOrWhitespaceString(x))
              .Select(x => x.Trim())
              .ToArray();
            if (range.Length == 0)
            {
                return true;
            }
            return User.Roles.Any(x => range.Contains(x));
        }
    }
}
