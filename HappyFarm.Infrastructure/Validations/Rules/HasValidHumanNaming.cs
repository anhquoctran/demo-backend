﻿using System.Text.RegularExpressions;
using FluentValidation.Resources;
using FluentValidation.Validators;

namespace HappyFarm.Infrastructure.Validations.Rules
{
    public class HasValidHumanNamingValidator : PropertyValidator, IRegularExpressionValidator
    {
        private const string EXPRESSION = @"^[a-zA-Z\u00C0-\u1ef9\s\b]*$";
        public string Expression => EXPRESSION;

        private static readonly Regex Regex = CreateRegex();
        private static Regex CreateRegex()
        {
            const RegexOptions options = RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.ExplicitCapture | RegexOptions.Singleline;
            return new Regex(EXPRESSION, options);
        }

        public HasValidHumanNamingValidator() : base(new LanguageStringSource(nameof(HasValidHumanNamingValidator))) { }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            if (context.PropertyValue == null) return true;

            return Regex.IsMatch((string)context.PropertyValue);
        }
    }
}
