﻿using FluentValidation.Resources;
using FluentValidation.Validators;

namespace HappyFarm.Infrastructure.Validations.Rules
{
    public class PasswordLengthValidator : PropertyValidator
    {
        private readonly int _minLength;
        private readonly int _maxLength;

        public PasswordLengthValidator(int minLength, int maxLength) : base(new LanguageStringSource(nameof(PasswordLengthValidator)))
        {
            _minLength = minLength;
            _maxLength = maxLength;
        }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            if (context.PropertyValue == null) return true;
            context.MessageFormatter
              .AppendArgument("MinLength", _minLength)
              .AppendArgument("MaxLength", _maxLength);

            var passwordString = (string)context.PropertyValue;
            return passwordString.Length >= _minLength && passwordString.Length <= _maxLength;
        }
    }
}
