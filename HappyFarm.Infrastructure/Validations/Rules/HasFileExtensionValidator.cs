﻿using FluentValidation.Resources;
using FluentValidation.Validators;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Linq;

namespace HappyFarm.Infrastructure.Validations.Rules
{
    public class HasFileExtensionValidator : PropertyValidator
    {
        private readonly string[] _extensions;

        public HasFileExtensionValidator(params string[] extensions) : base(new LanguageStringSource(nameof(HasFileExtensionValidator)))
        {
            _extensions = extensions.Where(ext => !string.IsNullOrEmpty(ext)).Select(ext => ext.Trim().ToLowerInvariant()).ToArray();
        }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            if (context.PropertyValue == null) return true;
            var file = (IFormFile)context.PropertyValue;
            var fileName = file.FileName;
            var extension = Path.GetExtension(fileName).ToLowerInvariant();
            var valid = _extensions.Contains(extension);

            return valid;
        }
    }
}
