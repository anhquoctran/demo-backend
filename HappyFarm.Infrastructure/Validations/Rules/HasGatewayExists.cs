﻿using System.Threading;
using System.Threading.Tasks;
using FluentValidation.Resources;
using FluentValidation.Validators;
using HappyFarm.Core.Enums;
using HappyFarm.Infrastructure.Services.Interfaces;

namespace HappyFarm.Infrastructure.Validations.Rules
{
    public class HasGatewayExists : AsyncValidatorBase
    {
        private readonly IGatewayService _gatewayService;

        public HasGatewayExists(IGatewayService gatewayService) : base(new LanguageStringSource(nameof(HasGatewayExists)))
        {
            _gatewayService = gatewayService;
        }

        protected override async Task<bool> IsValidAsync(PropertyValidatorContext context, CancellationToken cancellation)
        {
            if (context.PropertyValue == null) return false;
            if (int.TryParse(context.PropertyValue.ToString(), out var id))
            {
                var checkExists = await _gatewayService.IsExistById(id);

                return checkExists == OperationStatus.Succeed;
            }
            return false;
        }
    }
}
