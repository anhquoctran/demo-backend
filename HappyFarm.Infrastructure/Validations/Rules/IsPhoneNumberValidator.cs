﻿using FluentValidation.Resources;
using FluentValidation.Validators;
using HappyFarm.Core.Commons;

namespace HappyFarm.Infrastructure.Validations.Rules
{
    public class IsPhoneNumberValidator : PropertyValidator
    {

        public IsPhoneNumberValidator() : base(new LanguageStringSource(nameof(IsPhoneNumberValidator)))
        {
        }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            if (context.PropertyValue == null) return true;
            var classInstance = context.Instance.GetType();
            var countryCode = (string)classInstance.GetProperty("RegionCode").GetValue(context.Instance);
            var phoneNumberLike = (string)context.PropertyValue ?? string.Empty;
            if (StringUtils.CheckIsEmptyOrWhitespaceString(countryCode) || StringUtils.CheckIsEmptyOrWhitespaceString(phoneNumberLike)) return true;
            return PhoneNumber.IsValidPhonenumber(countryCode.Trim(), phoneNumberLike);
        }
    }
}
