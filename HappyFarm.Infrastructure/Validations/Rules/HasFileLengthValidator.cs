﻿using FluentValidation.Resources;
using FluentValidation.Validators;
using Microsoft.AspNetCore.Http;

namespace HappyFarm.Infrastructure.Validations.Rules
{
    public class HasFileLengthValidator : PropertyValidator
    {
        private const double DEFAULT_LENGTH = 2 * 1024 * 1024;

        private readonly double _length = DEFAULT_LENGTH;

        public HasFileLengthValidator(double fileLength = DEFAULT_LENGTH) : base(new LanguageStringSource(nameof(HasFileLengthValidator)))
        {
            if (fileLength <= 0) fileLength = DEFAULT_LENGTH;
            _length = fileLength;
        }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            if (context.PropertyValue == null) return true;
            var file = (IFormFile)context.PropertyValue;
            return file.Length <= _length;
        }
    }
}
