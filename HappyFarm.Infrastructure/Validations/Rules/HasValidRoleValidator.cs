﻿using System.Linq;
using FluentValidation.Resources;
using FluentValidation.Validators;
using HappyFarm.Core.Models;

namespace HappyFarm.Infrastructure.Validations.Rules
{
    public class HasValidRoleValidator : PropertyValidator
    {
        public HasValidRoleValidator() : base(new LanguageStringSource(nameof(HasValidRoleValidator))) { }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            if (context.PropertyValue == null) return true;
            var rawValue = (string)context.PropertyValue;
            if (rawValue != null)
            {
                rawValue = rawValue.Trim().ToUpperInvariant();
            }
            return User.Roles.Any(x => x.ToUpperInvariant() == rawValue);
        }
    }
}
