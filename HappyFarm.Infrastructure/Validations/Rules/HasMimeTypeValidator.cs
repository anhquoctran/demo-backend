﻿using System.Linq;
using FluentValidation.Resources;
using FluentValidation.Validators;
using Microsoft.AspNetCore.Http;

namespace HappyFarm.Infrastructure.Validations.Rules
{
    public class HasMimeTypeValidator : PropertyValidator
    {
        private readonly string[] _mimeTypes;

        public HasMimeTypeValidator(params string[] mimeTypes) : base(new LanguageStringSource(nameof(HasMimeTypeValidator)))
        {
            _mimeTypes = mimeTypes.Where(m => !string.IsNullOrEmpty(m)).Select(m => m.Trim().ToLowerInvariant()).ToArray();
        }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            if (context.PropertyValue == null) return true;
            var file = (IFormFile)context.PropertyValue;
            return _mimeTypes.Contains(file.ContentType);
        }
    }
}
