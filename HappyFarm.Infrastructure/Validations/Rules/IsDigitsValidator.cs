﻿using System.Text.RegularExpressions;
using FluentValidation.Resources;
using FluentValidation.Validators;

namespace HappyFarm.Infrastructure.Validations.Rules
{
    public class IsDigitsValidator : PropertyValidator, IRegularExpressionValidator
    {

        public IsDigitsValidator() : base(new LanguageStringSource(nameof(IsDigitsValidator))) { }

        private const string EXPRESSION = @"^\d*$";

        public string Expression => EXPRESSION;

        private static readonly Regex Regex = CreateRegex();
        private static Regex CreateRegex()
        {
            const RegexOptions options = RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.ExplicitCapture | RegexOptions.Singleline;
            return new Regex(EXPRESSION, options);
        }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            return context.PropertyValue == null || Regex.IsMatch((string)context.PropertyValue);
        }
    }
}
