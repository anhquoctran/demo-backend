﻿using System.Text.RegularExpressions;
using FluentValidation.Resources;
using FluentValidation.Validators;

namespace HappyFarm.Infrastructure.Validations.Rules
{
    public class HasValidCodeValidator : PropertyValidator, IRegularExpressionValidator
    {
        private const string EXPRESSION = @"^[A-Za-z\d]*$";

        public string Expression => EXPRESSION;
        private static readonly Regex Regex = CreateRegex();

        public HasValidCodeValidator() : base(new LanguageStringSource(nameof(HasValidCodeValidator)))
        {
        }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            return context.PropertyValue == null || Regex.IsMatch((string)context.PropertyValue);
        }

        private static Regex CreateRegex()
        {
            const RegexOptions options = RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.ExplicitCapture | RegexOptions.Singleline;
            return new Regex(EXPRESSION, options);
        }
    }
}
