﻿using FluentValidation.Resources;
using FluentValidation.Validators;
using HappyFarm.Core.Commons;

namespace HappyFarm.Infrastructure.Validations.Rules
{
    public class IsRegionCodeValidator : PropertyValidator
    {
        public IsRegionCodeValidator() : base(new LanguageStringSource(nameof(IsRegionCodeValidator))) { }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            if (context.PropertyValue == null) return true;

            var s = (string)context.PropertyValue;
            return PhoneNumber.GetCountryCodesSupported().Contains(s.Trim().ToUpperInvariant());
        }
    }
}
