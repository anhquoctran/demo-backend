﻿using System.Text.RegularExpressions;
using FluentValidation.Resources;
using FluentValidation.Validators;

namespace HappyFarm.Infrastructure.Validations.Rules
{
    public class HasValidDeviceNamingValidator : PropertyValidator, IRegularExpressionValidator
    {
        private const string EXPRESSION = @"^[a-zA-Z\d_\-\b\s ]*$";
        public string Expression => EXPRESSION;

        private static readonly Regex Regex = CreateRegex();
        private static Regex CreateRegex()
        {
            const RegexOptions options = RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.ExplicitCapture | RegexOptions.Singleline;
            return new Regex(EXPRESSION, options);
        }

        public HasValidDeviceNamingValidator() : base(new LanguageStringSource(nameof(HasValidDeviceNamingValidator))) { }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            return context.PropertyValue == null || Regex.IsMatch((string)context.PropertyValue);
        }
    }
}
