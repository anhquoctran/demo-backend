﻿/*
 * Copyright [2020] [anhquoctran]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

using FluentValidation.Resources;
using FluentValidation.Validators;
using HappyFarm.Infrastructure.Validations.Rules;

namespace HappyFarm.Infrastructure.Validations.Languages
{
    public class Vietnamese : Language
    {
        public const string Culture = "vi-vn";
        public override string Name => Culture;

        public Vietnamese()
        {
            Translate<EmailValidator>("Địa chỉ email không hợp lệ");
            Translate<GreaterThanOrEqualValidator>("Trường giá trị này phải lớn hơn hoặc bằng '{ComparisonValue}'.");
            Translate<GreaterThanValidator>("Trường giá trị này phải lớn hơn '{ComparisonValue}'.");
            Translate<LengthValidator>("Độ dài trường phải nằm trong khoảng {MinLength} đến {MaxLength} ký tự");
            Translate<MinimumLengthValidator>("Độ dài của giá trị phải lớn hơn hoặc bằng {MinLength} ký tự.");
            Translate<MaximumLengthValidator>("Độ dài của phải ít hơn bằng {MaxLength} ký tự");
            Translate<LessThanOrEqualValidator>("Trường giá trị này phải bé hơn hoặc bằng '{ComparisonValue}'.");
            Translate<LessThanValidator>("Trường giá trị này phải bé hơn '{ComparisonValue}'.");
            Translate<NotEmptyValidator>("Giá trị của trường này không được bỏ trống");
            Translate<NotEqualValidator>("Trường giá trị này không được bằng '{ComparisonValue}'.");
            Translate<NotNullValidator>("Giá trị của trường này không được bỏ trống");
            Translate<PredicateValidator>("Điều kiện quy định không đáp ứng cho trường giá trị này.");
            Translate<AsyncPredicateValidator>("Điều kiện quy định không đáp ứng cho trường giá trị này.");
            Translate<RegularExpressionValidator>("Trường giá trị này không đúng định dạng.");
            Translate<EqualValidator>("Giá trị trường này không khớp");
            Translate<ExactLengthValidator>("Trường giá trị này must be {MaxLength} characters in length");
            Translate<InclusiveBetweenValidator>("Trường giá trị này phải nằm trong khoảng từ {From} đến {To}.");
            Translate<ExclusiveBetweenValidator>("Trường giá trị này must be between {From} and {To} (exclusive).");
            Translate<CreditCardValidator>("Trường giá trị này không hợp lệ định dạng mã thẻ tín dụng.");
            Translate<ScalePrecisionValidator>("Trường giá trị này không được nhiều hơn {ExpectedPrecision} tổng số chữ số");
            Translate<EmptyValidator>("Trường phải bỏ trống.");
            Translate<NullValidator>("Trường phải bỏ trống.");
            Translate<EnumValidator>("Trường giá trị này có một loạt các giá trị không bao gồm '{PropertyValue}'.");
            Translate<HasFileExtensionValidator>("Phần mở rộng tập tin không hợp lệ.");
            Translate<HasMimeTypeValidator>("Định dạng tập tin không phù hợp.");
            Translate<HasFileLengthValidator>("Kích thước tập tin không hợp lệ. Tập tin quá lớn.");
            Translate<HasValidCodeValidator>("Định dạng mã không hợp lệ.");
            Translate<IsDigitsValidator>("Giá trị phải là chữ số.");
            Translate<HasValidHumanNamingValidator>("Giá trị trường phải là tên người hợp lệ (bằng tiếng Việt hoặc tiếng Anh), không được chứa ký tự đặc biệt hoặc số");
            Translate<HasValidDeviceNamingValidator>("Tên thiết bị không hợp lệ.");
            Translate<HasValidRolesValidator>("Phân quyền đã chọn không hợp lệ.");
            Translate<IsPhoneNumberValidator>("Dữ liệu bạn nhập không phải là số điện thoại.");
            Translate<IsRegionCodeValidator>("Mã quốc gia không hợp lệ hoặc không hỗ trợ.");
            Translate<PasswordLengthValidator>("Mật khẩu phải có độ dài từ {MinLength} đến {MaxLength} ký tự.");
            Translate<HasGatewayExists>("Gateway does not exists.");
        }
    }
}
