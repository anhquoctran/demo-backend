﻿/*
 * Copyright [2020] [anhquoctran]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

using FluentValidation.Resources;
using FluentValidation.Validators;
using HappyFarm.Infrastructure.Validations.Rules;

namespace HappyFarm.Infrastructure.Validations.Languages
{
    public class English : Language
    {
        public const string Culture = "en-us";
        public override string Name => Culture;

        public English()
        {
            Translate<EmailValidator>("Invalid login address");
            Translate<GreaterThanOrEqualValidator>("Field must be greater than or equal to '{ComparisonValue}'.");
            Translate<GreaterThanValidator>("'Field must be greater than '{ComparisonValue}'.");
            Translate<LengthValidator>("Field length must be between {MinLength} and {MaxLength} characters.");
            Translate<MinimumLengthValidator>("The length of field must be at least {MinLength} characters.");
            Translate<MaximumLengthValidator>("The length of field must be {MaxLength} characters or fewer.");
            Translate<LessThanOrEqualValidator>("Field must be less than or equal to '{ComparisonValue}'.");
            Translate<LessThanValidator>("Field must be less than '{ComparisonValue}'.");
            Translate<NotEmptyValidator>("Field must not be empty");
            Translate<NotEqualValidator>("Field must not be equal to '{ComparisonValue}'.");
            Translate<NotNullValidator>("Field must not be empty");
            Translate<PredicateValidator>("The specified condition was not met for field'.");
            Translate<AsyncPredicateValidator>("The specified condition was not met for field.");
            Translate<RegularExpressionValidator>("Field is not in the correct format.");
            Translate<EqualValidator>("Field confirmation does not match.");
            Translate<ExactLengthValidator>("Field must be {MaxLength} characters in length.");
            Translate<InclusiveBetweenValidator>("Field must be between {From} and {To}.");
            Translate<ExclusiveBetweenValidator>("Field must be between {From} and {To} (exclusive).");
            Translate<CreditCardValidator>("Field is not a valid credit card number.");
            Translate<ScalePrecisionValidator>("Field must not be more than {ExpectedPrecision} digits in total, with allowance for {ExpectedScale} decimals.");
            Translate<EmptyValidator>("Field must be empty");
            Translate<NullValidator>("Field must be empty");
            Translate<EnumValidator>("Field has a range of values which does not include '{PropertyValue}'.");
            Translate<HasFileExtensionValidator>("This field has invalid file extension");
            Translate<HasMimeTypeValidator>("Invalid file format");
            Translate<HasFileLengthValidator>("Invalid file size. File is too large");
            Translate<HasValidCodeValidator>("Invalid code number format");
            Translate<IsDigitsValidator>("A value must be a digits");
            Translate<HasValidHumanNamingValidator>("Field value must be valid human name in alphabet (English or Vietnamese) characters. Must not be contains special characters or digits");
            Translate<HasValidDeviceNamingValidator>("Device name seems to be invalid");
            Translate<PasswordLengthValidator>("Password must have {MinLength} to {MaxLength} characters length.");
            Translate<HasGatewayExists>("Gateway does not exists.");
        }
    }
}
