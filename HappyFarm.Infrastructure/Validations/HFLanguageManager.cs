﻿/*
 * Copyright [2020] [anhquoctran]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

using FluentValidation.Resources;
using HappyFarm.Infrastructure.Validations.Languages;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;

namespace HappyFarm.Infrastructure.Validations
{
    public class HFLanguageManager : ILanguageManager
    {
        private readonly ConcurrentDictionary<string, Language> _languages;
        private readonly Language _fallback = new Vietnamese();

        public HFLanguageManager()
        {
            _languages = new ConcurrentDictionary<string, Language>(new[] {
        new KeyValuePair<string, Language>(Vietnamese.Culture, _fallback)
      });
        }

        private static Language CreateLanguage(string culture)
        {
            return culture switch
            {
                English.Culture => new English(),
                Vietnamese.Culture => new Vietnamese(),
                _ => new Vietnamese()
            };
        }

        public bool Enabled { get; set; } = true;
        public CultureInfo Culture { get; set; }

        public string GetString(string key, CultureInfo culture = null)
        {
            string value;

            if (Enabled)
            {
                culture ??= Culture ?? CultureInfo.CurrentUICulture;
                Language languageToUse = GetCachedLanguage(culture) ?? _fallback;
                value = languageToUse.GetTranslation(key);

                // Selected language is missing a translation for this key - fall back to English translation
                // if we're not using english already.
                if (string.IsNullOrEmpty(value) && languageToUse != _fallback)
                {
                    value = _fallback.GetTranslation(key);
                }
            }
            else
            {
                value = _fallback.GetTranslation(key);
            }

            return value ?? string.Empty;
        }

        public void Clear()
        {
            _languages.Clear();
        }

        private Language GetCachedLanguage(CultureInfo culture)
        {
            // Find matching translations.
            Language languageToUse = _languages.GetOrAdd(culture.Name, CreateLanguage);

            // If we couldn't find translations for this culture, and it's not a neutral culture
            // then try and find translations for the parent culture instead.
            if (languageToUse == null && !culture.IsNeutralCulture)
            {
                languageToUse = _languages.GetOrAdd(culture.Parent.Name, CreateLanguage);
            }

            return languageToUse;
        }

        public void AddTranslation(string language, string key, string message)
        {
            if (string.IsNullOrEmpty(language)) throw new ArgumentNullException(nameof(language));
            if (string.IsNullOrEmpty(key)) throw new ArgumentNullException(nameof(key));
            if (string.IsNullOrEmpty(message)) throw new ArgumentNullException(nameof(message));

            Language languageInstance = _languages.GetOrAdd(language, c => CreateLanguage(c) ?? new GenericLanguage(c));
            languageInstance.Translate(key, message);
        }
    }

    internal class GenericLanguage : Language
    {
        public GenericLanguage(string name)
        {
            Name = name;
        }

        public override string Name { get; }
    }
}
