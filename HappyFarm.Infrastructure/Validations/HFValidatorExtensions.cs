﻿using FluentValidation;
using HappyFarm.Infrastructure.Services.Interfaces;
using HappyFarm.Infrastructure.Validations.Rules;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace HappyFarm.Infrastructure.Validations
{
    public static class HFValidatorExtensions
    {

        /// <summary>
        /// Validate file has allowed extensions
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ruleBuilder"></param>
        /// <param name="extensions">list extensions allowed</param>
        /// <returns></returns>
        public static IRuleBuilderOptions<T, IFormFile> HasFileExtension<T>(this IRuleBuilder<T, IFormFile> ruleBuilder, params string[] extensions)
        {
            return ruleBuilder.SetValidator(new HasFileExtensionValidator(extensions));
        }

        /// <summary>
        /// Validate file has mime types
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ruleBuilder"></param>
        /// <param name="mimeTypes">list mime types allowed</param>
        /// <returns></returns>
        public static IRuleBuilderOptions<T, IFormFile> HasMimeType<T>(this IRuleBuilder<T, IFormFile> ruleBuilder, params string[] mimeTypes)
        {
            return ruleBuilder.SetValidator(new HasMimeTypeValidator(mimeTypes));
        }

        /// <summary>
        /// Validate file must be less than or equals max length (in byte)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ruleBuilder"></param>
        /// <param name="maxLength"></param>
        /// <returns></returns>
        public static IRuleBuilderOptions<T, IFormFile> HasFileLength<T>(this IRuleBuilder<T, IFormFile> ruleBuilder, double maxLength)
        {
            return ruleBuilder.SetValidator(new HasFileLengthValidator(maxLength));
        }

        /// <summary>
        /// Validate has valid code
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ruleBuilder"></param>
        /// <returns></returns>
        public static IRuleBuilderOptions<T, string> HasValidCode<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder.SetValidator(new HasValidCodeValidator());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ruleBuilder"></param>
        /// <returns></returns>
        public static IRuleBuilderOptions<T, string> IsDigits<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder.SetValidator(new IsDigitsValidator());
        }

        /// <summary>
        /// Validate has human naming (included Vietnamese naming rule)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ruleBuilder"></param>
        /// <returns></returns>
        public static IRuleBuilderOptions<T, string> HasValidHumanNaming<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder.SetValidator(new HasValidHumanNamingValidator());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ruleBuilder"></param>
        /// <returns></returns>
        public static IRuleBuilderOptions<T, string> HasValidDeviceNaming<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder.SetValidator(new HasValidDeviceNamingValidator());
        }

        public static IRuleBuilderOptions<T, IEnumerable<string>> HasValidRoles<T>(this IRuleBuilder<T, IEnumerable<string>> ruleBuilder)
        {
            return ruleBuilder.SetValidator(new HasValidRolesValidator());
        }

        public static IRuleBuilderOptions<T, string> HasValidRole<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder.SetValidator(new HasValidRoleValidator());
        }

        public static IRuleBuilderOptions<T, string> IsValidPhoneNumber<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder.SetValidator(new IsPhoneNumberValidator());
        }

        public static IRuleBuilderOptions<T, string> IsRegionCode<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder.SetValidator(new IsRegionCodeValidator());
        }

        public static IRuleBuilderOptions<T, string> PasswordLength<T>(this IRuleBuilder<T, string> ruleBuilder, int minLength, int maxLength)
        {
            return ruleBuilder.SetValidator(new PasswordLengthValidator(minLength, maxLength));
        }

        public static IRuleBuilderOptions<T, int> GatewayExists<T>(this IRuleBuilder<T, int> ruleBuilder, IGatewayService gatewayService)
        {
            return ruleBuilder.SetValidator(new HasGatewayExists(gatewayService));
        }
    }
}
