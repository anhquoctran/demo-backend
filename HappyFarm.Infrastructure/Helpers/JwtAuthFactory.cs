﻿using HappyFarm.Core.Commons;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace HappyFarm.Infrastructure.Commons
{
    public class JwtAuthFactory
    {
        public enum AuthType
        {
            ForDevice = 0,
            ForWeb = 1,
            ForSystemDevice = 3,
            Both = 2
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string GenerateJwtToken(IDictionary<string, object> payload)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(Constants.Jwt.WEB_AUTH_SECRET);
            var listClaims = new List<Claim>();

            foreach(var item in payload)
            {
                listClaims.Add(new Claim(item.Key, item.Value.ToString()));
            }

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Expires = DateTimeUtils.NowWithUtcOffset().Add(Constants.Jwt.WebTtl).DateTime,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256),
                Subject = new ClaimsIdentity(listClaims),
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        public static KeyValuePair<bool, JwtSecurityToken> DecodeTokenWithoutValidate(string token)
        {
            try
            {
                var claimPrincipal = new JwtSecurityTokenHandler().ReadJwtToken(token);
                return new KeyValuePair<bool, JwtSecurityToken>(false, claimPrincipal);
            }
            catch (Exception)
            {
                return new KeyValuePair<bool, JwtSecurityToken>(false, null);
            }
        }

        public static ClaimsPrincipal GetPrincipalFromExpiredToken(string token)
        {
            try
            {
                var tokenValidationParameters = new TokenValidationParameters
                {
                    ValidateAudience = false,
                    ValidateIssuer = false,
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Constants.Jwt.WEB_REFRESH_TOKEN_SECRET)),
                    ValidateLifetime = false
                };

                var tokenHandler = new JwtSecurityTokenHandler();
                var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out var securityToken);

                if (!(securityToken is JwtSecurityToken jwtSecurityToken) || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
                {
                    return null;
                }

                return principal;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }

        }

        public static KeyValuePair<bool, JwtSecurityToken> ValidateJwtToken(string token)
        {
            var validationParameters = new TokenValidationParameters
            {
                ClockSkew = TimeSpan.FromMinutes(5),
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Constants.Jwt.WEB_AUTH_SECRET)),
                RequireExpirationTime = true,
                RequireSignedTokens = true,
                ValidateLifetime = true,
                ValidateAudience = false,
                ValidateIssuer = false
            };

            try
            {
                var claimsPrincipal = new JwtSecurityTokenHandler()
                  .ValidateToken(token, validationParameters, out var rawValidatedToken);

                return new KeyValuePair<bool, JwtSecurityToken>(true, (JwtSecurityToken)rawValidatedToken);
            }
            catch (Exception)
            {
                return new KeyValuePair<bool, JwtSecurityToken>(false, null);
            }
        }

        public static string GenerateRefreshToken(int authId)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(Constants.Jwt.WEB_REFRESH_TOKEN_SECRET);

            var listClaims = new List<Claim>
            {
                new Claim("id", authId.ToString()),
                new Claim("uuid", StringUtils.GetGuid())
            };

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Expires = DateTimeUtils.NowWithUtcOffset().Add(Constants.Jwt.WebRefreshTtl).DateTime,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256),
                Subject = new ClaimsIdentity(listClaims)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}
