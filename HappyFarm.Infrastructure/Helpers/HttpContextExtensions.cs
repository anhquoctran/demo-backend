﻿using HappyFarm.Infrastructure.Dto;
using Microsoft.AspNetCore.Http;

namespace HappyFarm.Infrastructure.Commons
{
    public static class HttpContextExtensions
    {
        public const string AuthKey = "authKey";

        public static void SetAuthorizedUser(this HttpContext httpContext, AuthorizedUser data)
        {
            if (httpContext.Items.ContainsKey(AuthKey))
            {
                httpContext.Items[AuthKey] = data;
            }
            else
            {
                httpContext.Items.Add(AuthKey, data);
            }
        }

        public static AuthorizedUser GetAuthorizedUser(this HttpContext httpContext)
        {
            if (httpContext.Items.ContainsKey(AuthKey) && httpContext.Items.TryGetValue(AuthKey, out object data))
            {
                return (AuthorizedUser)data;
            }

            return null;
        }
    }
}
