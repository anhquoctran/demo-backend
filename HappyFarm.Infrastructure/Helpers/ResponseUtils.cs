﻿using HappyFarm.Infrastructure.Responses;
using Microsoft.AspNetCore.WebUtilities;
using Newtonsoft.Json;
using System.Net;

namespace HappyFarm.Infrastructure.Commons
{
    public static class ResponseUtils
    {

        /// <summary>
        /// Handle JSON error response as string by status code
        /// </summary>
        /// <param name="statusCode"></param>
        /// <returns></returns>
        public static string GetJsonResponseString(HttpStatusCode statusCode)
        {
            string jsonResponse = statusCode switch
            {
                HttpStatusCode.BadRequest => JsonConvert.SerializeObject(new BadRequestResponse<string[]>(string.Empty)),
                HttpStatusCode.Unauthorized => JsonConvert.SerializeObject(new UnauthorizedResponse()),
                HttpStatusCode.NotFound => JsonConvert.SerializeObject(new NotFoundResponse()),
                HttpStatusCode.MethodNotAllowed => JsonConvert.SerializeObject(new MethodNotAllowedResponse()),
                HttpStatusCode.Forbidden => JsonConvert.SerializeObject(new ForbiddenResponse()),
                HttpStatusCode.UnsupportedMediaType => JsonConvert.SerializeObject(new UnsupportedMediaType()),
                HttpStatusCode.InternalServerError => JsonConvert.SerializeObject(new ServerErrorResponse()),
                HttpStatusCode.ServiceUnavailable => JsonConvert.SerializeObject(new ServiceNotAvailableResponse()),
                HttpStatusCode.BadGateway => JsonConvert.SerializeObject(new ServerErrorResponse()),
                _ => JsonConvert.SerializeObject(new ServerErrorResponse()),
            };
            return jsonResponse;
        }

        /// <summary>
        /// Get raw text error for plain text response
        /// </summary>
        /// <param name="statusCode"></param>
        /// <returns></returns>
        public static string GetRawTextResponse(HttpStatusCode statusCode)
        {
            var code = (int)statusCode;
            return $"<h1>{ReasonPhrases.GetReasonPhrase(code)}</h1>";
        }
    }
}
