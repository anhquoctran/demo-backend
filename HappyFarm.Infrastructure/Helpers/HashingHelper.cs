﻿using System;
using static BCrypt.Net.BCrypt;

namespace HappyFarm.Infrastructure.Helpers
{
    public static class HashingHelper
    {
        /// <summary>
        /// Hashing password using BCrypt algorithm
        /// </summary>
        /// <param name="data">Plain string to hash</param>
        /// <param name="round"></param>
        /// <returns></returns>
        public static string Hash(string data, int round = 10)
        {
            round = (round > 0 && round <= 12) ? round : 10;
            return HashPassword(data, round);
        }

        /// <summary>
        /// Check password using BCrypt algorithm
        /// </summary>
        /// <param name="plain">Plain string to check</param>
        /// <param name="cipher">Cipher text to compare</param>
        /// <returns></returns>
        public static bool CheckPassword(string plain, string cipher)
        {
            try
            {
                return Verify(plain, cipher);
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
