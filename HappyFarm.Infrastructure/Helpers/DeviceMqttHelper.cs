using HappyFarm.Core.Commons;
using HappyFarm.Infrastructure.Dto;
using Newtonsoft.Json;
using System.Linq;

namespace HappyFarm.Infrastructure.Helpers
{
    public static class DeviceMqttHelper
    {
        public static string GetDeviceMqttKey(string uid)
        {
            var deviceName = $"{Constants.DEVICE_KEY_NAME_PREFIX}-{uid}";
            var keyArray = new[] { string.Empty, deviceName, deviceName };

            return JsonConvert.SerializeObject(keyArray);
        }

        public static string BuildMqttAcl(string passwordHash, string[] publishTopic, string[] subscribeTopic)
        {
            if (StringUtils.CheckIsEmptyOrWhitespaceString(passwordHash)) return default;

            var acl = new MqttAclDto
            {
                Passhash = passwordHash
            };

            if (publishTopic != null || publishTopic.Length > 0)
            {
                acl.PublishAcl.UnionWith(publishTopic.Select(x =>
                {
                    return new Acl { Pattern = x };
                }));
            }

            if (subscribeTopic != null || subscribeTopic.Length > 0)
            {
                acl.SubcribeAcl.UnionWith(subscribeTopic.Select(x =>
                {
                    return new Acl { Pattern = x };
                }));
            }
            return JsonConvert.SerializeObject(acl);
        }
    }
}
