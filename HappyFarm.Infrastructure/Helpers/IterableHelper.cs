﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HappyFarm.Infrastructure.Helpers
{
    public static class IterableHelper
    {

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="Q"></typeparam>
        /// <param name="sources"></param>
        /// <param name="another"></param>
        /// <returns></returns>
        public static bool ContainsAnotherArray<Q>(this IEnumerable<Q> sources, IEnumerable<Q> another)
        {
            return sources.Any(another.Contains);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="Q"></typeparam>
        /// <param name="arrays"></param>
        /// <returns></returns>
        public static Q[] ConcatArrays<Q>(params Q[][] arrays)
        {
            var position = 0;
            var outputArray = new Q[arrays.Sum(array => array.Length)];
            foreach (var array in arrays)
            {
                Array.Copy(array, 0, outputArray, position, array.Length);
                position += array.Length;
            }
            return outputArray;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="Q"></typeparam>
        /// <param name="start"></param>
        /// <param name="length"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static Q[] SubArray<Q>(int start, int length, params Q[] data)
        {
            start = start >= 0 ? start : 0;
            length = length >= start || length <= data.Length ? length : data.Length;

            var result = new Q[length];

            Array.Copy(data, start, result, 0, length);

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="Q"></typeparam>
        /// <param name="array"></param>
        /// <param name="index"></param>
        /// <param name="element"></param>
        /// <returns></returns>
        public static bool TryGetElement<Q>(this Q[] array, int index, out Q element)
        {
            if (index >= 0 && index < array.Length)
            {
                element = array[index];
                return true;
            }
            element = default;
            return false;
        }

        public static bool TryGetElement<Q>(this IEnumerable<Q> source, int index, out Q element)
        {
            if (index >= 0 && index < source.Count())
            {
                element = source.ElementAt(index);
                return true;
            }
            element = default;
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="Q"></typeparam>
        /// <param name="source"></param>
        /// <param name="callback"></param>
        public static void Each<Q>(this IEnumerable<Q> source, Action<Q, int> callback)
        {
            var iterableCount = source.Count();
            for (var i = 0; i < iterableCount; i++)
            {
                callback?.Invoke(source.ElementAt(i), i);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="Q"></typeparam>
        /// <param name="source"></param>
        /// <param name="callback"></param>
        public static void Each<Q>(this IEnumerable<Q> source, Action<Q> callback)
        {
            var enumerator = source.GetEnumerator();
            while (enumerator.MoveNext())
            {
                callback?.Invoke(enumerator.Current);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="Q"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static Q GetRandomElement<Q>(this IEnumerable<Q> source)
        {
            var rand = new Random();
            if (!source.Any()) return default;
            var max = source.Count() - 1;
            return source.ElementAtOrDefault(rand.Next(0, max));
        }
    }
}
