﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace HappyFarm.Infrastructure.Helpers
{
    public static class MapperExtensions
    {
        public static void LoadProfileFromCurrentAssembly(this IMapperConfigurationExpression mc)
        {
            GetProfilesFromAssembly()
                .ToList()
                .ForEach(mc.AddProfile);
        }

        private static IEnumerable<Profile> GetProfilesFromAssembly()
        {
            var currentAssembly = Assembly.GetExecutingAssembly();
            var currentAssTypes = currentAssembly.GetTypes();
            return currentAssembly.GetReferencedAssemblies()
                .Select(x => Assembly.Load(x))
                .SelectMany(y => y.GetTypes())
                .Concat(currentAssTypes)
                .Select(Activator.CreateInstance)
                .OfType<Profile>();
        }
    }
}
