﻿using HappyFarm.Core.Commons;
using Microsoft.AspNetCore.Http;

namespace HappyFarm.Infrastructure.Commons
{
    public static class HttpRequestExtensions
    {
        public static bool IsStaticLocationRequest(this HttpRequest request)
        {
            return request.Path.StartsWithSegments(Constants.STATIC_PATH);
        }
    }
}
