﻿using AutoMapper;
using HappyFarm.Core.Models;
using HappyFarm.Infrastructure.Dto;

namespace HappyFarm.Infrastructure.MapperProfile
{
    public class Default : Profile
    {
        public Default()
        {
            CreateMap<User, UserDto>();
            CreateMap<UserDto, User>();

            CreateMap<Farm, FarmDto>();
            CreateMap<FarmDto, Farm>();

            CreateMap<Gateway, GatewayDto>()
                .ForPath(
                    dest => dest.Config,
                    opt => opt.MapFrom(src => src.Config.Object)
                );
            CreateMap<GatewayDto, Gateway>()
                .ForPath(
                    dest => dest.Config.Object,
                    opt => opt.MapFrom(x => x.Config)
                );

            CreateMap<SensorNode, SensorNodeDto>();
            CreateMap<SensorNodeDto, SensorNode>();

            CreateMap<SensorValue, SensorValueDto>();
            CreateMap<SensorValueDto, SensorValue>();
        }
    }
}
