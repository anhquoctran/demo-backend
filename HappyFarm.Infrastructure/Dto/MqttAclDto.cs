﻿using System.Collections.Generic;
using HappyFarm.Core.Commons;
using Newtonsoft.Json;

namespace HappyFarm.Infrastructure.Dto
{
    public class MqttAclDto
    {

        public MqttAclDto()
        {
            PublishAcl = new HashSet<Acl>();
            SubcribeAcl = new HashSet<Acl>();
        }

        [JsonProperty("passhash")]
        public string Passhash { get; set; }

        [JsonProperty("publish_acl")]
        public HashSet<Acl> PublishAcl { get; set; }

        [JsonProperty("subscribe_acl")]
        public HashSet<Acl> SubcribeAcl { get; set; }

        [JsonProperty("allowed_retain", NullValueHandling = NullValueHandling.Ignore)]
        public bool? AllowedRetain { get; set; }

        public static MqttAclDto FromJson(string json)
        {
            if (StringUtils.CheckIsEmptyOrWhitespaceString(json)) return default;
            if (!StringUtils.IsValidJson(json)) return default;

            return JsonConvert.DeserializeObject<MqttAclDto>(json);
        }
    }

    public class Acl
    {
        [JsonProperty("pattern")]
        public string Pattern { get; set; }
    }
}
