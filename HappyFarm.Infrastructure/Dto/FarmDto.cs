﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace HappyFarm.Infrastructure.Dto
{
    public class FarmDto
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("address", NullValueHandling = NullValueHandling.Ignore)]
        public string Address { get; set; }

        [JsonProperty("region_code", NullValueHandling = NullValueHandling.Ignore)]
        public string RegionCode { get; set; }

        [JsonProperty("phone", NullValueHandling = NullValueHandling.Ignore)]
        public string Phone { get; set; }

        [JsonProperty("total_users", NullValueHandling = NullValueHandling.Ignore)]
        public int? TotalUsers { get; set; }

        [JsonProperty("total_gateways", NullValueHandling = NullValueHandling.Ignore)]
        public int? TotalGateways { get; set; }

        [JsonProperty("gateways", NullValueHandling = NullValueHandling.Ignore)]
        public virtual ICollection<GatewayDto> Gateways { get; set; }

        [JsonProperty("users", NullValueHandling = NullValueHandling.Ignore)]
        public virtual ICollection<UserDto> Users { get; set; }

        [JsonProperty("updated_at", NullValueHandling = NullValueHandling.Ignore)]
        public int? UpdatedAt { get; set; }

        [JsonProperty("created_at", NullValueHandling = NullValueHandling.Ignore)]
        public int? CreatedAt { get; set; }
    }
}
