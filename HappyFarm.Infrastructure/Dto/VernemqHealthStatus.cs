﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace HappyFarm.Infrastructure.Dto
{
    public class VernemqHealthStatus
    {
        [JsonProperty("status")]
        public string Status { get; set; }
    }

    public class VernemqBaseResponse<TTableType>
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("table")]
        public TTableType[] Table { get; set; }
    }

    public class VernemqBaseResponse: VernemqBaseResponse<object>
    {

    }

    public class VernemqClusterStatus: VernemqBaseResponse<ClusterTable> { }

    public class ClusterTable
    {
        [JsonProperty("Running")]
        public bool? Running { get; set; }

        [JsonProperty("Node")]
        public string Node { get; set; }
    }


    public class SessionInformationTable
    {
        [JsonProperty("user")]
        public string User { get; set; }

        [JsonProperty("peer_port")]
        public int? PeerPort { get; set; }

        [JsonProperty("is_online")]
        public bool? IsOnline { get; set; }

        [JsonProperty("mountpoint")]
        public string Mountpoint { get; set; }

        [JsonProperty("client_id")]
        public string ClientId { get; set; }

        [JsonProperty("peer_host")]
        public string PeerHost { get; set; }
    }

    public class SessionInformation: VernemqBaseResponse<SessionInformationTable> { }

    public class InstalledListenerTable
    {
        [JsonProperty("max_conns")]
        public int? MaxConns { get; set; }

        [JsonProperty("port")]
        public string Port { get; set; }

        [JsonProperty("mountpoint")]
        public string Mountpoint { get; set; }

        [JsonProperty("ip")]
        public string Ip { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }
    }

    public class InstalledListeners: VernemqBaseResponse<InstalledListenerTable> { }

    public class PluginTable
    {
        [JsonProperty("Hook(s)")]
        public string HookS { get; set; }

        [JsonProperty("Plugin")]
        public string Plugin { get; set; }

        [JsonProperty("M:F/A")]
        public string MFA { get; set; }

        [JsonProperty("Type")]
        public string Type { get; set; }
    }

    public class InstalledPlugins: VernemqBaseResponse<PluginTable> { }

    public class VernemqHeartbeat
    {
        [JsonProperty("status")]
        public bool Status { get; set; }

        [JsonProperty("cluster_status")]
        public ClusterTable[] ClusterStatus { get; set; }

        [JsonProperty("session_informations")]
        public SessionInformationTable[] SessionInformations { get; set; }

        [JsonProperty("installed_plugins")]
        public PluginTable[] InstalledPlugins { get; set; }

        [JsonProperty("installed_listeners")]
        public InstalledListenerTable[] InstalledListeners { get; set; }
    }

}
