﻿namespace HappyFarm.Infrastructure.Dto
{
    public class ChangePasswordDto
    {
        public string OldPassword { get; set; }

        public string NewPassword { get; set; }

        public string RepeatNewPassword { get; set; }
    }
}
