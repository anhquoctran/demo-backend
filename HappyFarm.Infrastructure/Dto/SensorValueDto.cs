﻿using Newtonsoft.Json;

namespace HappyFarm.Infrastructure.Dto
{
    public class SensorValueDto
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("temperature", NullValueHandling = NullValueHandling.Ignore)]
        public decimal Temperature { get; set; }

        [JsonProperty("air_humidity", NullValueHandling = NullValueHandling.Ignore)]
        public decimal AirHumidity { get; set; }

        [JsonProperty("ph_scale", NullValueHandling = NullValueHandling.Ignore)]
        public decimal PhScale { get; set; }

        [JsonProperty("light_intensitiy", NullValueHandling = NullValueHandling.Ignore)]
        public decimal LightIntensity { get; set; }

        [JsonProperty("soil_humidity", NullValueHandling = NullValueHandling.Ignore)]
        public decimal SoilHumidity { get; set; }

        [JsonProperty("windspeed", NullValueHandling = NullValueHandling.Ignore)]
        public decimal WindSpeed { get; set; }

        [JsonProperty("rainfall", NullValueHandling = NullValueHandling.Ignore)]
        public decimal RainFall { get; set; }

        [JsonProperty("tds", NullValueHandling = NullValueHandling.Ignore)]
        public decimal Tds { get; set; }

        [JsonProperty("node_id", NullValueHandling = NullValueHandling.Ignore)]
        public int? NodeId { get; set; }

        [JsonProperty("sensor_node", NullValueHandling = NullValueHandling.Ignore)]
        public virtual SensorNodeDto SensorNode { get; set; }

        [JsonProperty("created_at", NullValueHandling = NullValueHandling.Ignore)]
        public int? CreatedAt { get; set; }


    }
}
