﻿using Newtonsoft.Json;

namespace HappyFarm.Infrastructure.Dto
{
    public class DropdownDto
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("code", NullValueHandling = NullValueHandling.Ignore)]
        public string Code { get; set; }
    }
}
