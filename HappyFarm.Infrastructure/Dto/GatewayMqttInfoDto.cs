﻿using Newtonsoft.Json;

namespace HappyFarm.Infrastructure.Dto
{
    public class GatewayMqttInfoDto
    {
        [JsonProperty("mqtt")]
        public DetailMqttInfo Mqtt { get; set; }

        [JsonProperty("systime")]
        public uint CurrentTime { get; set; }
    }

    public class DetailMqttInfo
    {
        [JsonProperty("host")]
        public string Server { get; set; }

        [JsonProperty("port")]
        public ushort Port { get; set; }

        [JsonProperty("uname")]
        public string Username { get; set; }

        [JsonProperty("pass")]
        public string Password { get; set; }

        [JsonProperty("topic")]
        public string Topic { get; set; }
    }
}
