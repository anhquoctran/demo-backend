﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace HappyFarm.Infrastructure.Dto
{
    public class SensorValueDataItem
    {
        [JsonProperty("node_id")]
        public int NodeId { get; set; }

        [JsonProperty("temperature")]
        public decimal Temperature { get; set; }

        [JsonProperty("air_humidity")]
        public decimal AirHumidity { get; set; }

        [JsonProperty("ph_scale")]
        public decimal PhScale { get; set; }

        [JsonProperty("light_intensitiy")]
        public decimal LightIntensity { get; set; }

        [JsonProperty("soil_humidity")]
        public decimal SoilHumidity { get; set; }

        [JsonProperty("windspeed")]
        public decimal WindSpeed { get; set; }

        [JsonProperty("rainfall")]
        public decimal RainFall { get; set; }

        [JsonProperty("tds")]
        public decimal Tds { get; set; }

        [JsonProperty("created_at", NullValueHandling = NullValueHandling.Ignore)]
        public int? CreatedAt { get; set; }
    }

    public class SensorValuesCmd
    {
        [JsonProperty("gateway_id")]
        public int GatewayId { get; set; }

        [JsonProperty("items")]
        public IEnumerable<SensorValueDataItem> Items { get; set; }
    }
}
