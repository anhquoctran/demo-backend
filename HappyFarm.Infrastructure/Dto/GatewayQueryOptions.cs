﻿namespace HappyFarm.Infrastructure.Dto
{
    public class GatewayQueryOptions
    {
        public string Keyword { get; set; }
        public int? UserId { get; set; }
        public string Role { get; set; }
        public int? FarmId { get; set; }
    }
}
