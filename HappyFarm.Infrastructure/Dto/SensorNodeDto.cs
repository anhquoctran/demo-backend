﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace HappyFarm.Infrastructure.Dto
{
    public class SensorNodeDto
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonIgnore]
        public int? GatewayId { get; set; }

        [JsonProperty("updated_at", NullValueHandling = NullValueHandling.Ignore)]
        public int? UpdatedAt { get; set; }

        [JsonProperty("created_at", NullValueHandling = NullValueHandling.Ignore)]
        public int? CreatedAt { get; set; }

        [JsonProperty("gateway", NullValueHandling = NullValueHandling.Ignore)]
        public virtual GatewayDto Gateway { get; set; }

        [JsonProperty("sensor_values", NullValueHandling = NullValueHandling.Ignore)]
        public virtual ICollection<SensorValueDto> SensorValues { get; set; }
    }
}
