﻿using HappyFarm.Core.Enums;

namespace HappyFarm.Infrastructure.Dto
{
    public class PublishMessageData<T>
    {
        public string Topic { get; set; } = "/";

        public bool Retained { get; set; } = false;

        public T Data { get; set; }

        public QualityOfService QualityOfService { get; set; } = QualityOfService.ExactlyOnce;
    }
}
