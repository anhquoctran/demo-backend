﻿using System;
using System.Collections.Generic;
using HappyFarm.Core.Commons;
using Newtonsoft.Json;

namespace HappyFarm.Infrastructure.Dto
{
    public class CmdStateResponse
    {
        [JsonProperty("gateway_id")]
        public int GatewayId { get; set; }

        [JsonProperty("status")]
        public bool State { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("time")]
        public int Time { get; set; } = DateTimeUtils.ConvertToUnixTime(DateTime.Now);

        [JsonProperty("errors", NullValueHandling = NullValueHandling.Ignore)]
        public IDictionary<string, IList<string>> Errors { get; set; }

    }
}
