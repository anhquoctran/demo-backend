﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HappyFarm.Core.Commons;
using Microsoft.EntityFrameworkCore;

namespace HappyFarm.Infrastructure.Dto
{
    public sealed class PaginatedList<TSource> : List<TSource> where TSource : class
    {
        public int PageIndex { get; private set; }
        public int TotalPages { get; private set; }
        public int TotalItems { get; private set; }
        public int PageSize { get; private set; }

        public PaginatedList(IEnumerable<TSource> items, int count, int pageIndex, int pageSize)
        {
            if (items == null) return;
            var enumerable = items as TSource[] ?? items.ToArray();
            PageIndex = pageIndex;
            TotalPages = (int)Math.Ceiling(count / (double)pageSize);
            TotalItems = count;
            PageSize = pageSize;
            AddRange(enumerable);
        }

        public bool HasPreviousPage => PageIndex > 1;

        public bool HasNextPage => PageIndex < TotalPages;

        public static PaginatedList<TSource> Empty(int pageSize)
        {
            return new PaginatedList<TSource>(new List<TSource>(), 0, 0, pageSize);
        }

        public static async Task<PaginatedList<TSource>> CreateAsync(IQueryable<TSource> source, int? pageIndex, int? pageSize, int? preCount = null)
        {
            var pg = pageIndex ?? Constants.PaginationConfiguration.DEFAULT_PAGE_NUMBER;
            var pgSize = pageSize ?? Constants.PaginationConfiguration.DEFAULT_PAGE_SIZE;

            int count;
            if (preCount != null)
            {
                count = preCount.GetValueOrDefault();
            }
            else
            {
                count = await source.CountAsync();
            }

            var items = source.Skip((pg - 1) * pgSize).Take(pgSize);
            return new PaginatedList<TSource>(await items.ToArrayAsync(), count, pg, pgSize);
        }

        public static PaginatedList<TSource> Create(IQueryable<TSource> source, int? pageIndex, int? pageSize, int? preCount = null)
        {
            var pg = pageIndex ?? 1;
            var pgSize = pageSize ?? 20;

            int count;
            if (preCount != null)
            {
                count = preCount.GetValueOrDefault();
            }
            else
            {
                count = source.Count();
            }

            var items = source.Skip((pg - 1) * pgSize).Take(pgSize);
            return new PaginatedList<TSource>(items.ToArray(), count, pg, pgSize);
        }

        public static PaginatedList<TSource> Empty()
        {
            return new PaginatedList<TSource>(new TSource[0], 0, 1, 20);
        }
    }
}
