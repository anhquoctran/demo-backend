﻿using System.Collections.Generic;
using HappyFarm.Core.Models;
using Newtonsoft.Json;

namespace HappyFarm.Infrastructure.Dto
{
    public class GatewayDto
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("password", NullValueHandling = NullValueHandling.Ignore)]
        public string Password { get; set; }

        [JsonProperty("code", NullValueHandling = NullValueHandling.Ignore)]
        public string Code { get; set; }

        [JsonProperty("config", NullValueHandling = NullValueHandling.Ignore)]
        public Config Config { get; set; }

        [JsonIgnore]
        public int? FarmId { get; set; }

        [JsonProperty("farm", NullValueHandling = NullValueHandling.Ignore)]
        public FarmDto Farm { get; set; }

        [JsonProperty("sensor_nodes", NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<SensorNodeDto> SensorNodes { get; set; }

        [JsonProperty("created_at", NullValueHandling = NullValueHandling.Ignore)]
        public int? CreatedAt { get; set; }

        [JsonProperty("updated_at", NullValueHandling = NullValueHandling.Ignore)]
        public int? UpdatedAt { get; set; }
    }
}
