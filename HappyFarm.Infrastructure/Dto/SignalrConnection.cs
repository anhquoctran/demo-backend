﻿namespace HappyFarm.Infrastructure.Dto
{
    public class SignalrConnection
    {
        public string ConnectionId { get; set; }
        public int ConnectedTime { get; set; }
        public string RemoteAddress { get; set; }
    }
}
