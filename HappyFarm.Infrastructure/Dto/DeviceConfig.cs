﻿using Newtonsoft.Json;

namespace HappyFarm.Infrastructure.Dto
{
    public class DeviceConfig
    {
        [JsonProperty("server")]
        public string Server { get; set; }

        [JsonProperty("port")]
        public ushort? Port { get; set; } = 1883;

        [JsonProperty("passsystem")]
        public string Password { get; set; }

        [JsonProperty("ssid")]
        public string WiFiSSID { get; set; }

        [JsonProperty("pass")]
        public string WiFiPassword { get; set; }

        [JsonProperty("timeout")]
        public uint? Timeout { get; set; } = 60;

        [JsonProperty("firmware_version")]
        public string FirmwareVersion { get; set; }

        [JsonProperty("reset")]
        public bool? Reset { get; set; } = false;

        [JsonProperty("local_ip")]
        public string LocalIP { get; set; }

        [JsonProperty("update_firmware")]
        public bool? UpdateFirmware { get; set; } = false;

        [JsonProperty("wifi_version")]
        public string WiFiVersion { get; set; }
    }
}
