﻿using System.Collections.Generic;

namespace HappyFarm.Infrastructure.Dto
{
    public class AuthenticatedUserDto
  {
    public int Id { get; set; }

    public string[] Role { get; set; }

    public string Avatar { get; set; }

    public string Token { get; set; }

    public IEnumerable<string> Permissions { get; set; }
  }
}
