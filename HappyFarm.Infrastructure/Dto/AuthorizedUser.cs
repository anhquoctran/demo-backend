﻿namespace HappyFarm.Infrastructure.Dto
{
    public class AuthorizedUser
    {
        public int Id { get; set; }

        public string Role { get; set; }
    }
}
