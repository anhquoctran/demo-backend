﻿using AutoMapper;
using HappyFarm.Core.Commons;
using HappyFarm.Core.Enums;
using HappyFarm.Core.Models;
using HappyFarm.Core.Repositories;
using HappyFarm.Core.UnitOfWork;
using HappyFarm.Infrastructure.Dto;
using HappyFarm.Infrastructure.Helpers;
using HappyFarm.Infrastructure.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace HappyFarm.Infrastructure.Services.Implementations
{
    public class GatewayService : CrudService<Gateway, GatewayDto>, IGatewayService
    {
        protected override IRepository<Gateway> Repository => _unitOfWork.GatewayRepository;
        private readonly IUserService _userService;
        private readonly IDistributedService _distributedService;
        private readonly IMessageQueueService _messageQueueService;

        public GatewayService(
            IMapper mapper,
            IUnitOfWork unitOfWork,
            ILogger<FarmService> logger,
            IUserService userService,
            IDistributedService distributedService,
            IMessageQueueService messageQueueService
            )
            : base(mapper, unitOfWork, logger)
        {
            _userService = userService;
            _distributedService = distributedService;
            _messageQueueService = messageQueueService;
        }

        public async Task<PaginatedList<GatewayDto>> FindAllAsync(string keyword, int? userId, int? page, int? pageSize, params int[] farms)
        {
            try
            {
                var query = AsQuery();

                if (!StringUtils.CheckIsEmptyOrWhitespaceString(keyword))
                {
                    keyword = keyword.Trim().ToLower();
                    query = query.Where(x => 
                        EF.Functions.Like(x.Name, $"%{keyword}%") ||
                        EF.Functions.Like(x.Code, $"%{keyword}%") ||
                        EF.Functions.Like(x.Farm.Name, $"%{keyword}%")
                    );
                }

                if (userId.HasValue && userId > 0)
                {
                    var user = await _unitOfWork.UserRepository.FindByIdAsync(userId.Value);
                    if (user != null && user.Role == User.MANAGER_ROLE)
                    {
                        if (user.FarmId.HasValue)
                        {
                            query = query.Where(x => x.FarmId == user.FarmId);
                        }
                        else
                        {
                            return PaginatedList<GatewayDto>.Empty();
                        }
                    }
                }
                else
                {
                    if (farms != null && farms.Length > 0)
                    {
                        query = query.Where(x => farms.Contains(x.FarmId));
                    }
                }

                var result = query
                    .OrderByDescending(x => x.CreatedAt)
                    .Select(x => new GatewayDto
                {
                    Id = x.Id,
                    Name = x.Name,
                    Code = x.Code,
                    Config = x.Config.Object,
                    Farm = x.Farm != null ? new FarmDto { Id = x.Farm.Id, Name = x.Farm.Name } : null,
                    CreatedAt = x.CreatedAt,
                    UpdatedAt = x.UpdatedAt
                });

                return await PaginatedList<GatewayDto>.CreateAsync(result, page, pageSize);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }
        }

        public async Task<GatewayDto> GetGateway(int gatewayId)
        {
            try
            {
                return await FindByIdAsync(gatewayId);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }
        }

        public async Task<KeyValuePair<OperationStatus, GatewayDto>> GetGatewayDetailForManager(int id, int userId)
        {
            try
            {
                var user = await _userService.FindAsync(u => u.Id == userId);

                if (user?.FarmId == null)
                    return new KeyValuePair<OperationStatus, GatewayDto>(OperationStatus.NotFound, null);
                var query = AsQuery()
                    .Where(x => x.FarmId == user.FarmId)
                    .Select(x =>
                        new Gateway {
                            Id = x.Id,
                            Name = x.Name,
                            Code = x.Code,
                            SensorNodes = null,
                            CreatedAt = x.CreatedAt,
                            UpdatedAt = x.UpdatedAt
                        });

                var result = await query.FirstOrDefaultAsync();
                if (result == null)
                    return new KeyValuePair<OperationStatus, GatewayDto>(OperationStatus.NotFound, null);
                {
                    var nodes = await _unitOfWork.SensorNodeRepository.AsQuery()
                        .Where(x => x.GatewayId == result.Id)
                        .Select(x => new SensorNode
                        {
                            Id = x.Id,
                            Name = x.Name,
                            SensorValues = x.SensorValues
                                .OrderByDescending(y => y.CreatedAt)
                                .Take(1)
                                .ToArray(),
                            CreatedAt = x.CreatedAt,
                            UpdatedAt = x.UpdatedAt
                        }).ToListAsync();
                    result.SensorNodes = nodes;
                    return new KeyValuePair<OperationStatus, GatewayDto>(OperationStatus.Succeed, EntityToDto(result));
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return new KeyValuePair<OperationStatus, GatewayDto>(OperationStatus.Exception, null);

            }
        }

        public async Task<IEnumerable<GatewayDto>> GetAllGatewaysByManager(int managerId)
        {
            try
            {
                var user = await _userService.FindAsync(u => u.Id == managerId);

                if (user?.FarmId == null) return null;
                var result = FindAll(gw => gw.FarmId == user.FarmId)
                    .Select(x => new GatewayDto 
                    { 
                        Id = x.Id,
                        Name = x.Name,
                        Code = x.Code,
                        CreatedAt = x.CreatedAt
                    });

                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }
        }

        public async Task<GatewayMqttInfoDto> GetMqttInfo(string deviceCode)
        {
            try
            {
                deviceCode = deviceCode.Trim().ToLower();
                var device = await FindAsync(d => d.Code.Trim().ToLower() == deviceCode);
                if (device == null) return null;
                var deviceKey = DeviceMqttHelper.GetDeviceMqttKey(device.Id.ToString());

                var mqttAcl = GetDeviceMqttAclConfig(HashingHelper.Hash(device.Password), device.Id);
                await _distributedService.SetStringAsync(deviceKey, mqttAcl, DistributedService.DEVICE_ACL_DB_INDEX);

                var (mqttHost, mqttPort) = GetMqttHostAndPort();

                return new GatewayMqttInfoDto
                {
                    CurrentTime = (uint)DateTimeUtils.NowUnix(),
                    Mqtt = new DetailMqttInfo
                    {
                        Username = $"hpdev-{device.Id}",
                        Password = device.Password,
                        Port = mqttPort,
                        Server = mqttHost,
                        Topic = $"/hpdev/{device.Id}"
                    }
                };
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }
        }

        public async Task<OperationStatus> CreateGateway(GatewayDto gateway, int userId)
        {
            try
            {
                var user = await _unitOfWork.UserRepository.FindByIdAsync(userId);
                if (user == null) return OperationStatus.Failed;
                if (user.Role == User.MANAGER_ROLE && gateway.FarmId != user.FarmId)
                {
                    return OperationStatus.AccessDenied;
                }
                gateway.Password = StringUtils.GenerateRandomString();
                var result = await CreateAsync(gateway);
                if (result == null || result.Id <= 0) return OperationStatus.Failed;
                var deviceKey = DeviceMqttHelper.GetDeviceMqttKey(result.Id.ToString());
                var mqttAcl = GetDeviceMqttAclConfig(HashingHelper.Hash(result.Password), result.Id);
                await _distributedService.SetStringAsync(deviceKey, mqttAcl, DistributedService.DEVICE_ACL_DB_INDEX);
                return OperationStatus.Succeed;

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return OperationStatus.Exception;
            }
        }

        private static string GetDeviceMqttAclConfig(string password, int deviceId)
        {
            var topicPrefix = $"/hpdev/{deviceId}";

            return DeviceMqttHelper.BuildMqttAcl(password,
              new[] { $"{topicPrefix}/status", $"{topicPrefix}/command" },
              new[] { $"{topicPrefix}/command", "/user/#"
            });
        }

        private static (string, ushort) GetMqttHostAndPort()
        {
            var host = StringUtils.CheckIsEmptyOrWhitespaceString(AppConfig.Config.Mqtt.Host) ? Constants.VERNREMQ_HOST : AppConfig.Config.Mqtt.Host;
            var port = AppConfig.Config.Mqtt.Port ?? Constants.VERNEMQ_PORT;
            return (host, port);
        }

        public async Task<IEnumerable<int>> GetListGatewayIdByFarmId(int farmId)
        {
            try
            {
                var query = AsQuery()
                       .Where(x => x.FarmId == farmId)
                       .Select(x => x.Id);

                return await query.ToArrayAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }
        }

        public async Task<int> GetGatewayIdByCode(string code)
        {
            try
            {
                if (StringUtils.CheckIsEmptyOrWhitespaceString(code)) return 0;
                code = code.Trim().ToLower();
                var query = AsQuery()
                    .Where(x => x.Code.Trim().ToLower() == code)
                    .Select(x => x.Id);

                return await query.FirstOrDefaultAsync();

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return -1;
            }
        }

        public async Task<OperationStatus> IsExistById(int gatewayId)
        {
            try
            {
                var gw = await ExistsByIdAsync(gatewayId);
                return gw ? OperationStatus.Succeed : OperationStatus.NotFound;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return OperationStatus.Exception;
            }
        }

        public async Task<OperationStatus> UpdateGateway(GatewayDto gateway, int userId)
        {
            try
            {
                var user = await _unitOfWork.UserRepository.FindByIdAsync(userId);
                if (user != null)
                {
                    if (user.Role == User.MANAGER_ROLE && gateway.FarmId != user.FarmId)
                    {
                        return OperationStatus.AccessDenied;
                    }
                    var gwFromDb = await FindByIdAsync(gateway.Id);
                    if (gwFromDb == null)
                    {
                        return OperationStatus.Exception;
                    }

                    gwFromDb.Name = gateway.Name;
                    gwFromDb.Code = gateway.Code;
                    gwFromDb.FarmId = gateway.FarmId;

                    var updateResult = await UpdateAsync(gwFromDb);

                    if (!updateResult) return OperationStatus.Failed;
                    var deviceKey = DeviceMqttHelper.GetDeviceMqttKey(gwFromDb.Id.ToString());
                    var mqttAcl = GetDeviceMqttAclConfig(HashingHelper.Hash(gwFromDb.Password), gwFromDb.Id);
                    await _distributedService.SetStringAsync(deviceKey, mqttAcl, DistributedService.DEVICE_ACL_DB_INDEX);

                    return OperationStatus.Succeed;

                }
                await UpdateAsync(gateway);
                return OperationStatus.Succeed;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return OperationStatus.Exception;
            }
        }

        public async Task<OperationStatus> CheckExistsBy(Expression<Func<Gateway, bool>> predicate)
        {
            try
            {
               
                return await ExistsByAsync(predicate) ? OperationStatus.Succeed : OperationStatus.Failed;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return OperationStatus.Exception;
            }
        }

        public async Task<OperationStatus> UpdateSettings(DeviceConfig configData, IEnumerable<int> gatewayId, bool sendToDevice = false)
        {
            var transaction = _unitOfWork.BeginTransaction();
            try
            {
                var result = await AsQuery(gw => gatewayId.Contains(gw.Id)).ToArrayAsync();
                
                var listDevices = EntityToDto(result);
                if (!listDevices.Any()) return OperationStatus.Failed;
                foreach(var item in listDevices)
                {
                    if (!StringUtils.CheckIsEmptyOrWhitespaceString(configData.Password))
                    {
                        item.Config.PasswordSystem = configData.Password;
                    }

                    if (configData.Port.HasValue && configData.Port > 0 && configData.Port <= ushort.MaxValue)
                    {
                        item.Config.Port = configData.Port;
                    }

                    if (configData.Timeout.HasValue && configData.Timeout > 0 && configData.Timeout <= uint.MaxValue)
                    {
                        item.Config.Timeout = configData.Timeout;
                    }

                    if (!StringUtils.CheckIsEmptyOrWhitespaceString(configData.Server))
                    {
                        item.Config.Server = configData.Server;
                    }

                    if (!StringUtils.CheckIsEmptyOrWhitespaceString(configData.LocalIP))
                    {
                        item.Config.LocalIp = configData.LocalIP;
                    }

                    if (!StringUtils.CheckIsEmptyOrWhitespaceString(configData.WiFiSSID))
                    {
                        item.Config.WiFiSsid = configData.WiFiSSID;
                    }

                    if (!StringUtils.CheckIsEmptyOrWhitespaceString(configData.WiFiPassword))
                    {
                        item.Config.WiFiSsid = configData.WiFiPassword;
                    }

                    if (!StringUtils.CheckIsEmptyOrWhitespaceString(configData.WiFiVersion))
                    {
                        item.Config.WiFiVersion = configData.WiFiVersion;
                    }

                    if (!StringUtils.CheckIsEmptyOrWhitespaceString(configData.FirmwareVersion))
                    {
                        item.Config.FirmwareVersion = configData.FirmwareVersion;
                    }
                }

                var updatedToDb = await UpdateRangeAsync(listDevices);
                await transaction.CommitAsync();
                if (!updatedToDb) return OperationStatus.Failed;
                if (!_messageQueueService.IsRunning) return OperationStatus.Failed;
                var success = 0;
                var listId = listDevices.Select(x => x.Id).ToArray();
                foreach (var id in listId)
                {
                    var topic = $"/hpdev/{id}/command";
                    var hfCmd = new HFCommand<DeviceConfig>
                    {
                        Command = (int) Constants.DeviceCommand.CommandUpdateSetting
                    };
                    var packetData = new PublishMessageData<HFCommand<DeviceConfig>>
                    {
                        Data = hfCmd,
                        Topic = topic,
                        Retained = false,
                    };
                    var isOk = await _messageQueueService.Publish(packetData);
                    if (isOk) success++;
                }

                return success == listId.Length ? OperationStatus.Succeed : OperationStatus.Failed;
            }
            catch (Exception ex)
            {
                await transaction.RollbackAsync();
                _logger.LogError(ex.Message);
                return OperationStatus.Exception;
            }
        }

        public async Task<GatewayDto> GetGatewayDetail(int gatewayId)
        {
            try
            {
                return await AsQuery(x => x.Id == gatewayId)
                        .Include(x => x.Farm)
                        .Select(x => new GatewayDto
                        {
                            Id = x.Id,
                            Name = x.Name,
                            Code = x.Code,
                            Password = x.Code,
                            Config = x.Config.Object,
                            Farm = x.Farm != null ? new FarmDto { Id = x.Id, Name = x.Name } : null,
                            UpdatedAt = x.UpdatedAt,
                            CreatedAt = x.CreatedAt,
                        }).FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }
        }

        public async Task<IEnumerable<DropdownDto>> GetGatewayDropdown(int? farmId)
        {
            try
            {
                var query = AsQuery();

                if (farmId.HasValue && farmId > 0)
                {
                    query = query.Where(x => x.FarmId == farmId);
                }

                return await query.Select(x => new DropdownDto
                {
                    Id = x.Id,
                    Name = x.Name,
                    Code = x.Code
                }).ToArrayAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }
        }

        public async Task<OperationStatus> Delete(IEnumerable<int> items)
        {
            try
            {
                var checkDependendNodes = await AsQuery(x => items.Contains(x.Id) && x.SensorNodes.Any())
                    .AnyAsync();

                if (checkDependendNodes)
                {
                    return OperationStatus.Failed;
                }

                var result = await DeleteAsync(items);
                if (result < 0) return OperationStatus.Failed;
                {
                    var redisKeys = items.Select(x => DeviceMqttHelper.GetDeviceMqttKey(x.ToString())).ToArray();

                    var res = _distributedService.BatchDelete(redisKeys, DistributedService.DEVICE_ACL_DB_INDEX);
                    return res ? OperationStatus.Succeed : OperationStatus.Failed;
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return OperationStatus.Exception;
            }
        }
    }
}
