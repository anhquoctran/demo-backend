﻿using AutoMapper;
using HappyFarm.Core.Enums;
using HappyFarm.Core.Models;
using HappyFarm.Core.Repositories;
using HappyFarm.Core.UnitOfWork;
using HappyFarm.Infrastructure.Dto;
using HappyFarm.Infrastructure.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HappyFarm.Infrastructure.Services.Implementations
{
    public class SensorNodeService : CrudService<SensorNode, SensorNodeDto>, ISensorNodeService
    {
        public SensorNodeService(
            IMapper mapper,
            IUnitOfWork unitOfWork,
            ILogger<FarmService> logger,
            IGatewayService gatewayService
        ): base(mapper, unitOfWork, logger)
        {
            _gatewayService = gatewayService;
        }

        protected override IRepository<SensorNode> Repository => _unitOfWork.SensorNodeRepository;
        private readonly IGatewayService _gatewayService;

        public async Task<IEnumerable<SensorNodeDto>> GetSensorsByGateway(int gatewayId)
        {
            try
            {
                var query = AsQuery()
                   .Where(n => n.GatewayId == gatewayId);

                var nodes = await (
                    from q in query
                    select new SensorNode
                    {
                        Id = q.Id,
                        Name = q.Name,
                        Code = q.Code,
                        CreatedAt = q.CreatedAt,
                        UpdatedAt = q.UpdatedAt,
                        SensorValues = q.SensorValues.Any() ?
                            q.SensorValues.Select(sv => new SensorValue
                            {
                                Id = sv.Id,
                                AirHumidity = sv.AirHumidity,
                                LightIntensity = sv.LightIntensity,
                                PhScale = sv.PhScale,
                                WindSpeed = sv.WindSpeed,
                                RainFall = sv.RainFall,
                                Temperature = sv.Temperature,
                                SoilHumidity = sv.SoilHumidity,
                                Tds = sv.Tds,
                                CreatedAt = sv.CreatedAt
                            }).OrderByDescending(x => x.CreatedAt).Take(1).ToArray() : null
                    }
                   ).ToArrayAsync();
                return EntityToDto(nodes);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }
        }

        public async Task<PaginatedList<SensorNodeDto>> GetSensorsList(string keyword, int? farmId, int? gatewayId, int? page, int? pageSize)
        {
            try
            {
                var query = AsQuery()
                    .Include(x => x.Gateway)
                    .AsQueryable();

                if (!string.IsNullOrEmpty(keyword))
                {
                    keyword = keyword.Trim().ToLower();
                    query = query.Where(x => x.Name.Trim().ToLower() == keyword);
                }

                if (gatewayId.HasValue && gatewayId.Value > 0)
                {
                    query = query.Where(x => x.GatewayId == gatewayId);
                }

                if (farmId.HasValue && farmId.Value > 0)
                {
                    var listGwId = await _gatewayService.GetListGatewayIdByFarmId(farmId.Value);
                    query = query.Where(x => listGwId.Contains(x.GatewayId));
                }

                var resultSelect = query
                    .OrderByDescending(x => x.CreatedAt)
                    .Select(x => new SensorNodeDto
                {
                    Id = x.Id,
                    Name = x.Name,
                    Code = x.Code,
                    Gateway = x.Gateway != null ? 
                    new GatewayDto 
                    { 
                        Id = x.Gateway.Id, 
                        Name = x.Gateway.Name,
                        Code = x.Gateway.Code 
                    } : null,
                    CreatedAt = x.CreatedAt,
                    UpdatedAt = x.UpdatedAt
                });

                return await PaginatedList<SensorNodeDto>.CreateAsync(resultSelect, page, pageSize);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }
        }

        public async Task<IEnumerable<DropdownDto>> GetSensorsDropdownByGateway(int gatewayId)
        {
            try
            {
                var query = AsQuery()
                    .Where(x => x.GatewayId == gatewayId)
                    .Select(x => new SensorNode { Id = x.Id, Name = x.Name, Code = x.Code });

                var tmp = EntityToDto(await query.ToArrayAsync());
                var result = tmp.Select(x => new DropdownDto { Id = x.Id, Name = x.Name, Code = x.Code });
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }
        }

        public async Task<OperationStatus> AddSensorNode(SensorNodeDto sensor)
        {
            try
            {
                var result = await CreateAsync(sensor);
                if (result != null && result.Id > 0)
                {
                    return OperationStatus.Succeed;
                }

                return OperationStatus.Failed;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return OperationStatus.Exception;
            }
        }

        public async Task<OperationStatus> UpdateSensorNode(SensorNodeDto sensor)
        {
            try
            {
                await UpdateAsync(sensor);
                return OperationStatus.Succeed;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return OperationStatus.Exception;
            }
        }

        public async Task<int> CountNodeByGateway(int gatewayId)
        {
            try
            {
                var count = await AsQuery(x => x.GatewayId == gatewayId)
                    .CountAsync();
                return count;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return -1;
            }
        }

        public async Task<bool> ValidateNodeOwnedByGateway(int gatewayId, IEnumerable<string> listNodeId)
        {
            try
            {
                var exist = await AsQuery()
                    .Where(x => x.GatewayId == gatewayId)
                    .AllAsync(x => listNodeId.Contains(x.Code));
                return exist;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return false;
            }
        }

        public async Task<int> GetIdByCode(string nodeCode)
        {
            try
            {
                var item = await FindAsync(x => x.Code == nodeCode);
                return item != null ? item.Id : 0;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return -1;
            }
        }

        public async Task<OperationStatus> IsExistsById(int sensorNodeId)
        {
            try
            {
                return await ExistsByIdAsync(sensorNodeId) ? OperationStatus.Succeed : OperationStatus.NotFound;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return OperationStatus.Exception;
            }
        }

        public async Task<SensorNodeDto> GetSensorNode(int nodeId, bool mustIncludeValue = false)
        {
            try
            {
                var query = AsQuery(x => x.Id == nodeId);

                if (mustIncludeValue)
                {
                    query = query.Include(x => x.SensorValues);
                }

                var sensorNode = await query.FirstOrDefaultAsync();

                return EntityToDto(sensorNode) ;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }
        }

        public async Task<OperationStatus> IsUserSensorNodeHasSameFarm(int nodeId, int userId)
        {
            try
            {
                var user = await _unitOfWork.UserRepository.FindByIdAsync(userId);
                var node = await AsQuery(x => x.Id == nodeId)
                    .Include(x => x.Gateway)
                    .FirstOrDefaultAsync();

                return (
                    user != null && node != null && user.FarmId == node.Gateway.FarmId
                    ) ? OperationStatus.Succeed : OperationStatus.Incorrect;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return OperationStatus.Exception;
            }
        }

        public async Task<OperationStatus> DeleteNodes(IEnumerable<int> nodeIds)
        {
            try
            {
                var deleted = await DeleteAsync(nodeIds);

                return deleted >= 0 ? OperationStatus.Succeed : OperationStatus.Incorrect;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return OperationStatus.Exception;
            }
        }
    }
}
