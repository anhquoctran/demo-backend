﻿using HappyFarm.Core.Commons;
using HappyFarm.Infrastructure.Dto;
using System.Collections.Generic;
using System.Linq;

namespace HappyFarm.Infrastructure.Services.Implementations
{
    public class SignalrConnectionManager
    {

        private static SignalrConnectionManager _instance;
        private readonly HashSet<SignalrConnection> _connections;

        public delegate void OnConnectionAddedHandler(string connectionId);
        public delegate void OnConnectionRemovedHandler(string connectionId);
        public delegate void OnClearAllConnectionsHandler();

        public event OnConnectionAddedHandler OnConnectionAdded;
        public event OnConnectionRemovedHandler OnConnectionRemoved;
        public event OnClearAllConnectionsHandler OnRemovedAll;
        private readonly object _lockObject = new object();

        public static SignalrConnectionManager Instance
        {
            get
            {
                if (_instance == null) _instance = new SignalrConnectionManager();
                return _instance;
            }
        }

        private SignalrConnectionManager()
        {
            _connections = new HashSet<SignalrConnection>();
        }

        public bool AddConnection(SignalrConnection connection)
        {
            lock(_lockObject)
            {
                var added = _connections.Add(connection);

                if (added)
                {
                    OnConnectionAdded?.Invoke(connection.ConnectionId);
                }


                return added;
            }

        }

        public bool RemoveConnection(string connectionId)
        {
            lock(_lockObject)
            {
                var removed = _connections.RemoveWhere(x => x.ConnectionId == connectionId) > 0;

                if (removed)
                {
                    OnConnectionRemoved?.Invoke(connectionId);
                }

                return removed;
            }
        }

        public void RemoveAll()
        {
            lock(_lockObject)
            {
                _connections.Clear();
                OnRemovedAll?.Invoke();
            }
            
        }

        public bool HasConnection(string connectionId)
        {
            return _connections.Any(c => c.ConnectionId == connectionId);
        }

        public ICollection<object> GetListConnectionPublic()
        {
            return _connections.Select(x => new
            {
                uuid = x.ConnectionId,
                address = x.RemoteAddress,
                time = DateTimeUtils.ConvertFromUnixTime(x.ConnectedTime)
            }).ToArray();
        }

        public object GetConnectionById(string connectionId)
        {
            return _connections
                .Where(x => x.ConnectionId == connectionId)
                .Select(x => new
            {
                uuid = x.ConnectionId,
                address = x.RemoteAddress,
                time = DateTimeUtils.ConvertFromUnixTime(x.ConnectedTime)
                }).FirstOrDefault();
        }

        public int Total => _connections.Count;
    }
}
