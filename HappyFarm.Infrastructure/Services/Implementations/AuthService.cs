﻿using AutoMapper;
using HappyFarm.Core.Commons;
using HappyFarm.Core.Enums;
using HappyFarm.Core.Models;
using HappyFarm.Core.Repositories;
using HappyFarm.Core.UnitOfWork;
using HappyFarm.Infrastructure.Commons;
using HappyFarm.Infrastructure.Dto;
using HappyFarm.Infrastructure.Helpers;
using HappyFarm.Infrastructure.Permissions;
using HappyFarm.Infrastructure.Services.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace HappyFarm.Infrastructure.Services.Implementations
{
    public class AuthService : CrudService<User, UserDto>, IAuthService
    {
        protected override IRepository<User> Repository => _unitOfWork.UserRepository;

        public AuthService(
            ILogger<AuthService> logger,
            IUnitOfWork unitOfWork,
            IMapper mapper
            ) : base(mapper, unitOfWork, logger)
        {
        }

        public async Task<KeyValuePair<AuthResult, AuthenticatedUserDto>> Authenticate(string username, string password)
        {
            try
            {
                username = username.Trim().ToLower();
                var user = await FindAsync(x => x.Username.Trim().ToLower() == username || x.Email.Trim().ToLower() == username);
                if (user == null) return new KeyValuePair<AuthResult, AuthenticatedUserDto>(AuthResult.Failed, null);
                var isPasswordValid = HashingHelper.CheckPassword(password, user.Password);
                if (!isPasswordValid)
                    return new KeyValuePair<AuthResult, AuthenticatedUserDto>(AuthResult.Failed, null);
                var idPayload = new Dictionary<string, object>
                {
                    { "id", user.Id }
                };

                var authUser = new AuthenticatedUserDto
                {
                    Id = user.Id,
                    Role = new[] { user.Role },
                    Avatar = user.Avatar,
                    Token = JwtAuthFactory.GenerateJwtToken(idPayload),
                    Permissions = GetPermissions(user.Role)
                };

                return new KeyValuePair<AuthResult, AuthenticatedUserDto>(AuthResult.Success, authUser);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return new KeyValuePair<AuthResult, AuthenticatedUserDto>(AuthResult.ExceptionFailed, null);
            }
        }

        public bool Authorize(int id)
        {
            try
            {
                var user = FindById(id);
                return user != null && Privileges.CanLoginToWeb.Contains(user.Role);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return false;
            }
        }

        public async Task<UserDto> GetUserProfile(int id)
        {
            try
            {
                return await FindByIdAsync(id);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return null;
            }
        }

        public async Task<OperationStatus> UpdatePassword(int id, ChangePasswordDto dtoPassword)
        {
            try
            {
                var user = await FindByIdAsync(id);

                if (user == null) return OperationStatus.NotFound;
                if (!HashingHelper.CheckPassword(dtoPassword.OldPassword, user.Password))
                    return OperationStatus.WrongPassword;
                user.Password = dtoPassword.NewPassword;
                var result = await UpdateAsync(user);
                return result ? OperationStatus.Succeed : OperationStatus.Failed;

            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return OperationStatus.Exception;
            }
        }

        public async Task<OperationStatus> UpdateProfile(UserDto dto)
        {
            try
            {
                var user = await FindByIdAsync(dto.Id);
                user.Firstname = dto.Firstname;
                user.Lastname = dto.Lastname;
                user.Avatar = dto.Avatar;
                user.Email = dto.Email;

                var result = await UpdateAsync(user);
                return result ? OperationStatus.Succeed : OperationStatus.Failed;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return OperationStatus.Exception;
            }
        }

        public IEnumerable<string> GetPermissions(string role)
        {
            try
            {
                var perType = typeof(Privileges);
                var staticFields = perType.GetFields(BindingFlags.Public | BindingFlags.Static);
                var result = staticFields
                    .Select(x => new KeyValuePair<string, string[]>(x.Name.PascalCaseToSnakeCase(), (string[])x.GetValue(null)))
                    .Where(x => x.Value.Contains(role))
                    .Select(x => x.Key)
                    .OrderBy(x => x);

                return result;
            }
            catch (Exception e)
            {
                _logger.LogError(e.ToString());
                return null;
            }
        }

        public OperationStatus IsRoleCanAccess(int userId, params string[] roles)
        {
            try
            {
                var roleValid = roles.Length > 0 && User.Roles.ContainsAnotherArray(roles);
                if (!roleValid) return OperationStatus.AccessDenied;
                var user = FindById(userId);
                return user != null && roles.Contains(user.Role) ? OperationStatus.Succeed : OperationStatus.AccessDenied;

            }
            catch (Exception e)
            {
                _logger.LogError(e.ToString());
                return OperationStatus.Exception;
            }
        }

        public async Task<OperationStatus> Authorize(string jwtToken)
        {
            try
            {
                if (!StringUtils.CheckIsEmptyOrWhitespaceString(jwtToken)) return OperationStatus.NotAuthorized;
                var (success, data) = JwtAuthFactory.ValidateJwtToken(jwtToken);
                if (!success) return OperationStatus.NotAuthorized;
                var payload = data.Payload;
                if (!payload.TryGetValue("id", out var idRaw)) return OperationStatus.NotAuthorized;
                var idString = (string)idRaw;
                var idParsedOk = int.TryParse(idString, out var id);

                if (!idParsedOk) return OperationStatus.NotAuthorized;
                var canAccess = await AuthorizeAsync(id);
                return canAccess switch
                {
                    OperationStatus.Succeed => OperationStatus.Succeed,
                    OperationStatus.NotAuthorized => OperationStatus.NotAuthorized,
                    OperationStatus.Exception => OperationStatus.Exception,
                    _ => OperationStatus.Exception
                };

            }
            catch (Exception e)
            {
                _logger.LogError(e.ToString());
                return OperationStatus.Exception;
            }
        }

        private async Task<OperationStatus> AuthorizeAsync(int userId)
        {
            try
            {
                var user = await FindByIdAsync(userId);
                if (user != null)
                {
                    return Privileges.CanLoginToWeb.Contains(user.Role) ? OperationStatus.Succeed : OperationStatus.NotAuthorized;
                }
                return OperationStatus.NotAuthorized;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return OperationStatus.Exception;
            }
        }
    }
}
