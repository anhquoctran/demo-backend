﻿using HappyFarm.Core.Commons;
using HappyFarm.Core.Enums;
using HappyFarm.Core.Events;
using HappyFarm.Core.Models;
using HappyFarm.Infrastructure.Dto;
using HappyFarm.Infrastructure.Services.Interfaces;
using HappyFarm.Infrastructure.SignalR;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace HappyFarm.Infrastructure.Services.Implementations
{
    public class MqttBackgroundService : IHostedService
    {
        private IMessageQueueService _messageQueueService;
        private ISensorValueService _sensorValueService;
        private ISensorNodeService _sensorNodeService;
        private IHubContext<HappyFarmHub, IHappyFarmConnector> _happyFarmHub;
        private ILogger _logger;
        private readonly IServiceProvider _serviceProvider;

        public MqttBackgroundService(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;

            InitLogger();
            InitMqttService();
            InitHfHub();
            InitSensorValueService();
            InitSensorNodeService();
        }

        private void InitLogger()
        {
            _logger = _serviceProvider.GetRequiredService<ILogger<MqttBackgroundService>>();
        }

        private void InitMqttService()
        {
            _messageQueueService = _serviceProvider.GetRequiredService<IMessageQueueService>();
            _messageQueueService.OnMessageReceived += OnMessageQueuedReceived;
        }

        private void InitHfHub()
        {
            var hubContext = _serviceProvider.GetRequiredService<IHubContext<HappyFarmHub, IHappyFarmConnector>>();
            _happyFarmHub = hubContext;
        }

        private void InitSensorValueService()
        {
            _sensorValueService = _serviceProvider.GetRequiredService<ISensorValueService>();
        }

        private void InitSensorNodeService()
        {
            _sensorNodeService = _serviceProvider.GetRequiredService<ISensorNodeService>();
        }

        private async Task OnMessageQueuedReceived(MessageReceivedEventArgs<string> eventArgs)
        {
            var receivedTopic = Regex.Replace(eventArgs.ReceivedTopic, @"\d+", "+");
            if (receivedTopic == Constants.STATUS_TOPIC)
            {
                await HandleStatusEvent(eventArgs);
            }

        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            if (_messageQueueService.IsRunning)
            {
                await _messageQueueService.SubscribeTo(Constants.STATUS_TOPIC);
            }
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            if (_messageQueueService.IsRunning)
            {
                await _messageQueueService.Disconnect();
            }
        }

        private async Task HandleStatusEvent(MessageReceivedEventArgs<string> eventArgs)
        {
            if (eventArgs.Data != null)
            {
                if (!StringUtils.IsValidJson(eventArgs.Data)) return;

                _logger.LogInformation("Received command from gateway: {0}", eventArgs.Data);

                if (RegexUtils.TryParseDigitFromString(eventArgs.ReceivedTopic, out var parsedGatewayId))
                {
                    try
                    {
                        var dataResult = JsonConvert.DeserializeObject<HFCommand<object>>(eventArgs.Data);
                        var gatewayId = parsedGatewayId.Value;
                        if (dataResult != null)
                        {
                            var cmd = ParseCommand(dataResult.Command);
                            if (dataResult.Data != null)
                            {
                                JContainer dataJson;
                                var farmConnector = GetFarmConnector(dataResult.ConnectionId ?? null);

                                switch (cmd)
                                {
                                    case Constants.DeviceCommand.CommandStatus:
                                        {
                                            dataJson = dataResult.Data as JObject;
                                            var statusData = dataJson.ToObject<GatewayStatus>();
                                            await farmConnector.ReceivedGatewayStatus(new GatewayStatus
                                            {
                                                GatewayId = parsedGatewayId.Value,
                                                Status = statusData.Status
                                            });
                                            break;
                                        }
                                    case Constants.DeviceCommand.CommandGateway:
                                        {
                                            dataJson = dataResult.Data as JObject;
                                            await _messageQueueService.Publish(eventArgs.ReceivedTopic);
                                            var gatewayControl = dataJson.ToObject<GatewayControl>();
                                            gatewayControl.GatewayId = parsedGatewayId.Value;
                                            await farmConnector.ReceivedSensorCmd(gatewayControl);
                                            break;
                                        }
                                    case Constants.DeviceCommand.CommandSensorValues:
                                        {
                                            dataJson = dataResult.Data as JObject;
                                            var sensorValueData = dataJson.ToObject<SensorValueCommandData>();

                                            if (sensorValueData != null && sensorValueData.NodesInfo != null && sensorValueData.NodesInfo.Count > 0)
                                            {
                                                var count = await _sensorNodeService.CountNodeByGateway(gatewayId) > 0;
                                                var validNodeOwned = await _sensorNodeService.ValidateNodeOwnedByGateway(
                                                    gatewayId,
                                                    sensorValueData.NodesInfo.Select(x => x.NodeCode)
                                                );

                                                var hasValidValidate = sensorValueData.NodesInfo != null && sensorValueData.NodesInfo.Count > 0
                                                && sensorValueData.NodesInfo.Count == sensorValueData.NumberOfNode && count && validNodeOwned;

                                                if (hasValidValidate)
                                                {
                                                    var values = sensorValueData.NodesInfo.Select(async x => new SensorValueDto
                                                    {
                                                        NodeId = await _sensorNodeService.GetIdByCode(x.NodeCode),
                                                        AirHumidity = x.AirHumidity,
                                                        CreatedAt = x.CreatedAt - Constants.UTC_HCM_OFFSET_SECONDS,
                                                        LightIntensity = x.LightIntensity,
                                                        PhScale = x.PhScale,
                                                        RainFall = x.Rainfall,
                                                        SoilHumidity = x.SoilHumidity,
                                                        Temperature = x.Temp,
                                                        Tds = x.Tds,
                                                        WindSpeed = x.WindSpeed
                                                    }).Select(x => x.Result);

                                                    var result = await _sensorValueService.AddValueRange(values);
                                                    if (result.Key == OperationStatus.Succeed)
                                                    {
                                                        var items = values.Select(x => new SensorValueDataItem
                                                        {
                                                            NodeId = x.NodeId.GetValueOrDefault(),
                                                            AirHumidity = x.AirHumidity,
                                                            CreatedAt = x.CreatedAt,
                                                            LightIntensity = x.LightIntensity,
                                                            PhScale = x.PhScale,
                                                            RainFall = x.RainFall,
                                                            SoilHumidity = x.SoilHumidity,
                                                            Tds = x.Tds,
                                                            Temperature = x.Temperature,
                                                            WindSpeed = x.WindSpeed
                                                        });
                                                        var packet = new SensorValuesCmd
                                                        {
                                                            GatewayId = gatewayId,
                                                            Items = items
                                                        };
                                                        await _messageQueueService.Publish(eventArgs.ReceivedTopic);
                                                        await _happyFarmHub.Clients.All.ReceivedSensorValueData(packet);
                                                        _logger.LogInformation($"Received data from gateway ${gatewayId}");
                                                    }
                                                    else
                                                    {
                                                        await _messageQueueService.Publish(eventArgs.ReceivedTopic);
                                                    }
                                                }
                                                else
                                                {
                                                    await _messageQueueService.Publish(eventArgs.ReceivedTopic);
                                                }
                                            }
                                            else
                                            {
                                                await _messageQueueService.Publish(eventArgs.ReceivedTopic);
                                            }

                                            break;
                                        }
                                    case Constants.DeviceCommand.CommandPowerSetting:
                                        {
                                            dataJson = dataResult.Data as JObject;
                                            var powerCmdData = dataJson.ToObject<PowerControl>();
                                            powerCmdData.GatewayId = gatewayId;
                                            await _messageQueueService.Publish(eventArgs.ReceivedTopic);
                                            await farmConnector.ReceivedGatewayPowerControl(powerCmdData);
                                            break;
                                        }
                                    case Constants.DeviceCommand.CommandApplyControl:
                                        {
                                            dataJson = dataResult.Data as JObject;
                                            await _messageQueueService.Publish(eventArgs.ReceivedTopic);
                                            var appyControlCmd = dataJson.ToObject<ApplyControl>();
                                            appyControlCmd.GatewayId = gatewayId;
                                            await farmConnector.ReceivedGatewayApplyControl(appyControlCmd);
                                            break;
                                        }
                                    case Constants.DeviceCommand.CommandTimerModeSetting:
                                        {
                                            dataJson = dataResult.Data as JObject;
                                            await _messageQueueService.Publish(eventArgs.ReceivedTopic);
                                            var timerMode = dataJson.ToObject<TimerMode>();
                                            timerMode.GatewayId = gatewayId;
                                            await farmConnector.ReceivedTimerMode(timerMode);
                                            break;
                                        }
                                    case Constants.DeviceCommand.CommandControlTimer:
                                        {
                                            dataJson = dataResult.Data as JObject;
                                            await _messageQueueService.Publish(eventArgs.ReceivedTopic);
                                            var controlTimer = dataJson.ToObject<SettingTimer>();
                                            controlTimer.GatewayId = gatewayId;
                                            await farmConnector.ReceivedTimerSetting(controlTimer);
                                            break;
                                        }
                                    case Constants.DeviceCommand.CommandTimeSync:
                                        {
                                            dataJson = dataResult.Data as JObject;
                                            var timeSync = new HFCommand<TimeSync>
                                            {
                                                Command = (int) Constants.DeviceCommand.CommandTimeSync,
                                                Data = new TimeSync
                                                {
                                                    ServerTime = DateTimeUtils.NowUnix()
                                                }
                                            };
                                            var packetData = new PublishMessageData<object>
                                            {
                                                Data = timeSync,
                                                Topic = eventArgs.ReceivedTopic,
                                                Retained = true
                                            };
                                            await _messageQueueService.Publish(packetData);
                                            break;
                                        }
                                    case Constants.DeviceCommand.CommandResponse:
                                        dataJson = dataResult.Data as JObject;
                                        await _messageQueueService.Publish(eventArgs.ReceivedTopic);
                                        await farmConnector.ReceivedResponseCmdState(new CmdStateResponse
                                        {
                                            Errors = null,
                                            GatewayId = gatewayId,
                                            Message = ReasonPhrases.GetReasonPhrase(200),
                                            State = true
                                        });
                                        break;
                                    case Constants.DeviceCommand.CommandInformation:
                                        {
                                            dataJson = dataResult.Data as JObject;
                                            await _messageQueueService.Publish(eventArgs.ReceivedTopic);
                                            var infoData = dataJson.ToObject<InformationData>();
                                            await farmConnector.ReceivedDeviceInformation(infoData);
                                            break;
                                        }
                                    case Constants.DeviceCommand.CommandGatewaySensor:
                                        {
                                            var dataArray = dataResult.Data as JArray;
                                            await _messageQueueService.Publish(eventArgs.ReceivedTopic);
                                            var gatewaySensorData = dataArray.ToObject<GatewaySensor[]>();
                                            var payload = new GatewaySensorBundle
                                            {
                                                GatewayId = gatewayId,
                                                Values = (GatewaySensorCollection)gatewaySensorData
                                            };
                                            await farmConnector.ReceivedGatewaySensors(payload);
                                            break;
                                        }
                                    
                                }
                            }
                            else
                            {
                                switch (cmd)
                                {
                                    case Constants.DeviceCommand.CommandResponse:
                                        {
                                            await GetFarmConnector(dataResult.ConnectionId)
                                            .ReceivedResponseCmdState(new CmdStateResponse
                                            {
                                                Errors = null,
                                                GatewayId = gatewayId,
                                                Message = ReasonPhrases.GetReasonPhrase(200),
                                                State = true
                                            });

                                            await _messageQueueService.Publish(eventArgs.ReceivedTopic);
                                            break;
                                        }
                                }
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        _logger.LogError($"MQTT Parse/Receive/Send error: {0}", ex.Message);
                    }

                }
            }
        }

        /// <summary>
        /// Parsing command flag from MQTT data package
        /// </summary>
        /// <param name="cmdRawValue">Command raw value</param>
        /// <returns>Device command</returns>
        private Constants.DeviceCommand ParseCommand(int cmdRawValue)
        {
            if (Enum.IsDefined(typeof(Constants.DeviceCommand), cmdRawValue))
            {
                return (Constants.DeviceCommand)cmdRawValue;
            }

            return Constants.DeviceCommand.CommandStatus;
        }

        /// <summary>
        /// Get farm connector by connection ID or send for all connected connection
        /// </summary>
        /// <param name="connectionId">Connection ID</param>
        /// <returns></returns>
        private IHappyFarmConnector GetFarmConnector(string connectionId)
        {


            if (!StringUtils.CheckIsEmptyOrWhitespaceString(connectionId))
            {
                if (SignalrConnectionManager.Instance.HasConnection(connectionId))
                {
                    _logger.LogInformation("Response for connection ID: {0}", connectionId);
                    return _happyFarmHub.Clients.Clients(connectionId);
                }

                _logger.LogInformation("Response for all connections");
                return _happyFarmHub.Clients.All;
            }
            else
            {
                _logger.LogInformation("Response for all connections");
                return _happyFarmHub.Clients.All;
            }
        }

        //private async Task HandleCommandEvent(MessageReceivedEventArgs<string> eventArgs)
        //{
        //    if (eventArgs.Data != null)
        //    {
        //        try
        //        {
        //            if (!StringUtils.IsValidJson(eventArgs.Data)) return;
        //            _logger.LogInformation("Received command from gateway: {0}", eventArgs.Data);

        //            if (RegexUtils.TryParseDigitFromString(eventArgs.ReceivedTopic, out var parsedGatewayId))
        //            {
        //                var dataResult = JsonConvert.DeserializeObject<HFCommand<object>>(eventArgs.Data);
        //                var gatewayId = parsedGatewayId.Value;
        //                var dataJson = (JObject)dataResult.Data;


        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            _logger.LogError(e.ToString());
        //        }
        //    }
        //}
    }
}
