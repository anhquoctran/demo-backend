﻿using HappyFarm.Core.Commons;
using HappyFarm.Infrastructure.Services.Interfaces;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace HappyFarm.Infrastructure.Services.Implementations
{
    public class DistributedService : IDistributedService
    {
        public const int JWT_BLACKLIST_DB_INDEX = 3;
        public const int DEVICE_ACL_DB_INDEX = 2;
        public const int GATEWAY_SENSOR_VALUE = 4;

        private static string _redisHost;
        private static ushort _redisPort;
        private static string _redisPassword;
        private static string _connectionString;

        private static ConnectionMultiplexer _redis;
        private readonly ILogger _logger;
        private static ConfigurationOptions _configOptions;

        public static ConnectionMultiplexer RedisConnection => LazyConnection.Value;

        public DistributedService(ILogger<IDistributedService> logger)
        {
            _logger = logger;
            Connect();
        }

        public static Lazy<ConfigurationOptions> LazyRedisConfig = new Lazy<ConfigurationOptions>(() =>
        {
            var conn = BuildConnectionString();
            _configOptions = ConfigurationOptions.Parse(conn);
            _configOptions.SyncTimeout = 10000;
            _configOptions.ConnectTimeout = 20000;
            _configOptions.AbortOnConnectFail = false;
            return _configOptions;
        });

        public static Lazy<ConnectionMultiplexer> LazyConnection = new Lazy<ConnectionMultiplexer>(() =>
        {
            try
            {
                _redis = ConnectionMultiplexer.Connect(LazyRedisConfig.Value);
                _redis.ConnectionFailed += OnRedisConnectionFailed;
                _redis.InternalError += OnRedisInternalError;
                _redis.ErrorMessage += OnRedisErrorMessage;
                _redis.ConnectionRestored += OnRedisConnectionRestored;
                Console.WriteLine($"Connected to Redis Server: {_redis.Configuration}");
                return _redis;
            }
            catch (Exception e)
            {
                throw e;
            }
        });

        private void Connect()
        {
            var redisConnectionString = AppConfig.Config.Redis.ConnectionString;
            if (!StringUtils.CheckIsEmptyOrWhitespaceString(redisConnectionString))
            {
                _connectionString = redisConnectionString;
                _ = LazyConnection.Value;
            }
            else
            {
                var redisPort = AppConfig.Config.Redis.Port;
                var host = AppConfig.Config.Redis.Host;
                if (!redisPort.HasValue || redisPort.Value <= 0)
                {
                    redisPort = Constants.REDIS_PORT;
                }

                _redisPort = redisPort.Value;

                _redisHost = !string.IsNullOrEmpty(host) ? host : Constants.REDIS_HOST;

                _redisPassword = AppConfig.Config.Redis.Password;

                _ = LazyConnection.Value;
            }
        }

        public IEnumerable<string> this[int dbIndex, string pattern = "*"]
        {
            get
            {
                pattern = string.IsNullOrEmpty(pattern) ? "*" : pattern;
                return GetAllKeys(dbIndex, pattern);
            }
        }

        public bool BatchDelete(string[] keys, int databaseIndex)
        {
            try
            {
                var batch = RedisConnection
                    .GetDatabase(databaseIndex)
                    .CreateBatch();

                batch.Execute();
                Task.WhenAll(keys.Select(key => batch.KeyDeleteAsync(key)).ToArray());

                return true;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return false;
            }
        }

        public async Task<bool> BatchSetStringAsync(Dictionary<string, string> data, int databaseIndex)
        {
            try
            {
                var batch = RedisConnection
                  .GetDatabase(databaseIndex)
                  .CreateBatch();

                batch.Execute();
                var done = await Task.WhenAll(data.Select(pair => batch.StringSetAsync(pair.Key, pair.Value)).ToArray());

                return done.All(x => x);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return false;
            }
        }

        public async Task<bool> SetToSetsAsync(string setName, string setValue, int dbIndex)
        {
            if (!RedisConnection.IsConnected) return false;
            var db = RedisConnection.GetDatabase(dbIndex);
            return await db.SetAddAsync(setName, setValue);
        }

        public IAsyncEnumerable<string> GetSetsDataAsync(string setName, int dbIndex)
        {
            if (!RedisConnection.IsConnected) return null;
            var db = RedisConnection.GetDatabase(dbIndex);
            var result = db.SetScanAsync(setName, "*", 10000);
            return result.Select(x => x.ToString());
        }

        public IEnumerable<string> GetSetsData(string setName, int dbIndex)
        {
            if (!RedisConnection.IsConnected) return null;
            var db = RedisConnection.GetDatabase(dbIndex);
            var result = db.SetScan(setName, "*", 10000);
            return result.Select(x => x.ToString());
        }

        public async Task<bool> RemoveSetElementAsync(string setName, string value, int dbIndex)
        {
            if (!RedisConnection.IsConnected) return false;
            var db = RedisConnection.GetDatabase(dbIndex);
            return await db.SetRemoveAsync(setName, value);
        }

        public bool RemoveSetElement(string setName, string value, int dbIndex)
        {
            if (!RedisConnection.IsConnected) return false;
            var db = RedisConnection.GetDatabase(dbIndex);
            return db.SetRemove(setName, value);
        }

        public bool SetToSets(string setName, string setValue, int dbIndex)
        {
            if (!RedisConnection.IsConnected) return false;
            var db = RedisConnection.GetDatabase(dbIndex);
            return db.SetAdd(setName, setValue);
        }

        public async Task<bool> DeleteAsync(string key, int databaseIndex, CancellationToken token = default)
        {
            if (!RedisConnection.IsConnected) return false;
            var db = RedisConnection.GetDatabase(databaseIndex);
            return await db.KeyDeleteAsync(key);
        }

        public T GetObject<T>(string key, int databaseIndex)
        {
            var value = GetString(key, databaseIndex);
            return StringUtils.IsValidJson(value) ? JsonConvert.DeserializeObject<T>(value) : default;
        }

        public async Task<T> GetObjectAsync<T>(string key, int databaseIndex, CancellationToken token = default)
        {
            var value = await GetStringAsync(key, databaseIndex, token);
            return StringUtils.IsValidJson(value) ? JsonConvert.DeserializeObject<T>(value) : default;
        }

        public string GetString(string key, int databaseIndex)
        {
            if (!RedisConnection.IsConnected) return null;
            var db = RedisConnection.GetDatabase(databaseIndex);
            return db.StringGet(key);
        }

        public async Task<string> GetStringAsync(string key, int databaseIndex, CancellationToken token = default)
        {
            if (!RedisConnection.IsConnected) return null;
            var db = RedisConnection.GetDatabase(databaseIndex);
            return await db.StringGetAsync(key);
        }

        public bool SetString(string key, string value, int databaseIndex)
        {
            if (!RedisConnection.IsConnected) return false;
            var db = RedisConnection.GetDatabase(databaseIndex);
            return db.StringSet(key, value);
        }

        public async Task<bool> SetStringAsync(string key, string value, int databaseIndex, CancellationToken token = default)
        {
            if (!RedisConnection.IsConnected) return false;
            var db = RedisConnection.GetDatabase(databaseIndex);
            return await db.StringSetAsync(key, value);
        }

        private static IEnumerable<string> GetAllKeys(int dbIndex, string pattern)
        {
            if (!RedisConnection.IsConnected) return Enumerable.Empty<string>();

            pattern = StringUtils.CheckIsEmptyOrWhitespaceString(pattern) ? "*" : pattern.Trim();

            if (!_configOptions.EndPoints.Any()) return Enumerable.Empty<string>();
            var endpoint = _configOptions.EndPoints.FirstOrDefault();
            var server = RedisConnection.GetServer(endpoint);
            return server.Keys(dbIndex, pattern, 10000, CommandFlags.None)
                .Select(key => key.ToString());
        }

        private static void OnRedisConnectionRestored(object sender, ConnectionFailedEventArgs e)
        {
            Console.WriteLine("Redis Server connected");
        }

        private static void OnRedisErrorMessage(object sender, RedisErrorEventArgs e)
        {
            Console.WriteLine("Redis Error: {0}", e.Message);
        }

        private static void OnRedisInternalError(object sender, InternalErrorEventArgs e)
        {
            Console.WriteLine("Redis Error: {0}", e.Origin);
        }

        private static void OnRedisConnectionFailed(object sender, ConnectionFailedEventArgs e)
        {
            Console.WriteLine("Redis Error: Failed to connect to Redis Server");
        }

        private static string BuildConnectionString()
        {
            if (!StringUtils.CheckIsEmptyOrWhitespaceString(_connectionString)) return _connectionString;
            var configString = $"{_redisHost}:{_redisPort},connectRetry=5,";
            if (!StringUtils.CheckIsEmptyOrWhitespaceString(_redisPassword))
            {
                configString += "password=" + _redisPassword;
            }

            return configString;

        }

        
    }
}
