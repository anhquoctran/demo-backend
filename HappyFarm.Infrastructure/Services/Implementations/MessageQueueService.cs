﻿using HappyFarm.Core.Commons;
using HappyFarm.Core.Enums;
using HappyFarm.Core.Events;
using HappyFarm.Infrastructure.Dto;
using HappyFarm.Infrastructure.Services.Interfaces;
using Microsoft.Extensions.Logging;
using MQTTnet;
using MQTTnet.Client;
using MQTTnet.Client.Connecting;
using MQTTnet.Client.Options;
using MQTTnet.Client.Publishing;
using Newtonsoft.Json;
using System;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;

namespace HappyFarm.Infrastructure.Services.Implementations
{
    public class MessageQueueService : IMessageQueueService
    {
        private IMqttClient _mqttClient;
        private IMqttClientOptions _mqttOptions;
        private readonly IMqttFactory _mqttFactory;

        private readonly ILogger _logger;
        private readonly string _username;
        private readonly string _password;

        public bool IsRunning { get; private set; }

        public event IMessageQueueService.OnMessageReceivedEventHandler<string> OnMessageReceived;

        public MessageQueueService(ILogger<MessageQueueService> logger)
        {
            _logger = logger;

            _mqttFactory = new MqttFactory();

            _username = AppConfig.Config.Mqtt.DefaultSystemAccount.Username ?? Constants.DEFAULT_SYSTEM_REDIS_ACCOUNT;
            _password = AppConfig.Config.Mqtt.DefaultSystemAccount.Password ?? Constants.DEFAULT_SYSTEM_REDIS_PASSWORD;

            Connect(_username, _password);

        }

        private async void Connect(string clientId, string password)
        {
            await Connect(clientId, clientId, password);
        }

        private async Task<bool> Connect(string clientId, string username, string password)
        {
            try
            {
                if (IsRunning)
                {
                    await Disconnect(); //disconnect before create new connection
                }
                var host = AppConfig.Config.Mqtt.Host;
                var port = AppConfig.Config.Mqtt.Port;

                if (!port.HasValue || port <= 0)
                {
                    port = Constants.VERNEMQ_PORT;
                }

                var options = new MqttClientOptionsBuilder()
                    .WithClientId(clientId)
                    .WithTcpServer(host, port)
                    .WithCredentials(username, password)
                    .WithCommunicationTimeout(TimeSpan.FromMinutes(10))
                    .WithKeepAlivePeriod(TimeSpan.FromMinutes(10));

                var useTls = AppConfig.Config.Mqtt.UseMqttTls.GetValueOrDefault();
                _logger.LogInformation($"Connecting to MQTT Server {host}, port: {port}, {clientId}, {password.HidePassword()}, use TLS/SSL: {useTls}...");
                if (useTls)
                {
                    _logger.LogInformation("Use TLS");
                    options = options.WithTls(config =>
                    {
                        config.IgnoreCertificateRevocationErrors = true;
                        config.IgnoreCertificateChainErrors = true;
                        config.SslProtocol = SslProtocols.Tls12;
                        config.AllowUntrustedCertificates = true;
                        config.UseTls = true;
                    });
                }

                _mqttOptions = options.Build();
                
                _mqttClient = _mqttFactory.CreateMqttClient();

                _mqttClient.UseApplicationMessageReceivedHandler(handler =>
                {
                    var topic = handler.ApplicationMessage.Topic;
                    var payload = handler.ApplicationMessage.Payload;

                    if (payload == null || payload.Length <= 0) return;
                    var eventArgs = new MessageReceivedEventArgs<string>(handler.ApplicationMessage.ConvertPayloadToString(), topic);
                    OnMessageReceived?.Invoke(eventArgs);
                });

                _mqttClient.UseDisconnectedHandler(handler =>
                {
                    _logger.LogInformation("Connection was disconnected with code {0}", (int)handler.ReasonCode);
                });

                var connected = await _mqttClient.ConnectAsync(_mqttOptions);
                IsRunning = connected.ResultCode == MqttClientConnectResultCode.Success && _mqttClient.IsConnected;
                _logger.LogInformation("Connection has been established OK.");
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                IsRunning = false;
                return false;
            }
        }

        public async Task<bool> Disconnect()
        {
            try
            {
                if (!IsRunning) return true;
                _logger.LogInformation("Trying to disconnect MQTT client...");
                await _mqttClient.DisconnectAsync();
                _logger.LogInformation($"MQTT client {_mqttClient.Options.ClientId} disconnected OK");
                return true;
            }
            catch (Exception e)
            {
                _logger.LogError("ERROR: Unable to disconnect MQTT client");
                _logger.LogError(e.ToString());
                return false;
            }
        }

        public async Task<bool> Publish<T>(PublishMessageData<T> packet)
        {
            if (packet == null) return false;

            if (!_mqttClient.IsConnected || !IsRunning)
            {
                _logger.LogInformation("Client is not running. Try to start client and connecting...");
                Connect(_username, _password);
            }

            if (!StringUtils.CheckIsEmptyOrWhitespaceString(packet.Topic))
            {
                try 
                {
                    _logger.LogInformation("MQTT user: {0}", _mqttClient.Options?.Credentials?.Username);

                    var data = packet.Data != null ? JsonConvert.SerializeObject(packet.Data) : null;
                    if (data != null)
                    {

                        var dataBinary = Encoding.UTF8.GetBytes(data);
                        var messageBuilder = new MqttApplicationMessageBuilder();

                        switch (packet.QualityOfService)
                        {
                            case QualityOfService.AtmostOnce:
                                messageBuilder = messageBuilder.WithAtMostOnceQoS();
                                break;
                            case QualityOfService.AtleastOnce:
                                messageBuilder = messageBuilder.WithAtLeastOnceQoS();
                                break;
                            case QualityOfService.ExactlyOnce:
                                messageBuilder = messageBuilder.WithExactlyOnceQoS();
                                break;
                        }

                        messageBuilder = messageBuilder.WithTopic(packet.Topic);
                        messageBuilder = messageBuilder.WithPayload(dataBinary);

                        if (packet.Retained)
                        {
                            messageBuilder = messageBuilder.WithRetainFlag(true);
                        }

                        var message = messageBuilder.Build();

                        _logger.LogInformation($"Preparing publish data to topic {packet.Topic}, data length = {message.Payload.Length}, data = {data}, qos = {(int)message.QualityOfServiceLevel}, retain = {message.Retain}");

                        var resultPub = await _mqttClient.PublishAsync(message);
                        if (resultPub.ReasonCode == MqttClientPublishReasonCode.Success)
                        {
                            _logger.LogInformation("Published message: {0} OK", data);
                            return true;
                        }
                        _logger.LogError("ERROR: Failed to publish topic {0}", packet.Topic);
                        return false;
                    }
                    else
                    {
                        _logger.LogInformation("Prepare publishing empty data to topic {0}", packet.Topic);

                        var message = new MqttApplicationMessageBuilder()
                            .WithExactlyOnceQoS()
                            .WithTopic(packet.Topic)
                            .WithRetainFlag()
                            .Build();

                        _logger.LogInformation($"Prepare publishing to topic {packet.Topic}, empty data, qos = {(int)message.QualityOfServiceLevel}, retain = {message.Retain}");

                        var resultPub = await _mqttClient.PublishAsync(message);
                        if (resultPub.ReasonCode == MqttClientPublishReasonCode.Success)
                        {
                            _logger.LogInformation("Published data to topic {0} OK", packet.Topic);
                            return true;
                        }
                        _logger.LogError("ERROR: Failed to publish topic {0}", packet.Topic);
                        return false;
                    }
                } 
                catch (Exception e)
                {
                    _logger.LogError(e.Message);
                    _logger.LogError("ERROR: Failed to publish topic {0}, client is not running", packet.Topic);
                    return false;
                }
            }
            
            return false;
        }

        public async Task<bool> SubscribeTo(string topicLike)
        {
            try
            {
                if (!IsRunning || StringUtils.CheckIsEmptyOrWhitespaceString(topicLike)) return false;
                _logger.LogInformation("Trying to subscribe to topic {0}.", topicLike);
                await _mqttClient.SubscribeAsync(topicLike);
                _logger.LogInformation("Subscribed to topic {0} OK.", topicLike);
                return true;
            }
            catch (Exception e)
            {
                _logger.LogError("ERROR: Failed to subscribing to topic {0}.", topicLike);
                _logger.LogError(e.ToString());
                return false;
            }
        }

        public async Task<bool> Publish(string topic)
        {
            if (!(_mqttClient.IsConnected || IsRunning))
            {
                Connect(_username, _password);
            }
            if (!StringUtils.CheckIsEmptyOrWhitespaceString(topic))
            {
                try
                {
                    _logger.LogInformation("MQTT user: {0}", _mqttClient.Options.Credentials?.Username);

                    var message = new MqttApplicationMessageBuilder()
                        .WithExactlyOnceQoS()
                        .WithTopic(topic)
                        .WithPayload((byte[])null)
                        .WithRetainFlag()
                        .Build();
                    _logger.LogInformation($"Prepare publishing to topic {topic}, empty payload, qos = {(int)message.QualityOfServiceLevel}, retain = {message.Retain}");

                    var resultPub = await _mqttClient.PublishAsync(message);
                    if (resultPub.ReasonCode == MqttClientPublishReasonCode.Success)
                    {
                        _logger.LogInformation("Published empty message OK");
                        return true;
                    }
                    _logger.LogError("ERROR: Failed to publish topic {0}", topic);
                    return false;
                }
                catch (Exception e)
                {
                    _logger.LogError(e.Message);
                    _logger.LogError("ERROR: Failed to publish topic {0}, client is not running", topic);
                    return false;
                }
            }
            
            return false;
        }
    }
}
