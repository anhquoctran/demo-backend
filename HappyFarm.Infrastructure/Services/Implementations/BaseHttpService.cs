/*
 * Copyright [2020] [anhquoctran]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

using HappyFarm.Infrastructure.Services.Interfaces;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace HappyFarm.Infrastructure.Services.Implementations
{
    public abstract class BaseHttpService : IHttpService
    {
        private readonly HttpClient _client;
        private readonly ILogger _logger;
        protected readonly string BaseURL;

        public BaseHttpService(string baseUrl, string token, ILogger logger)
        {
            BaseURL = baseUrl;
            _client = new HttpClient();
            _client.BaseAddress = new Uri(baseUrl);
            _client.DefaultRequestHeaders.TryAddWithoutValidation("content-type", "application/json");

            var byteArray = Encoding.ASCII.GetBytes($"{token}:");
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
            _logger = logger;
        }

        public async Task<T> GetAsync<T>(
          string path,
          Dictionary<string, string> queryParameters = null,
          Dictionary<string, string> headers = null
        ) where T : class, new()
        {
            try
            {
                var httpRequest = new HttpRequestMessage(HttpMethod.Get, path);
                var uriBuilder = new UriBuilder(_client.BaseAddress);
                uriBuilder.Path = path;

                var httpQueryParameters = HttpUtility.ParseQueryString(string.Empty);
                if (queryParameters != null && queryParameters.Count > 0)
                {
                    foreach(var q in queryParameters)
                    {
                        httpQueryParameters[q.Key] = q.Value ?? string.Empty;
                    }

                    uriBuilder.Query = httpQueryParameters.ToString();
                }

                httpRequest.RequestUri = uriBuilder.Uri;

                if (headers != null && headers.Count > 0)
                {
                    foreach (var h in headers)
                    {
                        httpRequest.Headers.TryAddWithoutValidation(h.Key, h.Value ?? string.Empty);
                    }
                }

                var res = await _client.SendAsync(httpRequest);

                if (res.IsSuccessStatusCode)
                {
                    var bodyResponse = await res.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<T>(bodyResponse ?? "{}");
                }

                return default(T);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return default(T);
            }
        }

        public async Task<bool> PostAsync<T>(
          string path,
          T body,
          Dictionary<string, string> queryParameters = null,
          Dictionary<string, string> headers = null
        ) where T : class, new()
        {
            try
            {
                var httpRequest = new HttpRequestMessage(HttpMethod.Get, path);

                var httpQueryParameters = HttpUtility.ParseQueryString(string.Empty);
                if (queryParameters != null && queryParameters.Count > 0)
                {
                    foreach (var q in queryParameters)
                    {
                        httpQueryParameters[q.Key] = q.Value ?? string.Empty;
                    }
                }

                var uriBuilder = new UriBuilder(httpRequest.RequestUri);
                uriBuilder.Query = httpQueryParameters.ToString();

                httpRequest.RequestUri = uriBuilder.Uri;

                if (headers != null && headers.Count > 0)
                {
                    foreach (var h in headers)
                    {
                        httpRequest.Headers.TryAddWithoutValidation(h.Key, h.Value ?? string.Empty);
                    }
                }

                var res = await _client.SendAsync(httpRequest);

                return res.IsSuccessStatusCode;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return false;
            }
        }
    }
}