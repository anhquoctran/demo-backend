﻿using AutoMapper;
using HappyFarm.Core.Interfaces;
using HappyFarm.Core.Repositories;
using HappyFarm.Core.UnitOfWork;
using HappyFarm.Infrastructure.Services.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace HappyFarm.Infrastructure.Services.Implementations
{
    public abstract class CrudService<TEntity, TDto>: ICrudService<TEntity, TDto>
        where TEntity : class, IEntity
        where TDto : new()
    {
        private readonly IMapper _mapper;

        protected readonly IUnitOfWork _unitOfWork;

        protected readonly ILogger _logger;

        protected abstract IRepository<TEntity> Repository { get; }

        protected const string DEVICE_PREFIX = "GW";

        protected const string NODE_PREFIX = "SN";

        public CrudService(IMapper mapper, IUnitOfWork unitOfWork, ILogger logger)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        public virtual TDto Create(TDto dto)
        {
            var entity = Repository.Add(DtoToEntity(dto));
            _unitOfWork.SaveChanges();
            return EntityToDto(entity);
        }

        public virtual void CreateRange(IEnumerable<TDto> dtos)
        {
            Repository.AddRange(DtoToEntity(dtos));
            _unitOfWork.SaveChanges();
        }

        public virtual async Task<TDto> CreateAsync(TDto dto, CancellationToken token = default)
        {
            var entity = Repository.Add(DtoToEntity(dto));
            await _unitOfWork.SaveChangeAsync(token);
            return EntityToDto(entity);
        }

        public virtual async Task CreateRangeAsync(IEnumerable<TDto> dtos, CancellationToken token = default)
        {
            Repository.AddRange(DtoToEntity(dtos));
            await _unitOfWork.SaveChangeAsync(token);
        }

        public virtual Task DeleteAsync(int id, CancellationToken token = default)
        {
            return Repository.DeleteAsync(id, token);
        }


        public virtual Task<int> DeleteAsync(IEnumerable<int> listId, CancellationToken token = default)
        {
            return Repository.DeleteAsync(listId, token);
        }

        public virtual int DeleteBy(Expression<Func<TEntity, bool>> predicate)
        {
            return Repository.Delete(predicate);
        }

        public virtual Task<int> DeleteByAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken token = default)
        {
            return Repository.DeleteAsync(predicate, token);
        }

        public virtual bool ExistsBy(Expression<Func<TEntity, bool>> predicate)
        {
            return Repository.ExistsBy(predicate);
        }

        public virtual Task<bool> ExistsByAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken token = default)
        {
            return Repository.ExistsByAsync(predicate, token);
        }

        public virtual bool ExistsById(int id)
        {
            return Repository.ExistsById(id);
        }

        public virtual Task<bool> ExistsByIdAsync(int id)
        {
            return Repository.ExistsByIdAsync(id);
        }

        public virtual TDto Find(Expression<Func<TEntity, bool>> predicate)
        {
            return EntityToDto(Repository.Find(predicate));
        }

        public virtual async Task<TDto> FindAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken token = default)
        {
            return EntityToDto(await Repository.FindAsync(predicate, token));
        }

        public virtual TDto FindById(int id)
        {
            return EntityToDto(Repository.FindById(id));
        }

        public virtual async Task<TDto> FindByIdAsync(int id, CancellationToken token = default)
        {
            var entity = await Repository.FindByIdAsync(id, token);
            return EntityToDto(entity);
        }

        public virtual bool Update(TDto dto)
        {
            var entity = DtoToEntity(dto);
            Repository.Update(entity);
            return _unitOfWork.SaveChanges() >= 0;
        }

        public virtual async Task<bool> UpdateAsync(TDto dto, CancellationToken token = default)
        {
            var entity = DtoToEntity(dto);
            Repository.Update(entity);
            return await _unitOfWork.SaveChangeAsync(token) >= 0;
        }

        public virtual async Task<bool> UpdateRangeAsync(IEnumerable<TDto> dtos)
        {
            var entities = DtoToEntity(dtos);
            Repository.UpdateRange(entities);
            await _unitOfWork.SaveChangeAsync();
            return true;
        }

        public virtual TDto EntityToDto(TEntity entity)
        {
            return _mapper.Map<TDto>(entity);
        }

        public virtual TEntity DtoToEntity(TDto dto)
        {
            return _mapper.Map<TEntity>(dto);
        }

        public virtual IEnumerable<TDto> EntityToDto(IEnumerable<TEntity> entities)
        {
            return _mapper.Map<IEnumerable<TDto>>(entities);
        }

        public virtual IEnumerable<TEntity> DtoToEntity(IEnumerable<TDto> dto)
        {
            return _mapper.Map<IEnumerable<TEntity>>(dto);
        }

        public ICollection<TEntity> FindAll(Expression<Func<TEntity, bool>> filter = null)
        {
            return Repository.FindAll(filter);
        }

        public IQueryable<TEntity> AsQuery(Expression<Func<TEntity, bool>> filter = null)
        {
            return Repository.AsQuery(filter);
        }

        public Task<int> MaxByAsync(Expression<Func<TEntity, int>> expression, CancellationToken token = default)
        {
            return Repository.MaxByAsync(expression, token);
        }

        public int MaxBy(Expression<Func<TEntity, int>> expression)
        {
            return Repository.MaxBy(expression);
        }

        protected virtual async Task<string> GetUniqueCode(string prefix)
        {
            var maxId = await MaxByAsync(x => x.Id);
            return $"{prefix}{maxId:D4}";
        }
    }
}
