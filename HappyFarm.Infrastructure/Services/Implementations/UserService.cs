﻿using AutoMapper;
using HappyFarm.Core.Commons;
using HappyFarm.Core.Enums;
using HappyFarm.Core.Models;
using HappyFarm.Core.Repositories;
using HappyFarm.Core.UnitOfWork;
using HappyFarm.Infrastructure.Dto;
using HappyFarm.Infrastructure.Helpers;
using HappyFarm.Infrastructure.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HappyFarm.Infrastructure.Services.Implementations
{
    public class UserService : CrudService<User, UserDto>, IUserService
    {
        protected override IRepository<User> Repository => _unitOfWork.UserRepository;

        public UserService(
            ILogger<AuthService> logger,
            IUnitOfWork unitOfWork,
            IMapper mapper
            ) : base(mapper, unitOfWork, logger)
        {
        }

        public UserDto GetDetailUser(int id, int? farmId)
        {
            try
            {
                if (farmId.HasValue && farmId.Value > 0)
                {
                    return Find(x => x.Id == id && x.FarmId == farmId && !x.IsDeactivated);
                }
                return FindById(id);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return null;
            }
        }

        public async Task<UserDto> GetDetailUserAsync(int id, int? farmId)
        {
            try
            {
                if (farmId.HasValue && farmId.Value > 0)
                {
                    return await FindAsync(x => x.Id == id && x.FarmId == farmId && !x.IsDeactivated);
                }
                return await FindByIdAsync(id);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return null;
            }
        }

        public PaginatedList<UserDto> GetUserList(string keyword, string byRole, string currentRole, int? page, int? pageSize)
        {
            try
            {
                var query = InternalGetUserList(AsQuery(), keyword, byRole, currentRole);
                var result = PaginatedList<UserDto>.Create(query, page, pageSize);
                return result;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return null;
            }
        }

        public Task<PaginatedList<UserDto>> GetUserListAsync(string keyword, string byRole, string currentRole, int? page, int? pageSize)
        {
            try
            {
                var query = InternalGetUserList(AsQuery(), keyword, byRole, currentRole);
                var result = PaginatedList<UserDto>.CreateAsync(query, page, pageSize);
                return result;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return null;
            }
        }

        public async Task<KeyValuePair<OperationStatus, string>> GetCurrentRoleOfUserAsync(int userId)
        {
            try
            {
                var user = await FindByIdAsync(userId);
                if (user != null)
                {
                    return new KeyValuePair<OperationStatus, string>(OperationStatus.Succeed, user.Role);
                }
                return new KeyValuePair<OperationStatus, string>(OperationStatus.Failed, null);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return new KeyValuePair<OperationStatus, string>(OperationStatus.Exception, null);
            }
        }

        public KeyValuePair<OperationStatus, string> GetCurrentRoleOfUser(int userId)
        {
            try
            {
                var user = FindById(userId);
                if (user != null)
                {
                    return new KeyValuePair<OperationStatus, string>(OperationStatus.Succeed, user.Role);
                }
                return new KeyValuePair<OperationStatus, string>(OperationStatus.Failed, null);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return new KeyValuePair<OperationStatus, string>(OperationStatus.Exception, null);
            }
        }

        public async Task<(OperationStatus, string)> TryGetUserAvatar(int userId, string baseAbsolutePath)
        {
            try
            {
                var user = await FindByIdAsync(userId);
                if (user != null)
                {
                    if (!StringUtils.CheckIsEmptyOrWhitespaceString(user.Avatar))
                    {
                        return (OperationStatus.Succeed, user.Avatar);
                    }

                    return (OperationStatus.Succeed, null);
                }

                return (OperationStatus.NotFound, null);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return (OperationStatus.Exception, null);
            }
        }
        

        public KeyValuePair<OperationStatus, int?> GetCurrentFarmIdOfUser(int userId)
        {
            try
            {
                var user = FindById(userId);
                if (user != null)
                {
                    return new KeyValuePair<OperationStatus, int?>(OperationStatus.Succeed, user.FarmId);
                }
                return new KeyValuePair<OperationStatus, int?>(OperationStatus.NotFound, null);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return new KeyValuePair<OperationStatus, int?>(OperationStatus.Exception, null);
            }
        }

        public async Task<KeyValuePair<OperationStatus, int?>> GetCurrentFarmIdOfUserAsync(int userId)
        {
            try
            {
                var user = await FindByIdAsync(userId);
                if (user != null)
                {
                    return new KeyValuePair<OperationStatus, int?>(OperationStatus.Succeed, user.FarmId);
                }
                return new KeyValuePair<OperationStatus, int?>(OperationStatus.NotFound, null);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return new KeyValuePair<OperationStatus, int?>(OperationStatus.Exception, null);
            }
        }

        public async Task<OperationStatus> CreateUser(UserDto userInfo)
        {
            try
            {
                if (userInfo.Role == User.ADMIN_ROLE)
                {
                    userInfo.FarmId = null;
                }
                userInfo.Password = HashingHelper.Hash(userInfo.Password);

                _ = await CreateAsync(userInfo);
                return OperationStatus.Succeed;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return OperationStatus.Exception;
            }
        }

        public async Task<OperationStatus> UpdateUser(UserDto userDto)
        {
            try
            {
                var dbUser = await FindByIdAsync(userDto.Id);

                if (dbUser.Role == User.ADMIN_ROLE)
                {
                    userDto.FarmId = null;
                }

                dbUser.Firstname = userDto.Firstname;
                dbUser.Lastname = userDto.Lastname;
                dbUser.Email = userDto.Email;

                await UpdateAsync(dbUser);

                return OperationStatus.Succeed;

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return OperationStatus.Exception;
            }
        }

        public async Task<OperationStatus> DeleleUsers(IEnumerable<int> listId)
        {
            try
            {
                var deleted = await DeleteByAsync(x => 
                    !x.PreventDelete &&
                    listId.Contains(x.Id)
                );

                return deleted >= 0 ? OperationStatus.Succeed : OperationStatus.Failed;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return OperationStatus.Exception;
            }
        }

        public async Task<OperationStatus> IsUserExist(int userId, string role)
        {
            try
            {
                var query = AsQuery(x => x.Id == userId);

                if (!StringUtils.CheckIsEmptyOrWhitespaceString(role) && User.Roles.Contains(role))
                {
                    query = query.Where(x => x.Role == role);
                }
                var result = await query.AnyAsync();
                return result ? OperationStatus.Succeed : OperationStatus.NotFound;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return OperationStatus.Exception;
            }
        }

        public async Task<OperationStatus> IsUserExist(int userId)
        {
            try
            {
                var result = await ExistsByIdAsync(userId);
                return result ? OperationStatus.Succeed : OperationStatus.NotFound;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return OperationStatus.Exception;
            }
        }

        public async Task<OperationStatus> CheckIfFarmOfUserAndGatewayIsSame(int userId, int gatewayId)
        {
            try
            {
                var user = await FindByIdAsync(userId);
                var gateway = await _unitOfWork.GatewayRepository.FindByIdAsync(gatewayId);

                return (
                    user != null &&
                    gateway != null && 
                    user.FarmId == gateway.FarmId
                    ) ? OperationStatus.Succeed : OperationStatus.Incorrect;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return OperationStatus.Exception;
            }
        }

        private IQueryable<User> SearchByKeyword(IQueryable<User> source, string keyword)
        {
            if (!StringUtils.CheckIsEmptyOrWhitespaceString(keyword))
            {
                keyword = keyword.Trim().ToLower();
                return source.Where(x =>
                    EF.Functions.Like(x.Firstname.ToLower().Trim(), $"%{keyword}%") ||
                    EF.Functions.Like(x.Lastname.ToLower().Trim(), $"%{keyword}%") ||
                    EF.Functions.Like(x.Email.ToLower().Trim(), $"%{keyword}%") ||
                    EF.Functions.Like(x.Username.ToLower().Trim(), $"%{keyword}%")
                );
            }
            return source;
        }

        private IQueryable<UserDto> InternalGetUserList(IQueryable<User> source, string keyword, string byRole, string currentUserRole)
        {
            var query = SearchByKeyword(source, keyword)
                        .Where(x => !x.IsDeactivated);

            if (!string.IsNullOrEmpty(byRole))
            {
                if (currentUserRole != User.ADMIN_ROLE)
                {
                    byRole = currentUserRole;
                }

                query = query.Where(x => x.Role == byRole);
            }

            var result = query
                .OrderByDescending(x => x.CreatedAt)
                .Select(x =>
                        new UserDto
                        {
                            Id = x.Id,
                            Firstname = x.Firstname,
                            Lastname = x.Lastname,
                            Avatar = x.Avatar,
                            Email = x.Email,
                            Username = x.Username,
                            Role = x.Role,
                            Farm = x.FarmId.HasValue ? new FarmDto
                            {
                                Id = x.Farm.Id,
                                Name = x.Farm.Name
                            } : null,
                            CreatedAt = x.CreatedAt,
                            UpdatedAt = x.UpdatedAt
                        });
            return result;
        }
    }
}
