﻿using AutoMapper;
using HappyFarm.Core.Commons;
using HappyFarm.Core.Enums;
using HappyFarm.Core.Models;
using HappyFarm.Core.Repositories;
using HappyFarm.Core.UnitOfWork;
using HappyFarm.Infrastructure.Dto;
using HappyFarm.Infrastructure.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HappyFarm.Infrastructure.Services.Implementations
{
    public class SensorValueService : CrudService<SensorValue, SensorValueDto>, ISensorValueService
    {

        public SensorValueService(
           IMapper mapper,
           IUnitOfWork unitOfWork,
           ILogger<FarmService> logger
       ) : base(mapper, unitOfWork, logger) { }

        protected override IRepository<SensorValue> Repository => _unitOfWork.SensorValueRepository;

        public async Task<OperationStatus> AddValue(SensorValueDto value)
        {
            try
            {
                var isGwExists = await _unitOfWork.SensorNodeRepository.ExistsByIdAsync(value.NodeId.GetValueOrDefault());
                if (!isGwExists) return OperationStatus.NotFound;
                await CreateAsync(value);
                return OperationStatus.Succeed;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return OperationStatus.Exception;
            }
        }

        public async Task<KeyValuePair<OperationStatus, IEnumerable<SensorValueDto>>> AddValueRange(IEnumerable<SensorValueDto> values)
        {
            try
            {
                values = values
                    .GroupBy(x => new { x.NodeId, x.CreatedAt })
                    .Where(x => x.Any())
                    .Select(x => x.FirstOrDefault());
                var listSensorNodeValue = values.Select(x => x.NodeId.GetValueOrDefault()).ToArray();
                var nodeId = await _unitOfWork.SensorNodeRepository.AsQuery().Select(x => x.Id).ToArrayAsync();
                var isEverySensorValueExist = await 
                    _unitOfWork
                    .SensorNodeRepository
                    .AsQuery()
                    .AnyAsync(x => listSensorNodeValue.Contains(x.Id));

                _logger.LogInformation("isEverySensorValueExist: {0}", isEverySensorValueExist);
                _logger.LogInformation("Data values: {0}", values.Count());

                if (!isEverySensorValueExist)
                    return new KeyValuePair<OperationStatus, IEnumerable<SensorValueDto>>(OperationStatus.Failed, null);
                await CreateRangeAsync(values);

                return new KeyValuePair<OperationStatus, IEnumerable<SensorValueDto>>(OperationStatus.Succeed, values);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return new KeyValuePair<OperationStatus, IEnumerable<SensorValueDto>>(OperationStatus.Exception, null);
            }
        }

        public async Task<PaginatedList<SensorValueDto>> GetListOfSensorValues(int? from, int? to, int? farmId, int? gwId, int? nodeId, int? page, int? pageSize)
        {
            try
            {
                (from, to) = PrepairQueryTimeFilter(from, to);

                var query = AsQuery()
                    .AsQueryable();

                if (farmId.HasValue && farmId > 0)
                {
                    query = query.Where(x => x.SensorNode.Gateway.FarmId == farmId);
                }

                if (gwId.HasValue && gwId > 0)
                {
                    query = query.Where(x => x.SensorNode.GatewayId == gwId);
                }

                if (nodeId.HasValue && nodeId > 0)
                {
                    query = query.Where(x => x.SensorNode.Id == nodeId);
                }

                query = query
                    .Where(x => x.CreatedAt >= from && x.CreatedAt <= to)
                    .OrderByDescending(x => x.CreatedAt);

                var result = query.Select(x =>
                    new SensorValueDto
                    {
                        Id = x.Id,
                        AirHumidity = x.AirHumidity,
                        LightIntensity = x.LightIntensity,
                        PhScale = x.PhScale,
                        RainFall = x.RainFall,
                        SoilHumidity = x.SoilHumidity,
                        Temperature = x.Temperature,
                        WindSpeed = x.WindSpeed,
                        Tds = x.Tds,
                        SensorNode = x.SensorNode != null
                            ? new SensorNodeDto
                            {
                                Id = x.SensorNode.Id,
                                Code = x.SensorNode.Code,
                                Name = x.SensorNode.Name,
                                Gateway =
                                x.SensorNode.Gateway != null
                                ? new GatewayDto
                                {
                                    Id = x.SensorNode.GatewayId,
                                    Name = x.SensorNode.Gateway.Name,
                                    Farm = x.SensorNode.Gateway.Farm != null 
                                    ? new FarmDto
                                    {
                                        Id = x.SensorNode.Gateway.FarmId,
                                        Name = x.SensorNode.Gateway.Farm.Name
                                    } : null
                                } : null,

                            }
                            : null,
                        CreatedAt = x.CreatedAt
                    }) ;

                return await PaginatedList<SensorValueDto>.CreateAsync(result, page, pageSize);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }
        }

        private static (int, int) PrepairQueryTimeFilter(int? from, int? to)
        {
            @from ??= (int) DateTimeOffset.UtcNow.AddMonths(-1).ToUnixTimeSeconds();
            
            to ??= DateTimeUtils.ConvertToUnixTime(DateTime.Now);

            var toDate = DateTimeUtils.ConvertFromUnixTime(to.Value);
            var fromDate = DateTimeUtils.ConvertFromUnixTime(from.Value);

            var diffToFrom = (int) ((toDate - fromDate).TotalDays / 30);
            var utcNow = DateTimeOffset.UtcNow;
            var last3Month = utcNow.AddMonths(-1);
            var diffNowLast3Month = (int)((DateTimeOffset.UtcNow - last3Month).TotalDays / 30);

            if (diffToFrom > diffNowLast3Month)
            {
                from = (int)last3Month.ToUnixTimeSeconds();
            }

            return (from.Value, to.Value);
        }
    }
}
