﻿using HappyFarm.Core.Commons;
using HappyFarm.Infrastructure.Dto;
using HappyFarm.Infrastructure.Services.Interfaces;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;

namespace HappyFarm.Infrastructure.Services.Implementations
{
    public class MqttHeartbeatService : BaseHttpService, IMqttHeartbeatService
    {
        public MqttHeartbeatService(string baseUrl, string token, ILogger<MqttHeartbeatService> logger)
            : base(baseUrl, token, logger) { }

        public async Task<VernemqHeartbeat> GetHeartbeatData()
        {
            var healthCheck = await GetAsync<VernemqHealthStatus>(HttpApiRoutes.HEALTH_CHECK);
            var clusterStatus = await GetAsync<VernemqClusterStatus>(HttpApiRoutes.CLUSTER_STATUS_INFORMATION);
            var sessionInformations = await GetAsync<SessionInformation>(HttpApiRoutes.SESSION_INFORMATION);
            var installedListeners = await GetAsync<InstalledListeners>(HttpApiRoutes.ALL_INSTALLED_LISTENERS);
            var installedPlugins = await GetAsync<InstalledPlugins>(HttpApiRoutes.ALL_INSTALLED_PLUGINS);

            var results = new object[] { healthCheck, clusterStatus, sessionInformations, installedListeners, installedPlugins };

            if (results.Any(x => x == null))
            {
                return null;
            }

            return new VernemqHeartbeat
            {
                ClusterStatus = clusterStatus.Table,
                Status = healthCheck.Status == "OK",
                SessionInformations = sessionInformations.Table,
                InstalledListeners = installedListeners.Table,
                InstalledPlugins = installedPlugins.Table,
            };
        }
    }
}
