﻿using AutoMapper;
using HappyFarm.Core.Commons;
using HappyFarm.Core.Enums;
using HappyFarm.Core.Models;
using HappyFarm.Core.Repositories;
using HappyFarm.Core.UnitOfWork;
using HappyFarm.Infrastructure.Dto;
using HappyFarm.Infrastructure.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace HappyFarm.Infrastructure.Services.Implementations
{
    public class FarmService : CrudService<Farm, FarmDto>, IFarmService
    {
        private readonly IUserService _userService;
        private readonly IGatewayService _gatewayService;

        public FarmService(
            IMapper mapper,
            IUnitOfWork unitOfWork,
            ILogger<FarmService> logger,
            IUserService userService,
            IGatewayService gatewayService
            ) 
            : base(mapper, unitOfWork, logger)
        {
            _userService = userService;
            _gatewayService = gatewayService;
        }

        protected override IRepository<Farm> Repository => _unitOfWork.FarmRepository;

        public async Task<bool> CreateFarm(FarmDto farm)
        {
            try
            {
                var result = await CreateAsync(farm);
                return result != null;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return false;
            }
        }

        public async Task<OperationStatus> DeleteFarms(IEnumerable<int?> listFarmId)
        {
            try
            {
                var listId = listFarmId
                    .Where(x => x > 0);

                if (listId.Any())
                {
                    var hasContainsUsers = await _unitOfWork.UserRepository.ExistsByAsync(x => listId.Contains(x.FarmId.Value));
                    var hasContainsGateways = await _unitOfWork.GatewayRepository.ExistsByAsync(x => listId.Contains(x.FarmId));

                    var deletedUsers = true;
                    var deletedGateways = true;

                    if (hasContainsGateways)
                    {
                        var gateways = _unitOfWork.GatewayRepository
                            .AsQuery(x => listFarmId.Contains(x.FarmId));

                        var deletedNodes = await _unitOfWork.SensorNodeRepository
                            .AsQuery(x => gateways.Any(y => y.Id == x.GatewayId))
                            .DeleteAsync() >= 0;

                        if (deletedNodes)
                        {
                            deletedGateways = await _unitOfWork.GatewayRepository
                                .AsQuery(x => listFarmId.Contains(x.FarmId))
                                .DeleteAsync() > 0;
                        }
                        else
                        {
                            deletedGateways = false;
                        }
                        
                    }

                    if (hasContainsUsers)
                    {
                        deletedUsers = await _unitOfWork.UserRepository
                            .AsQuery(x => listFarmId.Contains(x.FarmId))
                            .DeleteAsync() > 0;
                    }

                    if (deletedUsers && deletedGateways)
                    {
                        return (await DeleteByAsync(x => listId.Contains(x.Id))) >= 0 ? OperationStatus.Succeed : OperationStatus.Failed;
                    }

                    return OperationStatus.Failed;
                }

                return OperationStatus.Failed;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return OperationStatus.Exception;
            }
        }

        public IEnumerable<FarmDto> GetFarms()
        {
            try
            {
                return EntityToDto(FindAll());
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return null;
            }
            
        }

        public async Task<PaginatedList<FarmDto>> GetAllFarms(string keyword, int? page, int? pageSize)
        {
            try
            {
                var query = SearchByKeyword(AsQuery(), keyword);
                var resultData = query
                    .OrderByDescending(x => x.CreatedAt)
                    .Select(x => new FarmDto
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Address = x.Address,
                        RegionCode = x.RegionCode,
                        Phone = x.Phone,
                        TotalUsers = x.Users.Count(),
                        TotalGateways = x.Gateways.Count(),
                        CreatedAt = x.CreatedAt,
                        UpdatedAt = x.UpdatedAt
                    });
                return await PaginatedList<FarmDto>.CreateAsync(resultData, page, pageSize);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return null;
            }
        }

        public async Task<IEnumerable<DropdownDto>> GetDropdownFarms(int? farmId)
        {
            try
            {
                var query = AsQuery();
                if (farmId.HasValue && farmId.Value > 0)
                {
                    query = query.Where(x => x.Id == farmId);
                }

                var resultData = query.Select(x => new DropdownDto
                {
                    Id = x.Id,
                    Name = x.Name
                });

                return await resultData.ToArrayAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return null;
            }
        }

        public async Task<FarmDto> GetFarm(int id)
        {
            try
            {
                return await GetFarmDetailById(id);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return null;
            }
        }

        public async Task<FarmDto> GetFarmByUser(int userId)
        {
            try
            {
                var user = await _userService.FindByIdAsync(userId);

                return await GetFarmDetailById(user.FarmId.GetValueOrDefault());
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return null;
            }
        }


        public async Task<IEnumerable<int>> GetAllGatewayIdOwnedByFarm(int farmId)
        {
            try
            {
                var query = await (
                    from f in AsQuery()
                    join gw in _unitOfWork.GatewayRepository.AsQuery()
                    on f.Id equals gw.FarmId into t1
                    from t2 in t1.DefaultIfEmpty()
                    where f.Id == farmId
                    select t2.Id
                ).ToArrayAsync();

                return query;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }
        }

        public async Task<bool> UpdateFarm(FarmDto farm)
        {
            try
            {
                await UpdateAsync(farm);
                return true;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return false;
            }
        }

        private IQueryable<Farm> SearchByKeyword(IQueryable<Farm> source, string keyword)
        {
            if (!StringUtils.CheckIsEmptyOrWhitespaceString(keyword))
            {
                keyword = keyword.Trim().ToLower();
                return source.Where(x =>
                    EF.Functions.Like(x.Name.Trim().ToLower(), $"%{keyword}%") ||
                    EF.Functions.Like(x.Phone.Trim().ToLower(), $"%{keyword}%") ||
                    EF.Functions.Like(x.Address.Trim().ToLower(), $"%{keyword}%")
                );
            }

            return source;
        }

        private async Task<FarmDto> GetFarmDetailById(int farmId)
        {
            var userQuery = _userService.AsQuery();
            var gwQuery = _gatewayService.AsQuery();
            var farmQuery = AsQuery();

            return await(
                    from f in farmQuery
                    join u in userQuery
                    on f.Id equals u.FarmId into joinedTbl1
                    from tmpTbl1 in joinedTbl1.DefaultIfEmpty()

                    join gw in gwQuery
                    on f.Id equals gw.FarmId into joinedTbl2
                    from tmpTbl2 in joinedTbl2.DefaultIfEmpty()

                    where f.Id == farmId
                    select new FarmDto
                    {
                        Id = f.Id,
                        Name = f.Name,
                        Address = f.Address,
                        RegionCode = f.RegionCode,
                        Phone = f.Phone,
                        Gateways = f.Gateways.Any() ?
                            f.Gateways.Select(
                                x => new GatewayDto
                                {
                                    Id = x.Id,
                                    Name = x.Name
                                }).ToArray() : null,
                        CreatedAt = f.CreatedAt,
                        UpdatedAt = f.UpdatedAt
                    }
                    ).FirstOrDefaultAsync();
        }

        public async Task<OperationStatus> CheckExistsBy(Expression<Func<Farm, bool>> predicate)
        {
            try
            {
                return await ExistsByAsync(predicate) ? OperationStatus.Succeed : OperationStatus.NotFound;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return OperationStatus.Exception;
            }
        }
    }
}
