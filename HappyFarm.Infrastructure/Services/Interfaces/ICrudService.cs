﻿using HappyFarm.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace HappyFarm.Infrastructure.Services.Interfaces
{
    public interface ICrudService<TEntity, TDto> 
        where TEntity : class, IEntity
        where TDto : new()
    {
        Task<TDto> CreateAsync(TDto dto, CancellationToken token = default);

        Task CreateRangeAsync(IEnumerable<TDto> dtos, CancellationToken token = default);

        TDto Create(TDto dto);

        void CreateRange(IEnumerable<TDto> dtos);

        Task<bool> UpdateAsync(TDto dto, CancellationToken token = default);

        bool Update(TDto dto);

        Task<bool> UpdateRangeAsync(IEnumerable<TDto> dtos);

        Task DeleteAsync(int id, CancellationToken token);

        Task<int> DeleteAsync(IEnumerable<int> listId, CancellationToken token = default);

        Task<int> DeleteByAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken token = default);

        int DeleteBy(Expression<Func<TEntity, bool>> predicate);

        Task<TDto> FindByIdAsync(int id, CancellationToken token = default);

        TDto FindById(int id);

        Task<TDto> FindAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken token = default);

        TDto Find(Expression<Func<TEntity, bool>> predicate);

        bool ExistsById(int id);

        Task<bool> ExistsByIdAsync(int id);

        bool ExistsBy(Expression<Func<TEntity, bool>> predicate);

        Task<bool> ExistsByAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken token = default);

        ICollection<TEntity> FindAll(Expression<Func<TEntity, bool>> filter = null);

        IQueryable<TEntity> AsQuery(Expression<Func<TEntity, bool>> filter = null);

        Task<int> MaxByAsync(Expression<Func<TEntity, int>> expression, CancellationToken token = default);

        int MaxBy(Expression<Func<TEntity, int>> expression);
    }
}
