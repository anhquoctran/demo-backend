﻿using HappyFarm.Core.Enums;
using HappyFarm.Core.Models;
using HappyFarm.Infrastructure.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HappyFarm.Infrastructure.Services.Interfaces
{
    public interface ISensorNodeService : ICrudService<SensorNode, SensorNodeDto>
    {
        Task<IEnumerable<SensorNodeDto>> GetSensorsByGateway(int gatewayId);

        Task<IEnumerable<DropdownDto>> GetSensorsDropdownByGateway(int gatewayId);

        Task<SensorNodeDto> GetSensorNode(int nodeId, bool mustIncludeValue = false);

        Task<PaginatedList<SensorNodeDto>> GetSensorsList(string keyword, int? farmId, int? gatewayId, int? page, int? pageSize);

        Task<OperationStatus> AddSensorNode(SensorNodeDto sensor);

        Task<OperationStatus> UpdateSensorNode(SensorNodeDto sensorNodeDto);

        Task<int> CountNodeByGateway(int gatewayId);

        Task<bool> ValidateNodeOwnedByGateway(int gatewayId, IEnumerable<string> listNodeId);

        Task<int> GetIdByCode(string gwCode);

        Task<OperationStatus> IsExistsById(int sensorNodeId);

        Task<OperationStatus> IsUserSensorNodeHasSameFarm(int nodeId, int userId);

        Task<OperationStatus> DeleteNodes(IEnumerable<int> nodeIds);
    }
}
