﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace HappyFarm.Infrastructure.Services.Interfaces
{
    public interface IDistributedService
    {

        static bool IsConnect { get; }

        IEnumerable<string> this[int dbIndex, string pattern = "*"] { get; }

        Task<bool> SetStringAsync(string key, string value, int databaseIndex, CancellationToken token = default);

        bool SetString(string key, string value, int databaseIndex);

        string GetString(string key, int databaseIndex);

        Task<string> GetStringAsync(string key, int databaseIndex, CancellationToken token = default);

        T GetObject<T>(string key, int databaseIndex);

        Task<T> GetObjectAsync<T>(string key, int databaseIndex, CancellationToken token = default);

        Task<bool> DeleteAsync(string key, int databaseIndex, CancellationToken token = default);

        bool BatchDelete(string[] keys, int databaseIndex);

        Task<bool> BatchSetStringAsync(Dictionary<string, string> data, int databaseIndex);

        Task<bool> SetToSetsAsync(string setName, string setValue, int dbIndex);

        bool SetToSets(string setName, string setValue, int dbIndex);

        Task<bool> RemoveSetElementAsync(string setName, string value, int dbIndex);

        bool RemoveSetElement(string setName, string value, int dbIndex);
    }
}
