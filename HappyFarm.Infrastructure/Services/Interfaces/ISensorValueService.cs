﻿using HappyFarm.Core.Enums;
using HappyFarm.Core.Models;
using HappyFarm.Infrastructure.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HappyFarm.Infrastructure.Services.Interfaces
{
    public interface ISensorValueService : ICrudService<SensorValue, SensorValueDto>
    {
        Task<OperationStatus> AddValue(SensorValueDto value);

        Task<KeyValuePair<OperationStatus, IEnumerable<SensorValueDto>>> AddValueRange(IEnumerable<SensorValueDto> values);

        Task<PaginatedList<SensorValueDto>> GetListOfSensorValues(int? from, int? to, int? farmId, int? gwId, int? nodeId, int? page, int? pageSize);
    }
}
