using System.Collections.Generic;
using System.Threading.Tasks;
namespace HappyFarm.Infrastructure.Services.Interfaces
{
    public interface IHttpService
    {
        Task<T> GetAsync<T>(
          string path,
          Dictionary<string, string> queryParamaters = null,
          Dictionary<string, string> headers = null
        ) where T : class, new();

        Task<bool> PostAsync<T>(
          string path,
          T body,
          Dictionary<string, string> queryParameters = null,
          Dictionary<string, string> headers = null
        ) where T : class, new();
    }
}