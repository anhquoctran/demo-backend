﻿using HappyFarm.Core.Enums;
using HappyFarm.Core.Models;
using HappyFarm.Infrastructure.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HappyFarm.Infrastructure.Services.Interfaces
{
    public interface IUserService : ICrudService<User, UserDto>
    {
        Task<UserDto> GetDetailUserAsync(int id, int? farmId);

        UserDto GetDetailUser(int id, int? farmId);

        Task<PaginatedList<UserDto>> GetUserListAsync(string keyword, string byRole, string currentRole, int? page, int? pageSize);

        PaginatedList<UserDto> GetUserList(string keyword, string byRole, string currentRole, int? page, int? pageSize);

        Task<KeyValuePair<OperationStatus, string>> GetCurrentRoleOfUserAsync(int userId);

        KeyValuePair<OperationStatus, string> GetCurrentRoleOfUser(int userId);

        KeyValuePair<OperationStatus, int?> GetCurrentFarmIdOfUser(int userId);

        Task<KeyValuePair<OperationStatus, int?>> GetCurrentFarmIdOfUserAsync(int userId);

        Task<OperationStatus> CreateUser(UserDto userInfo);

        Task<OperationStatus> UpdateUser(UserDto userDto);

        Task<(OperationStatus, string)> TryGetUserAvatar(int userId, string baseAbsolutePath);

        Task<OperationStatus> IsUserExist(int userId, string role);

        Task<OperationStatus> IsUserExist(int userId);

        Task<OperationStatus> CheckIfFarmOfUserAndGatewayIsSame(int userId, int gatewayId);

        Task<OperationStatus> DeleleUsers(IEnumerable<int> listId);
    }
}
