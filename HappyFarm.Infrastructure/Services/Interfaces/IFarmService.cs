﻿using HappyFarm.Core.Enums;
using HappyFarm.Core.Models;
using HappyFarm.Infrastructure.Dto;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace HappyFarm.Infrastructure.Services.Interfaces
{
    public interface IFarmService : ICrudService<Farm, FarmDto>
    {

        IEnumerable<FarmDto> GetFarms();

        Task<FarmDto> GetFarmByUser(int userId);

        Task<IEnumerable<DropdownDto>> GetDropdownFarms(int? farmId);

        Task<PaginatedList<FarmDto>> GetAllFarms(string keyword, int? page, int? pageSize);

        Task<IEnumerable<int>> GetAllGatewayIdOwnedByFarm(int farmId);

        Task<FarmDto> GetFarm(int id);

        Task<bool> CreateFarm(FarmDto farm);

        Task<bool> UpdateFarm(FarmDto farm);

        Task<OperationStatus> DeleteFarms(IEnumerable<int?> listFarmId);

        Task<OperationStatus> CheckExistsBy(Expression<Func<Farm, bool>> predicate);
    }
}
