﻿using HappyFarm.Infrastructure.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HappyFarm.Infrastructure.Services.Interfaces
{
    public interface IMqttHeartbeatService
    {
        Task<VernemqHeartbeat> GetHeartbeatData();
    }
}
