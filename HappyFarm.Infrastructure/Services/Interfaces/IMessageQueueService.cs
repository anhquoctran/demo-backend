﻿using HappyFarm.Core.Events;
using HappyFarm.Infrastructure.Dto;
using System.Threading.Tasks;

namespace HappyFarm.Infrastructure.Services.Interfaces
{
    public interface IMessageQueueService
    {
        delegate Task OnMessageReceivedEventHandler<TData>(MessageReceivedEventArgs<TData> eventArgs);

        event OnMessageReceivedEventHandler<string> OnMessageReceived;

        bool IsRunning { get; }

        Task<bool> Disconnect();

        Task<bool> SubscribeTo(string topicLike);

        Task<bool> Publish<T>(PublishMessageData<T> packet);

        Task<bool> Publish(string topic);
    }
}
