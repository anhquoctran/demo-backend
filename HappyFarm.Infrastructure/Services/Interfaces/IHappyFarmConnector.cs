﻿using HappyFarm.Core.Models;
using HappyFarm.Infrastructure.Dto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HappyFarm.Infrastructure.Services.Interfaces
{
    public interface IHappyFarmConnector
    {
        [HubMethodName("cmdState"), Authorize(Policy = "HFAuthorizationPolicy")]
        Task ReceivedResponseCmdState(CmdStateResponse cmdState);

        [HubMethodName("sensorValue"), Authorize(Policy = "HFAuthorizationPolicy")]
        Task ReceivedSensorValueData(SensorValuesCmd value);

        [HubMethodName("sensorCmd"), Authorize(Policy = "HFAuthorizationPolicy")]
        Task ReceivedSensorCmd(GatewayControl data);

        [HubMethodName("gatewayStatus"), Authorize(Policy = "HFAuthorizationPolicy")]
        Task ReceivedGatewayStatus(GatewayStatus gwStatus);

        [HubMethodName("powerControl"), Authorize(Policy = "HFAuthorizationPolicy")]
        Task ReceivedGatewayPowerControl(PowerControl powerControl);

        [HubMethodName("applyControl"), Authorize(Policy = "HFAuthorizationPolicy")]
        Task ReceivedGatewayApplyControl(ApplyControl applyControl);

        [HubMethodName("timerMode"), Authorize(Policy = "HFAuthorizationPolicy")]
        Task ReceivedTimerMode(TimerMode timerMode);

        [HubMethodName("timerSetting"), Authorize(Policy = "HFAuthorizationPolicy")]
        Task ReceivedTimerSetting(SettingTimer settingTimer);

        [HubMethodName("administrativeLog"), Authorize(Policy = "HFAuthorizationPolicy")]
        Task ReceivedAdministrativeLogging(AdministrativeLog logData);

        [HubMethodName("deviceInformation"), Authorize(Policy = "HFAuthorizationPolicy")]
        Task ReceivedDeviceInformation(InformationData information);

        [HubMethodName("gatewaySensors"), Authorize(Policy = "HFAuthorizationPolicy")]
        Task ReceivedGatewaySensors(GatewaySensorBundle data);

        Task pong();
    }
}
