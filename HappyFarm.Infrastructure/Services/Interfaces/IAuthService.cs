﻿using HappyFarm.Core.Enums;
using HappyFarm.Core.Models;
using HappyFarm.Infrastructure.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HappyFarm.Infrastructure.Services.Interfaces
{
    public interface IAuthService : ICrudService<User, UserDto>
    {
        Task<KeyValuePair<AuthResult, AuthenticatedUserDto>> Authenticate(string username, string password);

        bool Authorize(int id);

        Task<OperationStatus> Authorize(string jwtToken);

        Task<UserDto> GetUserProfile(int id);

        Task<OperationStatus> UpdateProfile(UserDto dto);

        Task<OperationStatus> UpdatePassword(int id, ChangePasswordDto viewModel);

        IEnumerable<string> GetPermissions(string role);

        OperationStatus IsRoleCanAccess(int userId, params string[] roles);
    }
}
