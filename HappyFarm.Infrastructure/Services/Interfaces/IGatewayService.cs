﻿using HappyFarm.Core.Enums;
using HappyFarm.Core.Models;
using HappyFarm.Infrastructure.Dto;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace HappyFarm.Infrastructure.Services.Interfaces
{
    public interface IGatewayService : ICrudService<Gateway, GatewayDto>
    {
        Task<PaginatedList<GatewayDto>> FindAllAsync(string keyword, int? userId, int? page, int? pageSize, params int[] farms);

        Task<KeyValuePair<OperationStatus, GatewayDto>> GetGatewayDetailForManager(int id, int managerId);

        Task<IEnumerable<GatewayDto>> GetAllGatewaysByManager(int managerId);

        Task<IEnumerable<int>> GetListGatewayIdByFarmId(int farmId);

        Task<GatewayMqttInfoDto> GetMqttInfo(string deviceCode);

        Task<OperationStatus> CreateGateway(GatewayDto gateway, int userId);

        Task<int> GetGatewayIdByCode(string code);

        Task<GatewayDto> GetGateway(int gatewayId);

        Task<GatewayDto> GetGatewayDetail(int gatewayId);

        Task<OperationStatus> IsExistById(int gatewayId);

        Task<OperationStatus> CheckExistsBy(Expression<Func<Gateway, bool>> predicate);

        Task<OperationStatus> UpdateGateway(GatewayDto gateway, int userId);

        Task<OperationStatus> UpdateSettings(DeviceConfig deviceConfig, IEnumerable<int> gatewayId, bool sendToDevice = false);

        Task<IEnumerable<DropdownDto>> GetGatewayDropdown(int? farmId);

        Task<OperationStatus> Delete(IEnumerable<int> items);
    }
}
