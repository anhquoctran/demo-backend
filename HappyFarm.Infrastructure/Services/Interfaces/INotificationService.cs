﻿using System.Net;
using System.Threading.Tasks;

namespace HappyFarm.Infrastructure.Services.Interfaces
{
    public interface INotificationService
    {
        Task<bool> SendAsync(string header, string content);

        bool Send(string header, string content);

        ICredentialsByHost GetCredential();
    }
}
