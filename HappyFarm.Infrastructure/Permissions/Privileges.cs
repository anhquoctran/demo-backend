﻿using HappyFarm.Core.Commons;
using HappyFarm.Core.Models;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace HappyFarm.Infrastructure.Permissions
{
    public static class Privileges
    {
        /// <summary>
        /// Can login to the web
        /// </summary>
        public static readonly string[] CanLoginToWeb = { User.ADMIN_ROLE, User.MANAGER_ROLE };

        /// <summary>
        /// Can reset password for admin
        /// </summary>
        public static readonly string[] CanDoSystemResetPasswordForAdmin = { User.ADMIN_ROLE };

        /// <summary>
        /// Can reset password for manager
        /// </summary>
        public static readonly string[] CanDoSystemResetPasswordForManager = { User.ADMIN_ROLE };

        /// <summary>
        /// Can view farms
        /// </summary>
        public static readonly string[] CanViewFarms = { User.ADMIN_ROLE };

        /// <summary>
        /// Can view farm detail
        /// </summary>
        public static readonly string[] CanViewFarmDetail = { User.ADMIN_ROLE, User.MANAGER_ROLE };

        /// <summary>
        /// Can view and select farm dropdown
        /// </summary>
        public static readonly string[] CanViewFarmDropdown = { User.ADMIN_ROLE, User.MANAGER_ROLE };

        /// <summary>
        /// Can view farm detail
        /// </summary>
        public static readonly string[] CanAddFarm = { User.ADMIN_ROLE };

        /// <summary>
        /// Can update farm
        /// </summary>
        public static readonly string[] CanUpdateFarm = { User.ADMIN_ROLE, User.MANAGER_ROLE };

        /// <summary>
        /// Can delete farm
        /// </summary>
        public static readonly string[] CanDeleteFarm = { User.ADMIN_ROLE };

        /// <summary>
        /// Can view gateways
        /// </summary>
        public static readonly string[] CanViewGateways = { User.ADMIN_ROLE, User.MANAGER_ROLE };

        /// <summary>
        /// Can view farm by owner (for manager role only)
        /// </summary>
        public static readonly string[] CanViewFarmByOwner = { User.MANAGER_ROLE };

        /// <summary>
        /// 
        /// </summary>
        public static readonly string[] CanViewGatewaysByManager = { User.MANAGER_ROLE };

        /// <summary>
        /// 
        /// </summary>
        public static readonly string[] CanViewGatewayByManager = { User.MANAGER_ROLE };

        /// <summary>
        /// Can view gateway detail
        /// </summary>
        public static readonly string[] CanViewDetailGateway = { User.ADMIN_ROLE, User.MANAGER_ROLE };

        /// <summary>
        /// Can add gateway
        /// </summary>
        public static readonly string[] CanAddGateway = { User.ADMIN_ROLE, User.MANAGER_ROLE };

        /// <summary>
        /// Can update gateway
        /// </summary>
        public static readonly string[] CanUpdateGateway = { User.ADMIN_ROLE, User.MANAGER_ROLE };

        /// <summary>
        /// Can delete gateway
        /// </summary>
        public static readonly string[] CanDeleteGateway = { User.ADMIN_ROLE, User.MANAGER_ROLE };

        /// <summary>
        /// Can view sensor nodes
        /// </summary>
        public static readonly string[] CanViewSensorNodes = { User.ADMIN_ROLE, User.MANAGER_ROLE };

        /// <summary>
        /// Can view detail sensor node
        /// </summary>
        public static readonly string[] CanViewDetailSensorNode = { User.ADMIN_ROLE, User.MANAGER_ROLE };

        /// <summary>
        /// 
        /// </summary>
        public static readonly string[] CanViewSensorNodesByManager = { User.MANAGER_ROLE };

        /// <summary>
        /// Can view detail sensor node
        /// </summary>
        public static readonly string[] CanAddSensorNode = { User.ADMIN_ROLE, User.MANAGER_ROLE };

        /// <summary>
        /// Can update detail sensor node
        /// </summary>
        public static readonly string[] CanUpdateSensorNode = { User.ADMIN_ROLE, User.MANAGER_ROLE };

        /// <summary>
        /// Can delete sensor node
        /// </summary>
        public static readonly string[] CanDeleteSensorNode = { User.ADMIN_ROLE, User.MANAGER_ROLE };

        /// <summary>
        /// Can view statistic and review histories data
        /// </summary>
        public static readonly string[] CanViewStatisticsAndReviewHistories = { User.ADMIN_ROLE, User.MANAGER_ROLE };

        /// <summary>
        /// Can access profile page
        /// </summary>
        public static readonly string[] CanAccessProfile = { User.ADMIN_ROLE, User.MANAGER_ROLE };

        /// <summary>
        /// Can update profile page
        /// </summary>
        public static readonly string[] CanUpdateProfile = { User.ADMIN_ROLE, User.MANAGER_ROLE };

        /// <summary>
        /// Can get user dropdown
        /// </summary>
        public static readonly string[] CanGetUserDropdown = { User.ADMIN_ROLE, User.MANAGER_ROLE };

        /// <summary>
        /// Can get all users
        /// </summary>
        public static readonly string[] CanGetAllUsers = { User.ADMIN_ROLE, User.MANAGER_ROLE };

        /// <summary>
        /// Can get user detail
        /// </summary>
        public static readonly string[] CanGetUser = { User.ADMIN_ROLE, User.MANAGER_ROLE };

        /// <summary>
        /// 
        /// </summary>
        public static readonly string[] CanAddUser = { User.ADMIN_ROLE };

        /// <summary>
        /// 
        /// </summary>
        public static readonly string[] CanUpdateUser = { User.ADMIN_ROLE };

        /// <summary>
        /// 
        /// </summary>
        public static readonly string[] CanDeleteUser = { User.ADMIN_ROLE };

        /// <summary>
        /// 
        /// </summary>
        public static readonly string[] CanGetSensorNodeListDropdown = { User.ADMIN_ROLE, User.MANAGER_ROLE };

        /// <summary>
        /// 
        /// </summary>
        public static readonly string[] CanUpdateGatewaySettings = { User.ADMIN_ROLE };

        /// <summary>
        /// Get all privileges as string
        /// </summary>
        /// <param name="privilege"></param>
        /// <returns></returns>
        public static string[] GetPrivileges(SystemPrivileges privilege)
        {
            var classType = typeof(Privileges);
            var privName = privilege.ToString();
            var field = classType.GetField(privName, BindingFlags.Public | BindingFlags.Static);
            if (field == null) return new string[0];
            var fieldValues = field.GetValue(null) as IEnumerable<string> ?? Enumerable.Empty<string>();
            return fieldValues.ToArray();

        }

        /// <summary>
        /// Get privileges as common system SnakeCase instead of .NET PascalCase
        /// </summary>
        /// <param name="privileges"></param>
        /// <returns></returns>
        public static string[] GetPrivilegesAsSnakeCase(SystemPrivileges privileges)
        {
            var priv = GetPrivileges(privileges);
            if (priv.Length > 0) return priv;
            var listPriv = priv
                .Select(x => x.PascalCaseToSnakeCase())
                .ToArray();
            return listPriv;
        }
    }

    public enum SystemPrivileges
    {
        CanLoginToWeb,
        CanDoSystemResetPasswordForAdmin,
        CanDoSystemResetPasswordForManager,
        CanViewGateways,
        CanViewGatewaysByManager,
        CanViewGatewayByManager,
        CanViewDetailGateway,
        CanAddGateway,
        CanUpdateGateway,
        CanDeleteGateway,
        CanViewStatisticsAndReviewHistories,
        CanAccessProfile,
        CanUpdateProfile,
        CanGetUserDropdown,
        CanGetAllUsers,
        CanGetUser,
        CanAddUser,
        CanUpdateUser,
        CanDeleteUser,
        CanViewSensorNodes,
        CanViewSensorNodesByManager,
        CanViewDetailSensorNode,
        CanAddSensorNode,
        CanUpdateSensorNode,
        CanDeleteSensorNode,
        CanViewFarms,
        CanViewFarmDetail,
        CanViewFarmByOwner,
        CanAddFarm,
        CanViewFarmDropdown,
        CanUpdateFarm,
        CanDeleteFarm,
        CanGetSensorNodeListDropdown,
        CanUpdateGatewaySettings
    }
}
