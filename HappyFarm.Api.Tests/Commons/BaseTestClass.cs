﻿using Askmethat.Aspnet.JsonLocalizer.Localizer;
using HappyFarm.Api.Localizations;
using Moq;

namespace HappyFarm.Api.Tests.Commons
{
    public class BaseTestClass
    {
        protected Mock<IJsonStringLocalizer> MockLocalizer;

        public BaseTestClass()
        {
            MockLocalizer = new Mock<IJsonStringLocalizer>(new HFJsonStringLocalizer());
        }
    }
}
