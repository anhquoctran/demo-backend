using Askmethat.Aspnet.JsonLocalizer.Localizer;
using AutoMapper;
using FluentValidation;
using FluentValidation.AspNetCore;
using HappyFarm.Api.Filters;
using HappyFarm.Api.Localizations;
using HappyFarm.Api.Middlewares;
using HappyFarm.Core;
using HappyFarm.Core.Commons;
using HappyFarm.Core.UnitOfWork;
using HappyFarm.Infrastructure.Commons;
using HappyFarm.Infrastructure.MapperProfile;
using HappyFarm.Infrastructure.Services.Implementations;
using HappyFarm.Infrastructure.Services.Interfaces;
using HappyFarm.Infrastructure.Validations;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;

namespace HappyFarm.Api
{
    public class Startup
    {
        public Startup()
        {
            AppConfig.LoadConfig(GetAbsoluteEndpointPath());
            FileUtils.InitStaticFolder(EndpointPath);
        }

        private static readonly CultureInfo DefaultCultureInfo = new CultureInfo("vi-VN");

        private static readonly RequestCulture DefaultRequestCulture = new RequestCulture(DefaultCultureInfo);

        public static readonly HashSet<CultureInfo> SupportedCultures = new HashSet<CultureInfo>
        {
            new CultureInfo("en-US"),
            new CultureInfo("vi-VN")
        };

        public static readonly string EndpointPath = GetAbsoluteEndpointPath();

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers()
                .AddNewtonsoftJson();

            //services.AddSingleton<IAuthorizationHandler, HFAuthorizationHandler>();

            // services.AddAuthorization(options =>
            // {
            //     options.AddPolicy("HFAuthorizationPolicy", policy =>
            //     {
            //         policy.Requirements.Add(new HFAuthorizationRequirement());
            //     });
            // });

            services.AddApiVersioning(options => 
            {
                options.DefaultApiVersion = new ApiVersion(1, 0);
                options.AssumeDefaultVersionWhenUnspecified = true;
                options.ReportApiVersions = true;
            });

            SetupCors(services);

            // Auto Mapper Configurations
            // ReSharper disable once HeapView.ObjectAllocation.Evident
            var mapperConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile<Default>();
            });

            var mapper = mapperConfig.CreateMapper();
            services.AddSingleton(mapper);

            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
                options.SuppressMapClientErrors = true;
            });

            services.Configure<RequestLocalizationOptions>(options =>
            {
                var listSupportedCultures = SupportedCultures.ToList();
                options.DefaultRequestCulture = DefaultRequestCulture;
                options.SupportedCultures = listSupportedCultures;
                options.SupportedUICultures = listSupportedCultures;
            });

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddLocalization(options =>
            {
                options.ResourcesPath = Constants.RESOURCE_DIRECTORY_NAME;
                
            });

            services.AddMvc(options =>
            {
                options.Filters.Add<ValidationActionFilterAttribute>();
                options.OutputFormatters.Clear();
                options.OutputFormatters.Add(new NewtonsoftJsonOutputFormatter(new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore,

                }, ArrayPool<char>.Shared, options));

            })
            .SetCompatibilityVersion(CompatibilityVersion.Latest)
            .AddDataAnnotationsLocalization()
            .AddFluentValidation(fv =>
            {
                fv.RegisterValidatorsFromAssembly(Assembly.GetExecutingAssembly());
                ValidatorOptions.LanguageManager = new HFLanguageManager();
            });

            AddDbContextAndCoreDataAccessLayer(services);

            SetupFormUpload(services);

            SetupLogicServices(services);

            SetupLocalization(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseRouting();

            app.UseCors("default");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                HandleError(app);
            }

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                
            });

            CultureInfo.DefaultThreadCurrentCulture = DefaultCultureInfo;
            CultureInfo.DefaultThreadCurrentUICulture = DefaultCultureInfo;

            app.UseHttpExceptionMiddleware();

            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(EndpointPath, Constants.STATIC_DIRECTORY_NAME)),
                RequestPath = new PathString(Constants.STATIC_PATH)
            });

            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                DefaultRequestCulture = DefaultRequestCulture,
                SupportedCultures = SupportedCultures.ToList(),
                SupportedUICultures = SupportedCultures.ToList()
            });

            //app.UseHubRequireAuthorizationMiddleware();

            if (!env.IsProduction() && !env.IsStaging()) return;
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            app.Use((context, next) =>
            {
                foreach(var header in Constants.UntrustedHeaders)
                {
                    context.Response.Headers.Remove(header);
                }

                foreach(var (key, value) in Constants.SecurityHeaders)
                {
                    context.Response.Headers[key] = value;
                }

                return next();
            });


        }

        private static void HandleError(IApplicationBuilder app)
        {
            app.UseExceptionHandler(errorApp =>
            {
                errorApp.Run(async context =>
                {
                    context.Response.ContentType = "application/json";
                    context.Response.Headers["Accept"] = "application/json";

                    if (context.Response.StatusCode >= (int)HttpStatusCode.InternalServerError)
                    {
                        var httpStatus = (HttpStatusCode)context.Response.StatusCode;
                        var json = ResponseUtils.GetJsonResponseString(httpStatus);
                        context.Response.Clear();
                        await context.Response.WriteAsync(json, Encoding.UTF8);
                    }

                });
            });

        }

        private static void SetupLocalization(IServiceCollection services)
        {
            services.AddSingleton<IStringLocalizerFactory, HFJsonStringLocalizerFactory>();
            services.AddSingleton<IJsonStringLocalizer, HFJsonStringLocalizer>();
        }

        private static void SetupLogicServices(IServiceCollection services)
        {
            services.AddSingleton<IDistributedService, DistributedService>();
            services.AddSingleton<IMessageQueueService, MessageQueueService>();
            services.AddScoped<IAuthService, AuthService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IFarmService, FarmService>();
            services.AddScoped<IGatewayService, GatewayService>();
            services.AddScoped<ISensorNodeService, SensorNodeService>();
            services.AddScoped<ISensorValueService, SensorValueService>();
        }

        private static void SetupCors(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("default",
                    builder =>
                    {
                        builder.AllowAnyMethod()
                            .AllowAnyHeader()
                            .SetIsOriginAllowed(x => true)
                            .AllowCredentials()
                            .WithOrigins(Constants.AllowedOrigins)
                            .SetIsOriginAllowedToAllowWildcardSubdomains();
                    });
            });

        }

        private static void SetupFormUpload(IServiceCollection services)
        {
            services.Configure<FormOptions>(options =>
            {
                options.MultipartBodyLengthLimit = (long)Constants.MAXIMUM_FILE_LENGTH_ALLOWED;
                options.BufferBodyLengthLimit = (long)Constants.MAXIMUM_FILE_LENGTH_ALLOWED;
                options.BufferBody = true;
            });

            services.Configure<IISServerOptions>(options =>
            {
                options.MaxRequestBodySize = (long)Constants.MAXIMUM_FILE_LENGTH_ALLOWED;
                options.AllowSynchronousIO = true;
            });

            services.Configure<KestrelServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
                options.Limits.MaxRequestBufferSize = (long)Constants.MAXIMUM_FILE_LENGTH_ALLOWED;
                options.Limits.MaxRequestBodySize = (long)Constants.MAXIMUM_FILE_LENGTH_ALLOWED;
            });
        }

        private static void AddDbContextAndCoreDataAccessLayer(IServiceCollection services)
        {
            services.AddDbContext<HappyFarmDbContext>(options =>
            {
                options
                .UseMySql(AppConfig.Config.Database.ConnectionString, option => 
                {
                    option.EnableRetryOnFailure();
                })
                .UseLazyLoadingProxies(true);
            }, ServiceLifetime.Singleton);

            services.AddSingleton<IUnitOfWork, UnitOfWork>();
        }

        private static string GetAbsoluteEndpointPath()
        {
            var asmPath = new Uri(Assembly.GetExecutingAssembly().CodeBase).AbsolutePath;
            var executePath = Path.GetDirectoryName(asmPath) ?? string.Empty;
            return executePath;
        }
    }
}
