﻿using Askmethat.Aspnet.JsonLocalizer.Localizer;
using HappyFarm.Api.Filters;
using HappyFarm.Api.Models;
using HappyFarm.Api.Models.User;
using HappyFarm.Core.Commons;
using HappyFarm.Core.Enums;
using HappyFarm.Infrastructure.Commons;
using HappyFarm.Infrastructure.Dto;
using HappyFarm.Infrastructure.Permissions;
using HappyFarm.Infrastructure.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HappyFarm.Api.Controllers
{
    [Route("users"), ApiController, ApiVersion("1.0"), HFAuthorize]
    public class UserController : BaseController
    {
        private readonly IUserService _userService;
        private readonly IFarmService _farmService;

        public UserController(
            IUserService userService,
            IJsonStringLocalizer localizer,
            IFarmService farmService
        )
            : base(localizer)
        {
            _userService = userService;
            _farmService = farmService;
        }

        [HttpGet, HfAclAuthorize(SystemPrivileges.CanGetAllUsers)]
        public async Task<IActionResult> ListAllUser([FromQuery] ListUserQueryViewModel queryModel)
        {
            var user = HttpContext.GetAuthorizedUser();
            var users = await _userService.GetUserListAsync(queryModel.keyword, queryModel.role, user.Role, queryModel.page, queryModel.page_size);
            return users != null ? SucceedPagination(users) : ServerError();
        }

        [HttpGet("{id:int}"), HfAclAuthorize(SystemPrivileges.CanGetAllUsers)]
        public async Task<IActionResult> GetUser([FromRoute] int userId)
        {
            var user = HttpContext.GetAuthorizedUser();
            int? farmId = null;
            if (user.Role == Core.Models.User.ADMIN_ROLE)
            {
                farmId = null;
            }
            else
            {
                var userFarm = await _userService.GetCurrentFarmIdOfUserAsync(user.Id);
                switch (userFarm.Key)
                {
                    case OperationStatus.Succeed:
                        farmId = userFarm.Value;
                        break;
                    case OperationStatus.Exception:
                        return ServerError();
                    case OperationStatus.NotFound:
                        return SendBadRequestCommon(Localizer["hf.messages.invalid_farm_of_manager"].Value);
                }
            }

            var users = await _userService.GetDetailUserAsync(userId, farmId);
            return users != null ? Succeed(users) : ServerError();
        }

        [HttpPost, HfAclAuthorize(SystemPrivileges.CanAddUser)]
        public async Task<IActionResult> AddUser([FromForm] CreateUserViewModel viewModel)
        {
            var dictErrors = await ValidateUser(viewModel.Username, viewModel.Email, viewModel.Role, viewModel.FarmId, null);

            if (dictErrors.Count > 0)
            {
                return SendBadRequest(dictErrors);
            }

            string avatarUrl = null;
            if (viewModel.Avatar != null)
            {
                avatarUrl = ImageUtils.ProcessAndSaveImage(viewModel.Avatar, Startup.EndpointPath);
            }

            var userDto = new UserDto
            {
                Firstname = viewModel.Firstname.Trim(),
                Lastname = viewModel.Lastname.Trim(),
                Email = viewModel.Email.Trim(),
                Password = viewModel.Password.Trim(),
                Role = viewModel.Role.Trim(),
                Username = viewModel.Username.Trim(),
                FarmId = viewModel.FarmId,
                Avatar = avatarUrl
            };

            var created = await _userService.CreateUser(userDto);

            return created switch
            {
                OperationStatus.Succeed => Succeed(),
                _ => ServerError(),
            };
        }

        [HttpPut("{id:int}"), HfAclAuthorize(SystemPrivileges.CanUpdateUser)]
        public async Task<IActionResult> UpdateUser([FromRoute] int id, [FromForm] CreateUserViewModel viewModel)
        {
            var dictErrors = await ValidateUser(viewModel.Username, viewModel.Email, null, null, id);

            if (dictErrors.Count > 0)
            {
                return SendBadRequest(dictErrors);
            }

            var checkExists = await _userService.IsUserExist(id, viewModel.Role);

            if (checkExists == OperationStatus.Exception)
            {
                return ServerError();
            }
            else if (checkExists == OperationStatus.NotFound)
            {
                return SendNotFound();
            }
            else
            {
                //string avatarUrl = null;

                //if (viewModel.Avatar != null)
                //{
                //    var (status, imagePath) = await _userService.TryGetUserAvatar(id, Startup.EndpointPath);

                //    switch (status)
                //    {
                //        case OperationStatus.Succeed:
                //            if (!StringUtils.CheckIsEmptyOrWhitespaceString(imagePath))
                //            {
                //                FileUtils.DeleteImageResources(Startup.EndpointPath, imagePath);
                //            }

                //            avatarUrl = ImageUtils.ProcessAndSaveImage(viewModel.Avatar, Startup.EndpointPath, null);
                //            break;
                //        case OperationStatus.NotFound:
                //            return SendNotFound();
                //        case OperationStatus.Exception:
                //        default:
                //            return ServerError();
                //    }
                //}
            }

            var userDto = new UserDto
            {
                Id = id,
                Firstname = viewModel.Firstname.Trim(),
                Lastname = viewModel.Lastname.Trim(),
                Email = viewModel.Email.Trim()
            };

            var created = await _userService.UpdateUser(userDto);

            return created switch
            {
                OperationStatus.Succeed => Succeed(),
                _ => ServerError(),
            };
        }

        [HttpDelete, HfAclAuthorize(SystemPrivileges.CanDeleteUser)]
        public async Task<IActionResult> DeleteUser([FromQuery] ListIdDeletionViewModel viewModel)
        {
            var list = viewModel.ListId
                .Where(x => x.HasValue && x > 0)
                .Select(x => x.GetValueOrDefault());
            var deleted = await _userService.DeleleUsers(list);

            return deleted switch
            {
                OperationStatus.Succeed => Succeed(),
                OperationStatus.Failed => SendBadRequestCommon(Localizer["hf.messages.failed_delete_user"].Value),
                _ => ServerError()
            };
        }

        private async Task<IDictionary<string, string[]>> ValidateUser(string username, string email, string role, int? farm_id, int? id)
        {
            var dictErrors = new Dictionary<string, string[]>();

            if (!StringUtils.CheckIsEmptyOrWhitespaceString(role) && role == Core.Models.User.MANAGER_ROLE)
            {
                var farmExist = await _farmService.CheckExistsBy(x => x.Id == farm_id.Value);
                if (farmExist != OperationStatus.Succeed)
                {
                    dictErrors.Add(nameof(farm_id), new[] { Localizer["hf.messages.farm_not_exists"].Value });
                }
            }

            if (id.HasValue && id > 0)
            {
                if (!StringUtils.CheckIsEmptyOrWhitespaceString(username))
                {
                    username = username.Trim().ToLower();
                    if (await _userService.ExistsByAsync(x => x.Id != id && x.Username.Trim().ToLower() == username))
                    {
                        dictErrors.Add(nameof(username), new[] { Localizer["hf.messages.username_duplicated"].Value });
                    }
                }

                email = email.Trim().ToLower();
                if (await _userService.ExistsByAsync(x => x.Id != id && x.Email.Trim().ToLower() == email))
                {
                    dictErrors.Add(nameof(email), new[] { Localizer["hf.messages.email_duplicated"].Value });
                }
            }
            else
            {
                if (!StringUtils.CheckIsEmptyOrWhitespaceString(username))
                {
                    username = username.Trim().ToLower();
                    if (await _userService.ExistsByAsync(x => x.Username.Trim().ToLower() == username))
                    {
                        dictErrors.Add(nameof(username), new[] { Localizer["hf.messages.username_duplicated"].Value });
                    }
                }

                email = email.Trim().ToLower();
                if (await _userService.ExistsByAsync(x => x.Email.Trim().ToLower() == email))
                {
                    dictErrors.Add(nameof(email), new[] { Localizer["hf.messages.email_duplicated"].Value });
                }
            }

            return dictErrors;
        }
    }
}
