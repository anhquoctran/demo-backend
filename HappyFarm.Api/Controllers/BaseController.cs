﻿using Askmethat.Aspnet.JsonLocalizer.Localizer;
using HappyFarm.Api.Models.Responses;
using HappyFarm.Infrastructure.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;

namespace HappyFarm.Api.Controllers
{
    public abstract class BaseController : Controller
    {
        protected readonly IJsonStringLocalizer Localizer;

        protected BaseController(IJsonStringLocalizer localizer)
        {
            Localizer = localizer;
        }

        protected IActionResult OkOrFail(bool isOk)
        {
            return isOk ? Ok(new SucceedResponse()) : ServerError();
        }

        /// <summary>
        /// Return 500 Internal Server Error Common Response
        /// </summary>
        /// <returns></returns>
        protected IActionResult ServerError()
        {
            var response = new ServerErrorResponse();
            return StatusCode((int)HttpStatusCode.InternalServerError, response);
        }

        /// <summary>
        /// Return 200 Success Common Response with data
        /// </summary>
        /// <returns></returns>
        protected IActionResult Succeed(object data = null)
        {
            return Ok(new SucceedResponse<object>(data));
        }

        protected IActionResult SucceedPagination<T>(PaginatedList<T> result) where T : class
        {
            var paginationData = new PaginationData<T>(result.PageIndex, result.TotalPages, result.TotalItems, result.PageSize, result);
            return Ok(new PaginationSucceedRespponse<T>(paginationData));
        }

        protected IActionResult SendBadRequest(object errors = null)
        {
            return BadRequest(new BadRequestResponse<object>(errors));
        }

        protected IActionResult SendBadRequestCommon(string errorList)
        {
            var errors = new Dictionary<string, string>
            {
                { "common", errorList }
            };
            return SendBadRequest(errors);
        }

        protected IActionResult SendCustomResponse(int status, string errorList)
        {
            var ob = new ObjectResult(new BaseApiResponse(new { common = new[] { errorList } }, "Bad Request", status))
            {
                StatusCode = status
            };
            return ob;
        }

        protected IActionResult SendBadRequestCommon(string error, ResponseErrorFlag flag = ResponseErrorFlag.Other)
        {
            var errors = new Dictionary<string, object>
            {
                { "msg", new[] { error } },
                { "errcode", (int) flag }
            };
            return SendBadRequest(errors);
        }

        protected IActionResult SendBadRequestCommon(string[] errorList)
        {
            var errors = new Dictionary<string, string[]>
            {
                { "common", errorList }
            };
            return SendBadRequest(errors);
        }

        protected IActionResult SendNotFound()
        {
            return NotFound(new NotFoundResponse());
        }

        protected IActionResult SendForbidden()
        {
            return StatusCode((int)HttpStatusCode.Forbidden, new ForbiddenResponse());
        }

        protected IActionResult SendUnauthorized()
        {
            return StatusCode((int)HttpStatusCode.Unauthorized, new UnauthorizedResponse());
        }
    }
}
