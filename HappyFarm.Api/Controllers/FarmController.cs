﻿using Askmethat.Aspnet.JsonLocalizer.Localizer;
using HappyFarm.Api.Filters;
using HappyFarm.Api.Models;
using HappyFarm.Api.Models.Farm;
using HappyFarm.Core.Enums;
using HappyFarm.Infrastructure.Commons;
using HappyFarm.Infrastructure.Dto;
using HappyFarm.Infrastructure.Permissions;
using HappyFarm.Infrastructure.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HappyFarm.Api.Controllers
{
    [ApiController, Route("farms"), ApiVersion("1.0"), HFAuthorize]
    public class FarmController : BaseController
    {
        private readonly IFarmService _farmService;
        private readonly IUserService _userService;

        public FarmController(
            IFarmService farmService,
            IUserService userService,
            IJsonStringLocalizer localizer
            )
            : base(localizer)
        {
            _farmService = farmService;
            _userService = userService;
        }

        [HttpGet, HfAclAuthorize(SystemPrivileges.CanViewFarms)]
        public async Task<IActionResult> GetAllFarms([FromQuery] ListFarmQueryViewModel viewModel)
        {
            var farms = await _farmService.GetAllFarms(viewModel.Keyword, viewModel.Page, viewModel.PageSize);
            return SucceedPagination(farms);
        }

        [HttpGet("{id:int}"), HfAclAuthorize(SystemPrivileges.CanViewFarmDetail)]
        public async Task<IActionResult> GetFarm([FromRoute] int id)
        {
            var farm = await _farmService.GetFarm(id);
            return farm == null ? SendNotFound() : Succeed(farm);
        }

        [HttpGet("by-manager"), HfAclAuthorize(SystemPrivileges.CanViewFarmByOwner)]
        public async Task<IActionResult> GetFarmByUser()
        {
            var user = HttpContext.GetAuthorizedUser();
            var farm = await _farmService.GetFarmByUser(user.Id);
            return farm != null ? Succeed(farm) : ServerError();
        }

        [HttpGet("dropdown"), HfAclAuthorize(SystemPrivileges.CanViewFarmDropdown)]
        public async Task<IActionResult> GetFarmDropdown()
        {
            var user = HttpContext.GetAuthorizedUser();
            int? farmId = null;
            if (user.Role != Core.Models.User.ADMIN_ROLE)
            {
                var (operationStatus, id) = await _userService.GetCurrentFarmIdOfUserAsync(user.Id);
                switch (operationStatus)
                {
                    case OperationStatus.Succeed:
                        farmId = id;
                        break;
                    case OperationStatus.Exception:
                        return ServerError();
                    case OperationStatus.NotFound:
                        return SendBadRequestCommon(Localizer["hf.messages.invalid_farm_of_manager"].Value);
                }
            }
            var farms = await _farmService.GetDropdownFarms(farmId);
            return farms != null ? Succeed(farms) : ServerError();
        }

        [HttpPost, HfAclAuthorize(SystemPrivileges.CanAddFarm)]
        public async Task<IActionResult> CreateFarm([FromBody] FarmCreationViewModel viewModel)
        {
            var dictErrors = new Dictionary<string, string[]>();

            if (await _farmService.ExistsByAsync(x => x.Name.Trim().ToLower() == viewModel.Name.Trim().ToLower()))
            {
                dictErrors.Add("name", new[] { Localizer["hf.messages.farm_name_duplicated"].Value });
            }

            if (dictErrors.Count > 0)
            {
                return SendBadRequest(dictErrors);
            }

            var result = await _farmService.CreateFarm(new FarmDto
            {
                Name = viewModel.Name,
                Address = viewModel.Address,
                Phone = viewModel.Phone,
                RegionCode = viewModel.RegionCode
            });

            return OkOrFail(result);

        }

        [HttpPut("{id:int}"), HfAclAuthorize(SystemPrivileges.CanUpdateFarm)]
        public async Task<IActionResult> UpdateFarm([FromRoute] int id, [FromBody] FarmUpdateViewModel viewModel)
        {
            var dictErrors = new Dictionary<string, string[]>();

            if (await _farmService.ExistsByAsync(x => x.Id != id && x.Name.Trim().ToLower() == viewModel.Name.Trim().ToLower()))
            {
                dictErrors.Add("name", new[] { Localizer["hf.messages.farm_name_duplicated"].Value });
            }

            if (dictErrors.Count > 0)
            {
                return SendBadRequest(dictErrors);
            }

            var user = HttpContext.GetAuthorizedUser();
            if (user.Role == Core.Models.User.MANAGER_ROLE)
            {
                var userFarm = await _userService.GetCurrentFarmIdOfUserAsync(user.Id);

                if (userFarm.Key == OperationStatus.Succeed)
                {
                    if (userFarm.Value != id)
                    {
                        return SendForbidden();
                    }
                }
                else if (userFarm.Key == OperationStatus.NotFound)
                {
                    return SendBadRequestCommon(Localizer["hf.messages.invalid_farm_of_manager"].Value);
                }
                else
                {
                    return ServerError();
                }
            }

            var result = await _farmService.UpdateFarm(new FarmDto
            {
                Id = id,
                Name = viewModel.Name,
                Address = viewModel.Address,
                Phone = viewModel.Phone,
                RegionCode = viewModel.RegionCode
            });

            return OkOrFail(result);
        }

        [HttpDelete, HfAclAuthorize(SystemPrivileges.CanDeleteFarm)]
        public async Task<IActionResult> DeleteFarms([FromQuery] ListIdDeletionViewModel viewModel)
        {
            var result = await _farmService.DeleteFarms(viewModel.ListId);
            return result switch
            {
                OperationStatus.Succeed => Succeed(),
                OperationStatus.Failed => SendBadRequestCommon(Localizer["hf.messages.farm_contains_dependends"].Value),
                _ => ServerError(),
            };
        }
    }
}
