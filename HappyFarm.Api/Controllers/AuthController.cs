﻿using Askmethat.Aspnet.JsonLocalizer.Localizer;
using HappyFarm.Api.Filters;
using HappyFarm.Api.Models.User;
using HappyFarm.Core.Commons;
using HappyFarm.Core.Enums;
using HappyFarm.Infrastructure.Commons;
using HappyFarm.Infrastructure.Dto;
using HappyFarm.Infrastructure.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HappyFarm.Api.Controllers
{
    [ApiController, Route("auth"), ApiVersion("1.0")]
    public class AuthController : BaseController
    {
        private readonly IAuthService _authService;
        private readonly IUserService _userService;

        public AuthController(
            IAuthService authService,
            IJsonStringLocalizer localizer,
            IUserService userService
        )
            : base(localizer)
        {
            _authService = authService;
            _userService = userService;
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody]LoginViewModel model)
        {
            var auth = await _authService.Authenticate(model.Username, model.Password);

            return auth.Key switch
            {
                AuthResult.Success => Succeed(
                    new AuthUserViewModel 
                    { 
                        Id = auth.Value.Id, 
                        Avatar = auth.Value.Avatar, 
                        Role = auth.Value.Role,
                        Token = auth.Value.Token,
                        Permissions = auth.Value.Permissions
                    }),
                AuthResult.Failed => SendBadRequestCommon(Localizer["hf.messages.failed_to_login"].Value),
                AuthResult.ExceptionFailed => ServerError(),
                _ => ServerError(),
            };
        }

        [HttpGet("profile"), HFAuthorize]
        public async Task<IActionResult> GetProfile()
        {
            var user = HttpContext.GetAuthorizedUser();
            var auth = await _authService.GetUserProfile(user.Id);

            if (auth != null)
            {
                var profile = new ProfileViewModel
                {
                    Id = auth.Id,
                    FirstName = auth.Firstname,
                    LastName = auth.Lastname,
                    Avatar = auth.Avatar,
                    Email = auth.Email,
                    Role = new[] { auth.Role },
                    Username = auth.Username,
                    FarmId = auth.FarmId,
                    Permissions = _authService.GetPermissions(auth.Role)
                };

                return Succeed(profile);
            }

            return ServerError();

        }

        [HttpPut("profile"), HFAuthorize]
        public async Task<IActionResult> UpdateProfile([FromForm] ProfileUpdateViewModel viewModel)
        {
            var user = HttpContext.GetAuthorizedUser();
            var dict = new Dictionary<string, string>();

            var email = viewModel.Email.Trim().ToLower();
            if (await _userService.ExistsByAsync(x => x.Id != user.Id && x.Email.Trim().ToLower() == email))
            {
                dict.Add(nameof(email), Localizer["hf.messages.email_duplicated"].Value);
            }
            
            if (dict.Count > 0)
            {
                return SendBadRequest(dict);
            }

            string avatarUrl = null;

            if (viewModel.Avatar != null)
            {
                var (status, imagePath) = await _userService.TryGetUserAvatar(user.Id, Startup.EndpointPath);

                switch (status)
                {
                    case OperationStatus.Succeed:
                        if (!StringUtils.CheckIsEmptyOrWhitespaceString(imagePath))
                        {
                            FileUtils.DeleteImageResources(Startup.EndpointPath, imagePath);
                        }

                        avatarUrl = ImageUtils.ProcessAndSaveImage(viewModel.Avatar, Startup.EndpointPath);
                        break;
                    case OperationStatus.NotFound:
                        return SendNotFound();
                    default:
                        return ServerError();
                }
            }

            var result = await _authService.UpdateProfile(new UserDto
            {
                Id = user.Id,
                Firstname = viewModel.FirstName,
                Lastname = viewModel.LastName,
                Email = viewModel.Email,
                Avatar = avatarUrl
            });

            return result switch
            {
                OperationStatus.Succeed => Succeed(),
                OperationStatus.Failed => SendBadRequestCommon(new string[] { Localizer["hf.messages.failed_update_profile"] }),
                OperationStatus.Exception => ServerError(),
                _ => ServerError()
            };
        }

        [HttpPut("profile/change-password"), HFAuthorize]
        public async Task<IActionResult> UpdatePassword([FromForm] PasswordChangeViewModel viewModel)
        {
            var user = HttpContext.GetAuthorizedUser();
            var result = await _authService.UpdatePassword(user.Id, new ChangePasswordDto
            {
                OldPassword = viewModel.CurrentPassword,
                NewPassword = viewModel.Password,
                RepeatNewPassword = viewModel.PasswordConfirmation
            });

            return result switch
            {
                OperationStatus.Succeed => Succeed(),
                OperationStatus.Failed => SendBadRequestCommon(new string[] { Localizer["hf.messages.failed_update_password"] }),
                OperationStatus.Exception => ServerError(),
                _ => ServerError()
            };
        }

        [HttpGet("authorize")]
        public async Task<IActionResult> Authorize([FromQuery] string jwtToken)
        {
            var authorizeResult = await _authService.Authorize(jwtToken);

            return authorizeResult switch
            {
                OperationStatus.Succeed => Succeed(),
                OperationStatus.NotAuthorized => SendUnauthorized(),
                OperationStatus.Exception => ServerError(),
                _ => ServerError(),
            };
        }
    }
}
