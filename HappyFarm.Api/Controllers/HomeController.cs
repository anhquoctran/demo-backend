﻿using Askmethat.Aspnet.JsonLocalizer.Localizer;
using Microsoft.AspNetCore.Mvc;

namespace HappyFarm.Api.Controllers
{
    [ApiController, Route("")]
    public class HomeController: BaseController
    {
        public HomeController(IJsonStringLocalizer localizer) : base(localizer) { }

        public IActionResult Index()
        {
            
            return Succeed();
        }
    }
}
