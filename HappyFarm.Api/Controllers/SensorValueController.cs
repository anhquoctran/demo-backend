﻿using Askmethat.Aspnet.JsonLocalizer.Localizer;
using HappyFarm.Api.Filters;
using HappyFarm.Api.Models;
using HappyFarm.Core.Enums;
using HappyFarm.Infrastructure.Commons;
using HappyFarm.Infrastructure.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using UserModel = HappyFarm.Core.Models.User;

namespace HappyFarm.Api.Controllers
{
    [ApiController, Route("sensor-values"), HFAuthorize, ApiVersion("1.0")]
    public class SensorValueController : BaseController
    {
        private readonly ISensorValueService _sensorValueService;
        private readonly IUserService _userService;

        public SensorValueController(
            ISensorValueService sensorValueService,
            IJsonStringLocalizer localizer,
            IUserService userService
        ) : base(localizer)
        {
            _sensorValueService = sensorValueService;
            _userService = userService ;
        }

        [HttpGet("history")]
        public async Task<IActionResult> GetSensorValuesList([FromQuery] SensorValueQueryViewModel query)
        {
            var user = HttpContext.GetAuthorizedUser();

            if (user.Role == UserModel.MANAGER_ROLE)
            {
                var userFarm = await _userService.GetCurrentFarmIdOfUserAsync(user.Id);
                if (userFarm.Key == OperationStatus.Exception) return ServerError();
                if (userFarm.Key == OperationStatus.NotFound) return SendNotFound();
                query.FarmId = userFarm.Value.GetValueOrDefault();
            }

            var result = await _sensorValueService.GetListOfSensorValues(query.From, query.To, query.FarmId, query.GatewayId, query.NodeId, query.Page, query.PageSize);
            if (result != null) return SucceedPagination(result);

            return ServerError();
        }
    }
}
