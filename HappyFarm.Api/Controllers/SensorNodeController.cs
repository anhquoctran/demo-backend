﻿using Askmethat.Aspnet.JsonLocalizer.Localizer;
using HappyFarm.Api.Filters;
using HappyFarm.Api.Models;
using HappyFarm.Api.Models.SensorNode;
using HappyFarm.Core.Enums;
using HappyFarm.Infrastructure.Commons;
using HappyFarm.Infrastructure.Dto;
using HappyFarm.Infrastructure.Permissions;
using HappyFarm.Infrastructure.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserModel = HappyFarm.Core.Models.User;

namespace HappyFarm.Api.Controllers
{
    [ApiController, Route("sensor-nodes"), ApiVersion("1.0"), HFAuthorize]
    public class SensorNodeController : BaseController
    {
        private readonly ISensorNodeService _sensorNodeService;
        private readonly IUserService _userService;
        private readonly IFarmService _farmService;
        private readonly IGatewayService _gatewayService;

        public SensorNodeController(
            ISensorNodeService sensorNodeService,
            IUserService userService,
            IFarmService farmService,
            IGatewayService gatewayService,
            IJsonStringLocalizer localizer
            )
            : base(localizer)
        {
            _sensorNodeService = sensorNodeService;
            _userService = userService;
            _farmService = farmService;
            _gatewayService = gatewayService;
        }

        [HttpGet("by-gateway/{gwId:int}"), HfAclAuthorize(SystemPrivileges.CanViewSensorNodes)]
        public async Task<IActionResult> GetSensorNodeByGateway([FromRoute] int gwId)
        {
            var gatewayExists = await _gatewayService.IsExistById(gwId);

            if (gatewayExists == OperationStatus.NotFound)
            {
                return SendNotFound();
            }

            if (gatewayExists == OperationStatus.Exception)
            {
                return ServerError();
            }

            var user = HttpContext.GetAuthorizedUser();

            if (user.Role == UserModel.MANAGER_ROLE)
            {
                var gateway = await _gatewayService.GetGateway(gwId);
                var userFarm = await _userService.GetCurrentFarmIdOfUserAsync(user.Id);
                if (userFarm.Key == OperationStatus.Succeed && userFarm.Value.HasValue)
                {
                    if (userFarm.Value.Value != gateway.FarmId)
                    {
                        return SendForbidden();
                    }
                }
                else
                {
                    return ServerError();
                }
            }

            var result = await _sensorNodeService.GetSensorsByGateway(gwId);
            return result != null ? Succeed(result) : ServerError();

        }

        [HttpGet, HFAuthorize, HfAclAuthorize(SystemPrivileges.CanViewSensorNodes)]
        public async Task<IActionResult> GetSensorNodes([FromQuery] SensorQuery sensorQuery)
        {
            var user = HttpContext.GetAuthorizedUser();
            int? farmId = null;
            int? gwId;
            if (user.Role == UserModel.ADMIN_ROLE)
            {
                gwId = sensorQuery.GatewayId;
                farmId = null;
            }
            else
            {
                var farmOfManager = await _userService.GetCurrentFarmIdOfUserAsync(user.Id);
                switch (farmOfManager.Key)
                {
                    case OperationStatus.Succeed:
                        var listId = await _gatewayService.GetListGatewayIdByFarmId(farmOfManager.Value.GetValueOrDefault());
                        if (listId.Contains(sensorQuery.GatewayId.GetValueOrDefault()))
                        {
                            gwId = sensorQuery.GatewayId;
                        }
                        else
                        {
                            gwId = null;
                            farmId = farmOfManager.Value.GetValueOrDefault();
                        }
                        break;
                    case OperationStatus.NotFound:
                        return SendForbidden();
                    case OperationStatus.Exception:
                    default:
                        return ServerError();
                }
            }

            var result = await _sensorNodeService.GetSensorsList(sensorQuery.Keyword, farmId, gwId, sensorQuery.Page, sensorQuery.PageSize);
            return result != null ? SucceedPagination(result) : ServerError();
        }

        [HttpGet("{id:int}"), HFAuthorize, HfAclAuthorize(SystemPrivileges.CanViewDetailSensorNode)]
        public async Task<IActionResult> GetSensorNode([FromRoute] int id)
        {
            var exists = await _sensorNodeService.IsExistsById(id);
            if (exists == OperationStatus.Exception)
            {
                return ServerError();
            }
            else if (exists == OperationStatus.NotFound)
            {
                return SendNotFound();
            }
            else
            {
                var user = HttpContext.GetAuthorizedUser();

                if (user.Role == UserModel.MANAGER_ROLE)
                {
                    var checkSame = await _sensorNodeService.IsUserSensorNodeHasSameFarm(id, user.Id);

                    if (checkSame == OperationStatus.Exception) return ServerError();
                    else if (checkSame == OperationStatus.Incorrect) return SendForbidden();
                }

                var result = await _sensorNodeService.GetSensorNode(id);
                if (result != null)
                {
                    return Succeed(result);
                }
                else
                {
                    return ServerError();
                }
            }
        }

        [HttpGet("sensors-info/{gatewayId:int}"), HfAclAuthorize(SystemPrivileges.CanGetSensorNodeListDropdown)]
        public async Task<IActionResult> GetSensorNodeListId([FromRoute] int gatewayId)
        {
            var user = HttpContext.GetAuthorizedUser();
            
            if (user.Role == UserModel.MANAGER_ROLE)
            {
                var gateway = await _gatewayService.GetGateway(gatewayId);
                var userFarm = await _userService.GetCurrentFarmIdOfUserAsync(user.Id);
                if (userFarm.Key == OperationStatus.Succeed && userFarm.Value.HasValue)
                {
                    if (userFarm.Value.Value != gateway.FarmId)
                    {
                        return SendForbidden();
                    }
                }
                else
                {
                    return ServerError();
                }
            }

            var result = await _sensorNodeService.GetSensorsDropdownByGateway(gatewayId);
            return result != null ? Succeed(result) : ServerError();
        }

        [HttpPost, HfAclAuthorize(SystemPrivileges.CanAddSensorNode)]
        public async Task<IActionResult> CreateNode([FromBody] SensorNodeCreationViewModel viewModel)
        {
            var currentUser = HttpContext.GetAuthorizedUser();

            var isGatewayExist = await _gatewayService.IsExistById(viewModel.GatewayId.Value);

            if (isGatewayExist == OperationStatus.Succeed)
            {
                if (currentUser.Role == UserModel.MANAGER_ROLE)
                {
                    var gateway = await _gatewayService.GetGateway(viewModel.GatewayId.Value);
                    var userFarm = await _userService.GetCurrentFarmIdOfUserAsync(currentUser.Id);
                    if (userFarm.Key == OperationStatus.Succeed && userFarm.Value.HasValue)
                    {
                        if (userFarm.Value.Value != gateway.FarmId)
                        {
                            return SendForbidden();
                        }
                    }
                    else
                    {
                        return ServerError();
                    }
                }

                viewModel.Name = viewModel.Name.Trim();
                viewModel.Code = viewModel.Code.Trim();

                var errors = await ValidateSensorNode(viewModel.Name, viewModel.Code, null);

                if (errors.Count > 0)
                {
                    return SendBadRequest(errors);
                }

                var result = await _sensorNodeService.AddSensorNode(
                    new SensorNodeDto
                    {
                        Name = viewModel.Name,
                        Code = viewModel.Code,
                        GatewayId = viewModel.GatewayId
                    }
                );
                return OkOrFail(result == OperationStatus.Succeed);
            }

            if (isGatewayExist == OperationStatus.NotFound)
            {
                return SendNotFound();
            }

            return ServerError();
        }

        [HttpPut("{id:int}"), HfAclAuthorize(SystemPrivileges.CanUpdateSensorNode)]
        public async Task<IActionResult> UpdateNode([FromRoute] int id, [FromBody] SensorNodeCreationViewModel viewModel)
        {
            var currentUser = HttpContext.GetAuthorizedUser();

            var isGatewayExist = await _gatewayService.IsExistById(viewModel.GatewayId.Value);

            if (isGatewayExist == OperationStatus.Succeed)
            {
                if (currentUser.Role == UserModel.MANAGER_ROLE)
                {

                    if (currentUser.Role == UserModel.MANAGER_ROLE)
                    {
                        var checkSame = await _sensorNodeService.IsUserSensorNodeHasSameFarm(id, currentUser.Id);

                        if (checkSame == OperationStatus.Exception) return ServerError();
                        if (checkSame == OperationStatus.Incorrect) return SendForbidden();
                    }
                }

                viewModel.Name = viewModel.Name.Trim();
                viewModel.Code = viewModel.Code.Trim();

                var errors = await ValidateSensorNode(viewModel.Name, viewModel.Code, id);

                if (errors.Count > 0)
                {
                    return SendBadRequest(errors);
                }

                var result = await _sensorNodeService.UpdateSensorNode(
                    new SensorNodeDto
                    {
                        Id = id,
                        Name = viewModel.Name,
                        Code = viewModel.Code,
                        GatewayId = viewModel.GatewayId
                    }
                );
                return OkOrFail(result == OperationStatus.Succeed);
            }

            if (isGatewayExist == OperationStatus.NotFound)
            {
                return SendNotFound();
            }

            return ServerError();
        }

        [HttpDelete, HfAclAuthorize(SystemPrivileges.CanDeleteSensorNode)]
        public async Task<IActionResult> DeleteNodes([FromQuery] ListIdDeletionViewModel viewModel)
        {
            var user = HttpContext.GetAuthorizedUser();

            var listItems = viewModel.ListId
                .Where(x => x.HasValue && x.Value > 0)
                .Select(x => x.GetValueOrDefault());

            if (user.Role == UserModel.MANAGER_ROLE)
            {
                var farmOfUser = await _userService.GetCurrentFarmIdOfUserAsync(user.Id);
                if (farmOfUser.Key == OperationStatus.Exception || farmOfUser.Key == OperationStatus.NotFound)
                {
                    return ServerError();
                }

                var gatewayIdOfFarm = await _farmService.GetAllGatewayIdOwnedByFarm(farmOfUser.Value.GetValueOrDefault());

                if (gatewayIdOfFarm == null) return ServerError();

                listItems = listItems.Where(x => gatewayIdOfFarm.Contains(x));
            }

            var result = await _sensorNodeService.DeleteNodes(listItems);

            return result switch
            {
                OperationStatus.Succeed => Succeed(),
                OperationStatus.Failed => SendBadRequestCommon(Localizer["hf.messages.failed_to_delete_nodes"].Value),
                _ => ServerError()
            };
        }

        private async Task<Dictionary<string, string[]>> ValidateSensorNode(string name, string code, int? id)
        {
            var errors = new Dictionary<string, string[]>();

            name = name.Trim().ToLower();
            code = code.Trim().ToLower();

            if (id.HasValue && id > 0)
            {
                if (await _sensorNodeService.ExistsByAsync(x => x.Name.ToLower().Trim() == name && x.Id != id))
                {
                    errors.Add("name", new[] { Localizer["hf.messages.sensor_node_duplicated"].Value });
                }

                if (await _sensorNodeService.ExistsByAsync(x => x.Code.ToLower().Trim() == code && x.Id != id))
                {
                    errors.Add("code", new[] { Localizer["hf.messages.sensor_code_duplicated"].Value });
                }
            }
            else
            {
                if (await _sensorNodeService.ExistsByAsync(x => x.Name.ToLower().Trim() == name))
                {
                    errors.Add("name", new[] { Localizer["hf.messages.sensor_node_duplicated"].Value });
                }

                if (await _sensorNodeService.ExistsByAsync(x => x.Code.ToLower().Trim() == code))
                {
                    errors.Add("code", new[] { Localizer["hf.messages.sensor_code_duplicated"].Value });
                }
            }

            return errors;
        }
    }
}
