﻿using Askmethat.Aspnet.JsonLocalizer.Localizer;
using HappyFarm.Api.Filters;
using HappyFarm.Api.Models;
using HappyFarm.Api.Models.Gateway;
using HappyFarm.Core.Enums;
using HappyFarm.Core.Models;
using HappyFarm.Infrastructure.Commons;
using HappyFarm.Infrastructure.Dto;
using HappyFarm.Infrastructure.Permissions;
using HappyFarm.Infrastructure.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserModel = HappyFarm.Core.Models.User;

namespace HappyFarm.Api.Controllers
{
    [ApiController, Route("gateways"), ApiVersion("1.0")]
    public class GatewayController : BaseController
    {
        private readonly IGatewayService _gatewayService;
        private readonly IFarmService _farmService;
        private readonly IUserService _userService;

        public GatewayController(
            IGatewayService gatewayService,
            IFarmService farmService,
            IJsonStringLocalizer localizer,
            IUserService userService
            )
            : base(localizer)
        {
            _gatewayService = gatewayService;
            _farmService = farmService;
            _userService = userService;
        }

        [HttpGet, HFAuthorize, HfAclAuthorize(SystemPrivileges.CanViewGateways)]
        public async Task<IActionResult> GetAll([FromQuery] GatewayQueryFilter queryFilter)
        {
            var user = HttpContext.GetAuthorizedUser();
            int? userId = null;
            if (user.Role == UserModel.MANAGER_ROLE)
            {
                userId = user.Id;
            }
            var result = await _gatewayService.FindAllAsync(queryFilter.Keyword, userId, queryFilter.Page, queryFilter.PageSize);
            return result != null ? SucceedPagination(result) : ServerError();
        }

        [HttpGet("{id:int}"), HFAuthorize, HfAclAuthorize(SystemPrivileges.CanViewDetailGateway)]
        public async Task<IActionResult> GetGateway([FromRoute] int id)
        {
            var user = HttpContext.GetAuthorizedUser();
            var gwExists = await _gatewayService.IsExistById(id);

            switch (gwExists)
            {
                case OperationStatus.NotFound:
                    return SendNotFound();
                case OperationStatus.Exception:
                    return ServerError();
            }

            if (user.Role == UserModel.MANAGER_ROLE)
            {
                var isSameFarm = await _userService.CheckIfFarmOfUserAndGatewayIsSame(user.Id, id);

                switch (isSameFarm)
                {
                    case OperationStatus.Incorrect:
                        return SendForbidden();
                    case OperationStatus.Exception:
                        return ServerError();
                }
            }

            var result = await _gatewayService.GetGatewayDetail(id);
            return result != null ? Succeed(result) : ServerError();
        }

        [HttpGet("gateway-infos"), HFAuthorize, HfAclAuthorize(SystemPrivileges.CanViewGateways)]
        public async Task<IActionResult> GetGatewayDropdown()
        {
            var user = HttpContext.GetAuthorizedUser();
            int? farmId = null;

            if (user.Role == UserModel.MANAGER_ROLE)
            {
                var userFarm = await _userService.GetCurrentFarmIdOfUserAsync(user.Id);
                switch (userFarm.Key)
                {
                    case OperationStatus.Succeed:
                        farmId = userFarm.Value;
                        break;
                    case OperationStatus.NotFound:
                        return SendNotFound();
                    default: return ServerError();
                }
            }

            var result = await _gatewayService.GetGatewayDropdown(farmId);
            return result == null ? ServerError() : Succeed(result);
        }

        [HttpGet("by-manager"), HFAuthorize, HfAclAuthorize(SystemPrivileges.CanViewGatewaysByManager)]
        public async Task<IActionResult> GetGatewaysByManager()
        {
            var user = HttpContext.GetAuthorizedUser();
            var result = await _gatewayService.GetAllGatewaysByManager(user.Id);
            return result != null ? Succeed(result) : ServerError();
        }

        [HttpGet("by-manager/{gwId:int}"), HFAuthorize, HfAclAuthorize(SystemPrivileges.CanViewGatewayByManager)]
        public async Task<IActionResult> GetDetailGatewayByManager([FromRoute] int gwId)
        {
            var user = HttpContext.GetAuthorizedUser();
            var result = await _gatewayService.GetGatewayDetailForManager(gwId, user.Id);

            return result.Key switch
            {
                OperationStatus.Succeed => Succeed(result.Value),
                OperationStatus.NotFound => NotFound(),
                OperationStatus.Exception => ServerError(),
                _ => ServerError(),
            };
        }

        [HttpGet("info")]
        public async Task<IActionResult> GetMqttInfo([FromQuery] string code)
        {
            if (string.IsNullOrEmpty(code)) return SendNotFound();
            var info = await _gatewayService.GetMqttInfo(code);
            return Succeed(info);
        }

        [HttpPost, HFAuthorize, HfAclAuthorize(SystemPrivileges.CanAddGateway)]
        public async Task<IActionResult> CreateGateway([FromBody] GatewayCreationViewModel viewModel)
        {
            var userId = HttpContext.GetAuthorizedUser().Id;
            var errors = await ValidateExists(viewModel.Code, viewModel.FarmId, null);

            if (errors.Count > 0)
            {
                return SendBadRequest(errors);
            }

            var result = await _gatewayService.CreateGateway(
                new GatewayDto
                {
                    Name = viewModel.Name.Trim(),
                    Code = viewModel.Code.Trim(),
                    Config = Config.GetDefault(),
                    FarmId = viewModel.FarmId
                }
                , userId);
            return result switch
            {
                OperationStatus.Succeed => Succeed(),
                OperationStatus.Failed => SendBadRequestCommon(Localizer["hf.messages.failed_to_create_gateway"].Value),
                OperationStatus.AccessDenied => SendForbidden(),
                OperationStatus.Exception => ServerError(),
                _ => ServerError(),
            };
        }

        [HttpPut("{id:int}"), HFAuthorize, HfAclAuthorize(SystemPrivileges.CanUpdateGateway)]
        public async Task<IActionResult> UpdateGateway([FromRoute] int id, [FromBody] GatewayUpdateViewModel viewModel)
        {
            var checkGwExists = await _gatewayService.IsExistById(id);

            switch (checkGwExists)
            {
                case OperationStatus.NotFound:
                    return SendNotFound();
                case OperationStatus.Exception:
                    return ServerError();
            }

            var userId = HttpContext.GetAuthorizedUser().Id;
            var errors = await ValidateExists(viewModel.Code, viewModel.FarmId, id);

            if (errors.Count > 0)
            {
                return SendBadRequest(errors);
            }

            var result = await _gatewayService.UpdateGateway(
                new GatewayDto
                {
                    Id = id,
                    Name = viewModel.Name.Trim(),
                    Code = viewModel.Code.Trim(),
                    FarmId = viewModel.FarmId
                }
                , userId);
            return result switch
            {
                OperationStatus.Succeed => Succeed(),
                OperationStatus.Failed => SendBadRequestCommon(Localizer["hf.messages.failed_to_update_gateway"].Value),
                OperationStatus.AccessDenied => SendForbidden(),
                OperationStatus.Exception => ServerError(),
                _ => ServerError(),
            };
        }

        [HttpPut("update-settings"), HFAuthorize, HfAclAuthorize(SystemPrivileges.CanUpdateGatewaySettings)]
        public async Task<IActionResult> UpdateSetting([FromBody] UpdateSettingViewModel viewModel)
        {
            var listId = viewModel.ListId.Where(x => x > 0);
            var updated = await _gatewayService.UpdateSettings(viewModel.Data, listId, true);

            return updated switch
            {
                OperationStatus.Succeed => Succeed(),
                OperationStatus.Failed => SendBadRequestCommon(Localizer["hf.messages.failed_update_gatewate_settings"]),
                _ => ServerError()
            };

        }

        [HttpDelete, HFAuthorize, HfAclAuthorize(SystemPrivileges.CanDeleteGateway)]
        public async Task<IActionResult> Delete([FromQuery] ListIdDeletionViewModel viewModel)
        {
            var user = HttpContext.GetAuthorizedUser();

            var listItems = viewModel.ListId
                .Where(x => x.HasValue && x.Value > 0)
                .Select(x => x.GetValueOrDefault());

            if (listItems.Any())
            {
                if (user.Role == UserModel.MANAGER_ROLE)
                {
                    var farmOfUser = await _userService.GetCurrentFarmIdOfUserAsync(user.Id);
                    if (farmOfUser.Key == OperationStatus.Exception || farmOfUser.Key == OperationStatus.NotFound)
                    {
                        return ServerError();
                    }

                    var gatewayIdOfFarm = await _farmService.GetAllGatewayIdOwnedByFarm(farmOfUser.Value.GetValueOrDefault());

                    if (gatewayIdOfFarm == null) return ServerError();

                    listItems = listItems.Where(x => gatewayIdOfFarm.Contains(x));
                }

                var result = await _gatewayService.Delete(listItems);

                return result switch
                {
                    OperationStatus.Succeed => Succeed(),
                    OperationStatus.Failed => SendBadRequestCommon(Localizer["hf.messages.failed_to_delete_gateway"].Value),
                    _ => ServerError()
                };
            }

            return Succeed();
        }

        private async Task<IDictionary<string, string[]>> ValidateExists(string code, int farm_id, int? id)
        {
            var dictResult = new Dictionary<string, string[]>();

            code = code.Trim();

            if (await _farmService.CheckExistsBy(x => x.Id == farm_id) != OperationStatus.Succeed)
            {
                dictResult.Add(nameof(farm_id), new string[] { Localizer["hf.messages.farm_doesnt_exist"] });
            }

            if (id.HasValue && id > 0)
            {

                if (await _gatewayService.CheckExistsBy(x => x.Id != id.Value && x.Code == code) == OperationStatus.Succeed)
                {
                    dictResult.Add(nameof(code), new string[] { Localizer["hf.messages.gateway_code_duplicated"] });
                }
            }
            else
            {

                if (await _gatewayService.CheckExistsBy(x => x.Code == code) == OperationStatus.Succeed)
                {
                    dictResult.Add(nameof(code), new string[] { Localizer["hf.messages.gateway_code_duplicated"] });
                }
            }

            return dictResult;
        }
    }
}
