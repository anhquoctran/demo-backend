﻿using HappyFarm.Core.Enums;
using HappyFarm.Infrastructure.Commons;
using HappyFarm.Infrastructure.Dto;
using HappyFarm.Infrastructure.Responses;
using HappyFarm.Infrastructure.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Primitives;
using System;
using System.Net;

namespace HappyFarm.Api.Filters
{
    /// <summary>
    /// RESTFul API JWT Authorize Filter
    /// </summary>
    public class HFAuthorizeAttribute : Attribute, IAuthorizationFilter, IOrderedFilter
    {
        private IAuthService _authService;
        private IUserService _userService;

        public int Order { get; set; } = int.MinValue + 10;

        /// <summary>
        /// On authorization method, called while authorizing
        /// </summary>
        /// <param name="context">Authorization context</param>
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var requestServices = context.HttpContext.RequestServices;
            _authService = (IAuthService)requestServices.GetRequiredService(typeof(IAuthService));
            _userService = (IUserService)requestServices.GetRequiredService(typeof(IUserService));

            DoAuthorize(context);
        }

        /// <summary>
        /// Authorize for web
        /// </summary>
        /// <param name="context">Authorization context</param>
        /// <param name="userService">User</param>
        /// <param name="tokenManager">Token manager</param>
        private void DoAuthorize(AuthorizationFilterContext context)
        {
            try
            {
                var isJwtExists = context.HttpContext.Request.Headers.TryGetValue("Authorization", out StringValues jwt);
                if (isJwtExists && !string.IsNullOrEmpty(jwt.ToString()))
                {
                    var (success, data) = JwtAuthFactory.ValidateJwtToken(jwt);

                    if (success)
                    {

                        //var isTokenActive = tokenManager.IsActive(jwt);

                        //if (!isTokenActive)
                        //{
                        //    context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                        //    context.Result = new UnauthorizedObjectResult(new UnauthorizedResponse());
                        //    return;
                        //}

                        var payload = data.Payload;

                        if (payload.TryGetValue("id", out object idRaw))
                        {
                            var idString = (string)idRaw;
                            var idParsedOk = int.TryParse(idString, out int id);

                            if (idParsedOk)
                            {
                                var canAccess = _authService.Authorize(id);
                                if (!canAccess)
                                {
                                    context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Forbidden;
                                    context.Result = new ObjectResult(new ForbiddenResponse());
                                }
                                else
                                {
                                    var resultRole = _userService.GetCurrentRoleOfUser(id);

                                    switch (resultRole.Key)
                                    {
                                        case OperationStatus.Succeed:
                                            context.HttpContext.SetAuthorizedUser(new AuthorizedUser { Id = id, Role = resultRole.Value });
                                            return;
                                        case OperationStatus.Failed:
                                            context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                                            context.Result = new UnauthorizedObjectResult(new UnauthorizedResponse());
                                            return;
                                        case OperationStatus.Exception:
                                            context.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                                            context.Result = new UnauthorizedObjectResult(new ServerErrorResponse());
                                            return;
                                    }
                                    
                                }
                            }
                            else
                            {
                                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                                context.Result = new UnauthorizedObjectResult(new UnauthorizedResponse());
                            }
                        }
                        else
                        {
                            context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                            context.Result = new UnauthorizedObjectResult(new UnauthorizedResponse());
                        }
                    }
                    else
                    {
                        context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                        context.Result = new UnauthorizedObjectResult(new UnauthorizedResponse());
                    }
                }
                else
                {
                    context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                    context.Result = new UnauthorizedObjectResult(new UnauthorizedResponse());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                context.Result = new ObjectResult(new ServerErrorResponse());
            }

        }
    }
}
