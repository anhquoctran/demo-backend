﻿using HappyFarm.Core.Enums;
using HappyFarm.Infrastructure.Commons;
using HappyFarm.Infrastructure.Permissions;
using HappyFarm.Infrastructure.Responses;
using HappyFarm.Infrastructure.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Net;

namespace HappyFarm.Api.Filters
{
    /// <summary>
    /// Set access control/privileges for API endpoint
    /// </summary>
    public class HfAclAuthorize : TypeFilterAttribute
    {

        public HfAclAuthorize(SystemPrivileges permission) : base(typeof(AccessControlType))
        {
            GetPermissionList(permission);
        }

        private void GetPermissionList(SystemPrivileges permission)
        {
            var privilege = Privileges.GetPrivileges(permission);
            Arguments = new object[] { privilege };
        }

        private class AccessControlType : IActionFilter
        {
            private readonly string[] _acceptsRole;
            private readonly IAuthService _authService;

            public AccessControlType(string[] privilege, IAuthService authService)
            {
                _acceptsRole = privilege;
                _authService = authService;
            }

            public void OnActionExecuted(ActionExecutedContext context) { }

            public void OnActionExecuting(ActionExecutingContext context)
            {
                var user = context.HttpContext.GetAuthorizedUser();
                if (user != null)
                {
                    var isAccepts = _authService.IsRoleCanAccess(user.Id, _acceptsRole);

                    switch (isAccepts)
                    {
                        case OperationStatus.Succeed:
                            break;
                        case OperationStatus.AccessDenied:
                            context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Forbidden;
                            context.Result = new ObjectResult(new ForbiddenResponse());
                            break;
                        case OperationStatus.Exception:
                        default:
                            context.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                            context.Result = new ObjectResult(new ServerErrorResponse());
                            break;
                    }
                }
                else
                {
                    context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                    context.Result = new UnauthorizedObjectResult(new UnauthorizedResponse());
                }

            }
        }
    }
}
