﻿using FluentValidation;
using HappyFarm.Api.Models.Gateway;
using HappyFarm.Infrastructure.Validations;

namespace HappyFarm.Api.Validators
{
    public class GatewayCreationValidator : AbstractValidator<GatewayCreationViewModel>
    {
        public GatewayCreationValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .HasValidDeviceNaming()
                .OverridePropertyName("name");

            RuleFor(x => x.Code)
                .NotEmpty()
                .MaximumLength(20)
                .OverridePropertyName("code");

            RuleFor(x => x.FarmId)
                .NotEmpty()
                .OverridePropertyName("farm_id");
        }
    }
}
