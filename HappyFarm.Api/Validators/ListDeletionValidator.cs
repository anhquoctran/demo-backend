﻿using FluentValidation;
using HappyFarm.Api.Models;

namespace HappyFarm.Api.Validations.Validators
{
    public class ListDeletionValidator : AbstractValidator<ListIdDeletionViewModel>
    {
        public ListDeletionValidator()
        {
            RuleFor(x => x.ListId)
                .NotEmpty()
                .OverridePropertyName("id");
        }
    }
}
