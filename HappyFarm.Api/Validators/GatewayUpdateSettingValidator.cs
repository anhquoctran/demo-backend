﻿using FluentValidation;
using HappyFarm.Api.Models.Gateway;

namespace HappyFarm.Api.Validations.Validators
{
    public class GatewayUpdateSettingValidator : AbstractValidator<UpdateSettingViewModel>
    {
        public GatewayUpdateSettingValidator()
        {
            RuleFor(x => x.ListId)
                .NotEmpty()
                .OverridePropertyName("list_id");

            RuleFor(x => x.Data)
                .NotEmpty()
                .OverridePropertyName("data");
        }
    }
}
