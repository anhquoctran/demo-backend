﻿using FluentValidation;
using HappyFarm.Api.Models.Farm;

namespace HappyFarm.Api.Validations.Validators
{
    public class ListFarmQueryValidator : AbstractValidator<ListFarmQueryViewModel>
    {
        public ListFarmQueryValidator()
        {
            RuleFor(x => x.Keyword)
                .Length(2, 64)
                .OverridePropertyName("keyword");

            RuleFor(x => x.Page)
                .GreaterThanOrEqualTo(1)
                .OverridePropertyName("page");

            RuleFor(x => x.PageSize)
                .GreaterThanOrEqualTo(5)
                .OverridePropertyName("page_size");
        }
    }
}
