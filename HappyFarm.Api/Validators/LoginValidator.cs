﻿using FluentValidation;
using HappyFarm.Api.Models.User;

namespace HappyFarm.Api.Validations.Validators
{
    public class LoginValidator : AbstractValidator<LoginViewModel>
    {
        public LoginValidator()
        {
            RuleFor(x => x.Username)
                .NotEmpty()
                .WithName("username");

            RuleFor(x => x.Password)
                .NotEmpty()
                .WithName("password");
        }
    }
}
