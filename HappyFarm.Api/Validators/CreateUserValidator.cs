﻿using FluentValidation;
using FluentValidation.Validators;
using HappyFarm.Api.Models.User;
using HappyFarm.Core.Commons;
using HappyFarm.Core.Models;
using HappyFarm.Infrastructure.Validations;

namespace HappyFarm.Api.Validations.Validators
{
    public class CreateUserValidator : AbstractValidator<CreateUserViewModel>
    {
        public CreateUserValidator()
        {
            RuleFor(cd => cd.Firstname)
                .NotEmpty()
                .DependentRules(() =>
                {
                    RuleFor(cd => cd.Firstname)
                        .HasValidHumanNaming()
                        .Length(2, 24);
                })
                .OverridePropertyName("first_name");

            RuleFor(cd => cd.Lastname)
                .NotEmpty()
                .DependentRules(() =>
                {
                    RuleFor(cd => cd.Lastname)
                    .HasValidHumanNaming()
                    .Length(2, 24)
                    .OverridePropertyName("last_name");
                })
                .OverridePropertyName("last_name");

            RuleFor(cd => cd.Username)
                .NotEmpty()
                .DependentRules(() =>
                {
                    RuleFor(cd => cd.Username)
                    .Length(2, 24)
                    .OverridePropertyName("username");
                })
                .OverridePropertyName("username");

            RuleFor(cd => cd.Email)
                .NotEmpty()
                .DependentRules(() =>
                {
                    RuleFor(cd => cd.Email)
                    .EmailAddress(EmailValidationMode.AspNetCoreCompatible)
                    .OverridePropertyName("email");
                })
                .OverridePropertyName("email");

            RuleFor(cd => cd.Password)
                .NotEmpty()
                .DependentRules(() =>
                {
                    RuleFor(cd => cd.Password)
                        .Length(6, 64)
                        .Equal(c => c.PasswordConfirmation)
                        .OverridePropertyName("password");
                })
                .OverridePropertyName("password");

            RuleFor(cd => cd.PasswordConfirmation)
                .NotEmpty()
                .OverridePropertyName("password_confirmation");

            RuleFor(x => x.Role)
                .NotEmpty()
                .HasValidRole()
                .OverridePropertyName("role");

            RuleFor(x => x.FarmId)
                .NotEmpty()
                .When(x => x.Role == User.MANAGER_ROLE)
                .OverridePropertyName("farm_id");

            RuleFor(x => x.Avatar)
                .HasFileExtension(FileUtils.ImageExtensions)
                .HasFileLength(Constants.MAXIMUM_FILE_LENGTH_ALLOWED)
                .HasMimeType(FileUtils.ImageMimeTypes)
                .OverridePropertyName("avatar");
        }
    }
}
