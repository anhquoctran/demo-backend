﻿using FluentValidation;
using HappyFarm.Api.Models.SensorNode;

namespace HappyFarm.Api.Validations.Validators
{
    public class SensorNodeCreationValidator : AbstractValidator<SensorNodeCreationViewModel>
    {
        public SensorNodeCreationValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                //.HasValidDeviceNaming()
                .OverridePropertyName("name");

            RuleFor(x => x.Code)
                .NotEmpty()
                .OverridePropertyName("code");

            RuleFor(x => x.GatewayId)
                .NotEmpty()
                .OverridePropertyName("gateway_id");
        }
    }
}
