﻿using FluentValidation;
using HappyFarm.Api.Models.Farm;
using HappyFarm.Infrastructure.Validations;

namespace HappyFarm.Api.Validations.Validators
{
    public class FarmUpdateValidator : AbstractValidator<FarmUpdateViewModel>
    {
        public FarmUpdateValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .MaximumLength(128)
                .WithName("name");

            RuleFor(x => x.Phone)
                .NotEmpty()
                .IsValidPhoneNumber()
                .WithName("phone");

            RuleFor(x => x.Address)
                .NotEmpty()
                .MaximumLength(255)
                .WithName("address");

            RuleFor(x => x.RegionCode)
                .NotEmpty()
                .IsRegionCode()
                .WithName("region_code");
        }
    }
}
