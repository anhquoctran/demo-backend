﻿/*
 * Copyright [2020] [anhquoctran]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace HappyFarm.Api.Middlewares
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class CustomCoreMiddleware
    {
        private readonly RequestDelegate _next;

        public CustomCoreMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            httpContext.Response.OnStarting(() =>
            {
                httpContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { (string)httpContext.Request.Headers["Origin"] });
                httpContext.Response.Headers.Add("Access-Control-Allow-Headers", new[] { "Origin, X-Requested-With, Content-Type, Accept" });
                httpContext.Response.Headers.Add("Access-Control-Allow-Methods", new[] { "GET, POST, PUT, DELETE, OPTIONS" });
                httpContext.Response.Headers.Add("Access-Control-Allow-Credentials", new[] { "true" });

                return Task.CompletedTask;
            });

            if (!httpContext.Response.HasStarted)
                await _next.Invoke(httpContext);

            
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class CustomCoreMiddlewareExtension
    {
        public static IApplicationBuilder UseCustomCoreMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<CustomCoreMiddleware>();
        }
    }
}
