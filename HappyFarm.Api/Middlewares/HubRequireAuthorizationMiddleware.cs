﻿using HappyFarm.Infrastructure.Commons;
using HappyFarm.Infrastructure.Services.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace HappyFarm.Api.Middlewares
{
    public class HubRequireAuthorizationMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IAuthService _authService;

        public HubRequireAuthorizationMiddleware(
            RequestDelegate next,
            IAuthService authService
            )
        {
            _next = next;
            _authService = authService;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            var json = ResponseUtils.GetJsonResponseString(HttpStatusCode.Unauthorized);
            
            if (httpContext.Request.Path.StartsWithSegments("/happyfarm-hub"))
            {
                if (httpContext.Request.Query.TryGetValue("access_token", out var accessToken))
                {
                    if (accessToken.Any())
                    {
                        var (success, data) = JwtAuthFactory.ValidateJwtToken(accessToken);
                        if (!success)
                        {
                            httpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized ;
                            await httpContext.Response.WriteAsync(json, Encoding.UTF8);
                            return;
                        }

                        var payload = data.Payload;
                        if (payload.TryGetValue("id", out object idRaw))
                        {
                            var idString = (string)idRaw;
                            var idParsedOk = int.TryParse(idString, out int id);

                            if (idParsedOk)
                            {
                                var canAccess = _authService.Authorize(id);
                                if (!canAccess)
                                {
                                    httpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                                    await httpContext.Response.WriteAsync(json, Encoding.UTF8);
                                    return;
                                }

                            }

                            httpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                            await httpContext.Response.WriteAsync(json, Encoding.UTF8);
                            return;
                        }
                    }
                    else
                    {

                    }
                }
            }

            if (!httpContext.Response.HasStarted)
            {
                await _next.Invoke(httpContext);
            }
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class HubRequireAuthorizationMiddlewareExtensions
    {
        public static IApplicationBuilder UseHubRequireAuthorizationMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<HubRequireAuthorizationMiddleware>();
        }
    }
}
