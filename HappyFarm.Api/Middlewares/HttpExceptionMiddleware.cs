﻿/*
 * Copyright [2020] [anhquoctran]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

using HappyFarm.Infrastructure.Commons;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace HappyFarm.Api.Middlewares
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class HttpExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IWebHostEnvironment _env;

        public HttpExceptionMiddleware(RequestDelegate next, IWebHostEnvironment env)
        {
            _next = next;
            _env = env;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            if (!_env.IsDevelopment())
            {
                if (!httpContext.Request.IsStaticLocationRequest())
                {
                    httpContext.Response.ContentType = "application/json";
                    httpContext.Response.Headers["Accept"] = "application/json";

                    if (!httpContext.Response.HasStarted)
                        await _next.Invoke(httpContext);

                    if (httpContext.Response.StatusCode >= (int)HttpStatusCode.NotFound && httpContext.Response.StatusCode < (int)HttpStatusCode.InternalServerError)
                    {
                        var httpStatus = (HttpStatusCode)httpContext.Response.StatusCode;
                        var json = ResponseUtils.GetJsonResponseString(httpStatus);
                        await httpContext.Response.WriteAsync(json, Encoding.UTF8);
                    }
                }
                else
                {
                    httpContext.Response.ContentType = "text/html";
                    httpContext.Response.Headers["Accept"] = "text/html";
                    if (!httpContext.Response.HasStarted)
                        await _next.Invoke(httpContext);
                    if (httpContext.Response.StatusCode >= (int)HttpStatusCode.NotFound && httpContext.Response.StatusCode < (int)HttpStatusCode.InternalServerError)
                    {
                        var httpStatus = (HttpStatusCode)httpContext.Response.StatusCode;
                        var rawText = ResponseUtils.GetRawTextResponse(httpStatus);
                        await httpContext.Response.WriteAsync(rawText, Encoding.UTF8);
                    }
                }
            } else
            {
                if (!httpContext.Response.HasStarted)
                    await _next.Invoke(httpContext);
            }
            
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class HttpExceptionMiddlewareExtensions
    {
        public static IApplicationBuilder UseHttpExceptionMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<HttpExceptionMiddleware>();
        }
    }
}
