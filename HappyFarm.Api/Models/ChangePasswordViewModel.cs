﻿using Newtonsoft.Json;

namespace HappyFarm.Api.Models
{
    public class ChangePasswordViewModel
    {
        [JsonProperty("old_password")]
        public string OldPassword { get; set; }

        [JsonProperty("old_password")]
        public string NewPassword { get; set; }

        [JsonProperty("repeat_new_password")]
        public string RepeatNewPassword { get; set; }
    }
}
