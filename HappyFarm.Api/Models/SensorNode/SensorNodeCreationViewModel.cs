﻿using Newtonsoft.Json;

namespace HappyFarm.Api.Models.SensorNode
{
    public class SensorNodeCreationViewModel
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("gateway_id")]
        public int? GatewayId { get; set; }
    }
}
