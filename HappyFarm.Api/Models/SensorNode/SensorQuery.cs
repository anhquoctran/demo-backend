﻿using Microsoft.AspNetCore.Mvc;

namespace HappyFarm.Api.Models.SensorNode
{
    public class SensorQuery
    {
        [FromQuery(Name = "keyword")]
        public string Keyword { get; set; }

        [FromQuery(Name ="gateway_id")]
        public int? GatewayId { get; set; }

        [FromQuery(Name ="page")]
        public int? Page { get; set; }

        [FromQuery(Name = "page_size")]
        public int? PageSize { get; set; }
    }
}
