﻿using HappyFarm.Core.Commons;

namespace HappyFarm.Api.Models.User
{
    public class ListUserQueryViewModel
    {
        public string keyword { get; set; }

        public string role { get; set; }

        public int? page { get; set; } = Constants.PaginationConfiguration.DEFAULT_PAGE_NUMBER;

        public int? page_size { get; set; } = Constants.PaginationConfiguration.DEFAULT_PAGE_SIZE;
    }
}
