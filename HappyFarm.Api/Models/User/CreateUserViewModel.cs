﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HappyFarm.Api.Models.User
{
    public class CreateUserViewModel
    {
        [FromForm(Name = "first_name")]
        public string Firstname { get; set; }

        [FromForm(Name = "last_name")]
        public string Lastname { get; set; }

        [FromForm(Name = "role")]
        public string Role { get; set; }

        [FromForm(Name = "username")]
        public string Username { get; set; }

        [FromForm(Name = "password")]
        public string Password { get; set; }

        [FromForm(Name = "password_confirmation")]
        public string PasswordConfirmation { get; set; }

        [FromForm(Name = "avatar")]
        public IFormFile Avatar { get; set; }

        [FromForm(Name = "email")]
        public string Email { get; set; }

        [FromForm(Name = "farm_id")]
        public int? FarmId { get; set; }
    }
}
