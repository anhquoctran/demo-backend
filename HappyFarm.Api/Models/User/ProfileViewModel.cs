﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace HappyFarm.Api.Models.User
{
    public class ProfileViewModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("firstname")]
        public string FirstName { get; set; }

        [JsonProperty("lastname")]
        public string LastName { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("avatar")]
        public string Avatar { get; set; }

        [JsonProperty("roles")]
        public string[] Role { get; set; }

        [JsonProperty("farm_id", NullValueHandling = NullValueHandling.Ignore)]
        public int? FarmId { get; set; }

        [JsonProperty("permission", NullValueHandling = NullValueHandling.Ignore)]
        public IEnumerable<string> Permissions { get; set; }
    }
}
