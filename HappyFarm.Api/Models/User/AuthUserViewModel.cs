﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace HappyFarm.Api.Models.User
{
    public class AuthUserViewModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("roles")]
        public string[] Role { get; set; }

        [JsonProperty("avatar")]
        public string Avatar { get; set; }

        [JsonProperty("token")]
        public string Token { get; set; }

        [JsonProperty("permissions")]
        public IEnumerable<string> Permissions { get; set; }
    }
}
