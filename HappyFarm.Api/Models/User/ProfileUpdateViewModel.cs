﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HappyFarm.Api.Models.User
{
    public class ProfileUpdateViewModel
    {
        [FromForm(Name = "firstname")]
        public string FirstName { get; set; }

        [FromForm(Name = "lastname")]
        public string LastName { get; set; }

        [FromForm(Name = "avatar")]
        public IFormFile Avatar { get; set; }

        [FromForm(Name = "email")]
        public string Email { get; set; }
    }
}
