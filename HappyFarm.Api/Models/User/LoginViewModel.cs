﻿using Newtonsoft.Json;

namespace HappyFarm.Api.Models.User
{
    public class LoginViewModel
    {
        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }
    }
}
