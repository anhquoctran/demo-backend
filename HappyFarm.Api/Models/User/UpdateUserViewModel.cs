﻿using Newtonsoft.Json;

namespace HappyFarm.Api.Models.User
{
    public class UpdateUserViewModel
    {
        [JsonProperty("first_name")]
        public string Firstname { get; set; }

        [JsonProperty("last_name")]
        public string Lastname { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }
    }
}
