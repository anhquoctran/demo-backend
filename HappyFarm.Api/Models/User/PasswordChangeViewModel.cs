﻿using Newtonsoft.Json;

namespace HappyFarm.Api.Models.User
{
    public class PasswordChangeViewModel
    {
        [JsonProperty("current_password")]
        public string CurrentPassword { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("password_confirmation")]
        public string PasswordConfirmation { get; set; }
    }
}
