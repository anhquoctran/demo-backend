﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace HappyFarm.Api.Models
{
    public class ListIdDeletionViewModel
    {
        [JsonProperty("list_id"), FromQuery(Name = "id" )]
        public IEnumerable<int?> ListId { get; set; }
    }
}
