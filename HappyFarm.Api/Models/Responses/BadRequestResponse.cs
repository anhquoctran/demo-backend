﻿/*
 * Copyright [2020] [anhquoctran]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

using System.Net;

namespace HappyFarm.Api.Models.Responses
{
    public class BadRequestResponse<T> : BaseApiResponse<T>
    {
        public BadRequestResponse(string messages) : base(default, "Bad Request", (int)HttpStatusCode.BadRequest, false)
        {
            Errors = new[] { messages };
        }

        public BadRequestResponse(T messages) : base(default, "Bad Request", (int)HttpStatusCode.BadRequest, false)
        {
            Errors = messages;
        }

        private BadRequestResponse() { }

        public static BadRequestResponse<T> SetCommonError(string commonMessageError)
        {
            BadRequestResponse<T> badResponse = new BadRequestResponse<T>
            {
                Data = null,
                Success = false,
                Status = (int)HttpStatusCode.BadRequest,
                Message = GetDefaultMessageFromStatusCode(400),
                Errors = new { common = commonMessageError }
            };
            return badResponse;
        }
    }
}
