﻿using Newtonsoft.Json;

namespace HappyFarm.Api.Models.Gateway
{
    public class SensorNodesByGatewaysViewModel
    {

        [JsonProperty("id")]
        public int NodeId { get; set; }
    }
}
