﻿using HappyFarm.Infrastructure.Dto;
using Newtonsoft.Json;

namespace HappyFarm.Api.Models.Gateway
{
    public class UpdateSettingViewModel
    {
        [JsonProperty("list_id")]
        public int[] ListId { get; set; }

        [JsonProperty("data")]
        public DeviceConfig Data { get; set; }
    }
}
