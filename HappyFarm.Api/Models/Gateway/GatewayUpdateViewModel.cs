﻿using Newtonsoft.Json;

namespace HappyFarm.Api.Models.Gateway
{
    public class GatewayUpdateViewModel
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("farm_id")]
        public int FarmId { get; set; }
    }
}
