﻿using Newtonsoft.Json;

namespace HappyFarm.Api.Models.Farm
{
    public class FarmCreationViewModel
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("region_code")]
        public string RegionCode { get; set; } = "VN";

        [JsonProperty("phone")]
        public string Phone { get; set; }
    }
}
