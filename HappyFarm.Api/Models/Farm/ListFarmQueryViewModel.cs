﻿using Microsoft.AspNetCore.Mvc;

namespace HappyFarm.Api.Models.Farm
{
    public class ListFarmQueryViewModel
    {
        [FromQuery(Name = "keyword")]
        public string Keyword { get; set; }

        [FromQuery(Name ="page")]
        public int? Page { get; set; }

        [FromQuery(Name = "page_size")]
        public int? PageSize { get; set; }
    }
}
