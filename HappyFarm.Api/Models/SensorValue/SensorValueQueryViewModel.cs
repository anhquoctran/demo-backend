﻿using Microsoft.AspNetCore.Mvc;

namespace HappyFarm.Api.Models
{
    public class SensorValueQueryViewModel
    {
        [FromQuery(Name = "farm")]
        public int? FarmId { get; set; }

        [FromQuery(Name = "gateway")]
        public int? GatewayId { get; set; }

        [FromQuery(Name = "node")]
        public int? NodeId { get; set; }

        [FromQuery(Name = "from")]
        public int? From { get; set; }

        [FromQuery(Name = "to")]
        public int? To { get; set; }

        [FromQuery(Name = "page")]
        public int? Page { get; set; }

        [FromQuery(Name = "page_size")]
        public int? PageSize { get; set; } = 20;
    }
}
