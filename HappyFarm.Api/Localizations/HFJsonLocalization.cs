﻿/*
 * Copyright [2020] [anhquoctran]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

using System.Collections.Generic;

namespace HappyFarm.Api.Localizations
{
    public class HFJsonLocalization
    {
        public string Locale { get; set; } = "vi";
        public IDictionary<string, string> LocalizedValues { get; set; } = new Dictionary<string, string>();
    }
}
