﻿/*
 * Copyright [2020] [anhquoctran]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

using Askmethat.Aspnet.JsonLocalizer.Localizer;
using HappyFarm.Core.Commons;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace HappyFarm.Api.Localizations
{
    public class HFJsonStringLocalizer : IJsonStringLocalizer
    {
        private readonly List<HFJsonLocalization> _localization = new List<HFJsonLocalization>();

        public HFJsonStringLocalizer()
        {
            ReadJsonFile();
        }

        private void ReadJsonFile()
        {
            try
            {
                var resourcePath = Path.Combine(Startup.EndpointPath, Constants.RESOURCE_DIRECTORY_NAME, Constants.LOCALE_DIRECTORY_NAME);
                Directory.CreateDirectory(resourcePath);
                var ciSupported = Startup.SupportedCultures.Select(ci => ci.Name.ToLower()).ToArray();
                foreach (var ciName in ciSupported)
                {
                    var jsonFilename = $"{ciName}.json";
                    var jsonFilepath = Path.Combine(resourcePath, jsonFilename);
                    if (!File.Exists(jsonFilepath))
                    {
                        File.WriteAllText(jsonFilepath, "{}");
                    }
                    var jsonLocalizationData = File.ReadAllText(jsonFilepath).Trim() ?? "{}";

                    var localizedData = new Dictionary<string, string>();
                    if (StringUtils.IsValidJson(jsonLocalizationData))
                    {
                        localizedData = JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonLocalizationData);
                    }
                    

                    if (!_localization.Any(l => l.Locale == ciName))
                    {
                        _localization.Add(new HFJsonLocalization
                        {
                            Locale = ciName,
                            LocalizedValues = localizedData
                        });
                    }
                    else
                    {
                        var existing = _localization.FirstOrDefault(l => l.Locale == ciName);
                        existing.LocalizedValues = localizedData;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }

        public LocalizedString this[string name]
        {
            get
            {
                var value = GetString(name);
                return new LocalizedString(name, value ?? name, resourceNotFound: value == null);
            }
        }

        public LocalizedString this[string name, params object[] arguments]
        {
            get
            {
                var format = GetString(name);
                var value = string.Format(format ?? name, arguments);
                return new LocalizedString(name, value, resourceNotFound: format == null);
            }
        }

        public IEnumerable<LocalizedString> GetAllStrings(bool includeParentCultures)
        {
            var locale = _localization
              .FirstOrDefault(l => l.Locale.ToLower() == CultureInfo.CurrentCulture.Name.ToLower());
            return locale
              .LocalizedValues
              .Select(l => new LocalizedString(l.Key, l.Value, true));
        }

        public IStringLocalizer WithCulture(CultureInfo culture)
        {
            return new HFJsonStringLocalizer();
        }

        private string GetString(string name)
        {
            var locale = _localization
              .FirstOrDefault(l => l.Locale.ToLower() == CultureInfo.CurrentCulture.Name.ToLower());
            var tryGetValue = locale.LocalizedValues.TryGetValue(name, out string value);
            if (tryGetValue)
            {
                return value;
            }

            return name;
        }

        /// <summary>
        /// Clear all loaded localization strings
        /// </summary>
        /// <param name="culturesToClearFromCache"></param>
        public void ClearMemCache(IEnumerable<CultureInfo> culturesToClearFromCache = null)
        {
            if (culturesToClearFromCache != null && culturesToClearFromCache.Any())
            {
                _localization.ForEach(x =>
                {
                    x.LocalizedValues.Clear();
                });
            }
            else
            {
                foreach(var l in _localization)
                {
                    if (culturesToClearFromCache.Any(ln => ln.Name.ToLower() == l.Locale.ToLower())) {
                        l.LocalizedValues.Clear();
                    }
                }
            }
        }
    }
}
