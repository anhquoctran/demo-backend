##Environment requirements

- Visual Studio 2019
- .NET Core 3.1 SDK and ASP.NET Core 3.1 installed
- MySQL Server 8.x Community Edition
- Redis
- VerneMQ MQTT broker
