﻿using HappyFarm.Core.Commons;
using HappyFarm.MqttService.Models.Responses;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Collections.Generic;
using System.Linq;

namespace HappyFarm.MqttService.Filters
{
    public class ValidationActionFilterAttribute : ActionFilterAttribute
    {
        private static readonly string[] DefaultCommonKeys = { "$" };

        /// <summary>
        /// Ignored. Called after controller's action methods executed
        /// </summary>
        /// <param name="context"></param>
        public override void OnActionExecuted(ActionExecutedContext context) { }

        /// <summary>
        /// Called before controller's action methods executing
        /// </summary>
        /// <param name="context"></param>
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                ObjectResult objectResult = new ObjectResult(new BadRequestResponse<Dictionary<string, List<string>>>(GetErrorValidate(context.ModelState)))
                {
                    StatusCode = 400
                };
                context.Result = objectResult;
            }
        }

        /// <summary>
        /// Get error key from model state entry
        /// </summary>
        /// <param name="error"></param>
        /// <returns></returns>
        private static string GetErrorKey(KeyValuePair<string, ModelStateEntry> error)
        {
            if (StringUtils.CheckIsEmptyOrWhitespaceString(error.Key) || DefaultCommonKeys.Contains(error.Key))
            {
                return "common";
            }

            return error.Key.Replace("$.", string.Empty);

        }

        /// <summary>
        /// Get formatted errors
        /// </summary>
        /// <param name="modelStateErrors">Request model state binding errors</param>
        /// <returns></returns>
        private Dictionary<string, List<string>> GetErrorValidate(ModelStateDictionary modelStateErrors)
        {
            var dict = new Dictionary<string, List<string>>();

            foreach (var error in modelStateErrors)
            {
                if (error.Value.Errors.Count > 0)
                {
                    var messages = new List<string>();

                    foreach (var message in error.Value.Errors)
                    {
                        messages.Add(message.ErrorMessage);
                    }
                    var key = GetErrorKey(error);
                    if (dict.ContainsKey(key))
                    {
                        var value = dict[key];
                        value.AddRange(messages);
                        dict[key] = value;
                    }
                    else
                    {
                        dict.Add(key, messages);
                    }

                }
            }
            return dict;
        }
    }
}
