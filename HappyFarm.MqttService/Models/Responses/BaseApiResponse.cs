﻿/*
 * Copyright [2020] [anhquoctran]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

using System.Net;
using Microsoft.AspNetCore.WebUtilities;
using Newtonsoft.Json;

namespace HappyFarm.MqttService.Models.Responses
{
    public class BaseApiResponse<T> : BaseApiResponse
    {
        public BaseApiResponse(int statusCode = (int)HttpStatusCode.OK, string message = "OK", bool success = true, T data = default)
          : base(data, message, statusCode, success)
        { }

        public BaseApiResponse(T data, string message, int statusCode = (int)HttpStatusCode.OK, bool success = true)
          : base(data, message, statusCode, success)
        { }
    }

    public class BaseApiResponse
    {
        public BaseApiResponse(object data, string message, int statusCode = (int)HttpStatusCode.OK, bool success = true)
        {
            Status = statusCode;
            Message = message ?? GetDefaultMessageFromStatusCode(statusCode);
            Success = success;
            Data = data;
        }

        public BaseApiResponse(object errors, string message, int statusCode = (int)HttpStatusCode.BadRequest)
        {
            Status = statusCode;
            Message = message ?? GetDefaultMessageFromStatusCode(statusCode);
            Success = false;
            Data = null;
            Errors = errors;
        }

        [JsonProperty("status")]
        public int Status { get; protected set; }

        [JsonProperty("message")]
        public string Message { get; protected set; }

        [JsonProperty("success")]
        public bool Success { get; protected set; }

        [JsonProperty(PropertyName = "data", NullValueHandling = NullValueHandling.Ignore)]
        public object Data { get; protected set; }

        [JsonProperty(PropertyName = "errors", NullValueHandling = NullValueHandling.Ignore)]
        public object Errors { get; protected set; }

        public static string GetDefaultMessageFromStatusCode(int statusCode)
        {
            return ReasonPhrases.GetReasonPhrase(statusCode);
        }
    }

    public enum ResponseErrorFlag
    {
        AlreadyLoggedIn = 0,
        AlreadyUsedByAnother = 1,
        Other = 2
    }
}
