﻿/*
 * Copyright [2020] [anhquoctran]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json;

namespace HappyFarm.MqttService.Models.Responses
{
    public class SucceedResponse<T> : BaseApiResponse<T> where T : class
    {
        public SucceedResponse(T data = null) : base((int)HttpStatusCode.OK, "OK", true, data) { }
    }

    public class SucceedResponse : BaseApiResponse
    {
        public SucceedResponse() : base(null, "OK", 200, true) { }
    }

    public class PaginationSucceedRespponse<T> : BaseApiResponse<PaginationData<T>> where T : class
    {
        public PaginationSucceedRespponse(PaginationData<T> list) : base((int)HttpStatusCode.OK, "OK", true, list) { }
    }

    public class PaginationData<T>
    {
        [JsonProperty("current_page")]
        public int Page { get; private set; }

        [JsonProperty("last_page")]
        public int TotalPages { get; private set; }

        [JsonProperty("total")]
        public int TotalItems { get; private set; }

        [JsonProperty("per_page")]
        public int PageSize { get; private set; }

        [JsonProperty("from")]
        public int From { get; set; }

        [JsonProperty("to")]
        public int To { get; set; }

        [JsonProperty("items")]
        public IList<T> Items { get; private set; }

        public PaginationData(int page, int totalPages, int totalItems, int pageSize, IList<T> items)
        {
            Page = page;
            TotalItems = totalItems;
            TotalPages = totalPages;
            Items = items;
            PageSize = pageSize;
            (From, To) = GetFromAndTo(page, pageSize);
        }

        private (int, int) GetFromAndTo(int page, int pageSize)
        {
            var from = ((page - 1) * pageSize) + 1;
            var to = page * pageSize;

            return (from, to);
        }
    }
}
