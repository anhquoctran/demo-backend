﻿using Askmethat.Aspnet.JsonLocalizer.Localizer;
using HappyFarm.Infrastructure.Services.Implementations;
using HappyFarm.Infrastructure.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace HappyFarm.MqttService.Controllers
{
    [ApiController, Route("")]
    public class HomeController : BaseController
    {
        private readonly IMqttHeartbeatService _mqttHeartbeatService;

        public HomeController(
            IJsonStringLocalizer localizer,
            IMqttHeartbeatService mqttHeartbeatService
        ) : base(localizer)
        {
            _mqttHeartbeatService = mqttHeartbeatService;
        }
        
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var checkResponse = await _mqttHeartbeatService.GetHeartbeatData();

            if (checkResponse != null)
            {
                return Succeed(new
                {
                    time_req = DateTime.UtcNow,
                    sockets = new
                    {
                        total = SignalrConnectionManager.Instance.Total,
                        connections = SignalrConnectionManager.Instance.GetListConnectionPublic()
                    },
                    mqtt_heartbeat = checkResponse
                }) ;
            }
            else
            {
                return ServerError();
            }
        }

        [HttpGet("socket")]
        public IActionResult RedirectToConnections()
        {
            return RedirectToAction("GetConnections");
        }

        [HttpGet("socket/connections")]
        public IActionResult GetConnections()
        {
            return Succeed(new
            {
                connections = SignalrConnectionManager.Instance.GetListConnectionPublic()
            });
        }
    }
}