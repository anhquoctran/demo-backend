using Askmethat.Aspnet.JsonLocalizer.Localizer;
using AutoMapper;
using FluentValidation;
using FluentValidation.AspNetCore;
using HappyFarm.Core;
using HappyFarm.Core.Commons;
using HappyFarm.Core.UnitOfWork;
using HappyFarm.Infrastructure.Commons;
using HappyFarm.Infrastructure.MapperProfile;
using HappyFarm.Infrastructure.Services.Implementations;
using HappyFarm.Infrastructure.Services.Interfaces;
using HappyFarm.Infrastructure.SignalR;
using HappyFarm.Infrastructure.SignalR.Authorization;
using HappyFarm.Infrastructure.Validations;
using HappyFarm.MqttService.Filters;
using HappyFarm.MqttService.Localizations;
using HappyFarm.MqttService.Middlewares;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Buffers;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;

namespace HappyFarm.MqttService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            AppConfig.LoadConfig(GetAbsoluteEndpointPath());
            FileUtils.InitStaticFolder(EndpointPath);
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public static readonly CultureInfo DefaultCultureInfo = new CultureInfo("vi-VN");

        public static readonly RequestCulture DefaultRequestCulture = new RequestCulture(DefaultCultureInfo);

        public static readonly HashSet<CultureInfo> SupportedCultures = new HashSet<CultureInfo>
        {
            new CultureInfo("en-US"),
            new CultureInfo("vi-VN")
        };

        public static readonly string EndpointPath = GetAbsoluteEndpointPath();

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers()
                .AddNewtonsoftJson();

            services.AddSingleton<IAuthorizationHandler, HFAuthorizationHandler>();

            services.AddAuthorization(options =>
            {
                options.AddPolicy("HFAuthorizationPolicy", policy =>
                {
                    policy.Requirements.Add(new HFAuthorizationRequirement());
                });
            });

            services.AddSingleton<IUserIdProvider, HFUserIdProvider>();

            services.AddSignalR()
                .AddNewtonsoftJsonProtocol();

            // Auto Mapper Configurations
            var mapperConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile<Default>();
            });

            var mapper = mapperConfig.CreateMapper();
            services.AddSingleton(mapper);

            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
                options.SuppressMapClientErrors = true;
            });

            services.Configure<RequestLocalizationOptions>(options =>
            {
                var listSupportedCultures = SupportedCultures.ToList();
                options.DefaultRequestCulture = DefaultRequestCulture;
                options.SupportedCultures = listSupportedCultures;
                options.SupportedUICultures = listSupportedCultures;
            });

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddLocalization(options =>
            {
                options.ResourcesPath = Constants.RESOURCE_DIRECTORY_NAME;
            });

            services.AddTransient<IMqttHeartbeatService>(x =>
            {
                var logger = x.GetRequiredService<ILogger<MqttHeartbeatService>>();
                return new MqttHeartbeatService(AppConfig.Config.Mqtt.VernemqBaseURL, AppConfig.Config.Mqtt.Token, logger);
            });

            services.AddMvc(options =>
                {
                    options.Filters.Add<ValidationActionFilterAttribute>();
                    options.OutputFormatters.Clear();
                    options.OutputFormatters.Add(new NewtonsoftJsonOutputFormatter(new JsonSerializerSettings
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore,

                    }, ArrayPool<char>.Shared, options));

                })
                .SetCompatibilityVersion(CompatibilityVersion.Latest)
                .AddDataAnnotationsLocalization()
                .AddFluentValidation(fv =>
                {
                    fv.RegisterValidatorsFromAssembly(Assembly.GetExecutingAssembly());
                    ValidatorOptions.LanguageManager = new HFLanguageManager();
                });

            services.AddDbContext<HappyFarmDbContext>(options =>
            {
                options
                    .UseMySql(AppConfig.Config.Database.ConnectionString, option =>
                    {
                        option.EnableRetryOnFailure();
                    });
            }, ServiceLifetime.Singleton);

            services.AddSingleton<IUnitOfWork, UnitOfWork>();
            services.AddSingleton<IDistributedService, DistributedService>();
            services.AddSingleton<IMessageQueueService, MessageQueueService>();
            services.AddSingleton<IAuthService, AuthService>();
            services.AddSingleton<IUserService, UserService>();
            services.AddSingleton<IFarmService, FarmService>();
            services.AddSingleton<IGatewayService, GatewayService>();
            services.AddSingleton<ISensorNodeService, SensorNodeService>();
            services.AddSingleton<ISensorValueService, SensorValueService>();

            SetupCors(services);
            SetupLocalization(services);

            services.AddHostedService<MqttBackgroundService>();
        }

        private static void SetupCors(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("default",
                    builder =>
                    {
                        builder.AllowAnyMethod()
                            .AllowAnyHeader()
                            .SetIsOriginAllowed(x => true)
                            .AllowCredentials()
                            .WithOrigins(Constants.AllowedOrigins)
                            .SetIsOriginAllowedToAllowWildcardSubdomains();
                    });
            });

        }

        private static void SetupLocalization(IServiceCollection services)
        {
            services.AddSingleton<IStringLocalizerFactory, HFJsonStringLocalizerFactory>();
            services.AddSingleton<IJsonStringLocalizer, HFJsonStringLocalizer>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseRouting();

            app.UseCors("default");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                HandleError(app);
            }

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<HappyFarmHub>("/happyfarm-hub");
            });

            CultureInfo.DefaultThreadCurrentCulture = DefaultCultureInfo;
            CultureInfo.DefaultThreadCurrentUICulture = DefaultCultureInfo;

            app.UseHttpExceptionMiddleware();

            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(EndpointPath, Constants.STATIC_DIRECTORY_NAME)),
                RequestPath = new PathString(Constants.STATIC_PATH)
            });

            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                DefaultRequestCulture = DefaultRequestCulture,
                SupportedCultures = SupportedCultures.ToList(),
                SupportedUICultures = SupportedCultures.ToList()
            });

            app.UseHubRequireAuthorizationMiddleware();

            if (!env.IsProduction() && !env.IsStaging()) return;
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            app.Use((context, next) =>
            {
                foreach (var header in Constants.UntrustedHeaders)
                {
                    context.Response.Headers.Remove(header);
                }

                foreach (var (key, value) in Constants.SecurityHeaders)
                {
                    context.Response.Headers[key] = value;
                }

                return next();
            });
        }

        private static void HandleError(IApplicationBuilder app)
        {
            app.UseExceptionHandler(errorApp =>
            {
                errorApp.Run(async context =>
                {
                    context.Response.ContentType = "application/json";
                    context.Response.Headers["Accept"] = "application/json";

                    if (context.Response.StatusCode >= (int)HttpStatusCode.InternalServerError)
                    {
                        var httpStatus = (HttpStatusCode)context.Response.StatusCode;
                        var json = ResponseUtils.GetJsonResponseString(httpStatus);
                        context.Response.Clear();
                        await context.Response.WriteAsync(json, Encoding.UTF8);
                    }

                });
            });

        }

        private static string GetAbsoluteEndpointPath()
        {
            var asmPath = Assembly.GetExecutingAssembly().CodeBase;
            var executePath = Path.GetDirectoryName(asmPath) ?? string.Empty;
            var result = executePath.Replace("file:\\", string.Empty).Replace("file:", string.Empty);
            return result;
        }
    }
}